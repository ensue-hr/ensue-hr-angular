export const environment = {
  production: true,
  api: 'https://botapi.ensue.com.np/api/',
  googleProvider: 'https://botapi.ensue.com.np/auth/login/google?redirect_uri=https://ensuebot-staging.ensue.com.np/auth/callback',
  microsoftProvider: 'https://botapi.ensue.com.np/auth/login/microsoft?redirect_uri=https://ensuebot-staging.ensue.com.np/auth/callback',
};
