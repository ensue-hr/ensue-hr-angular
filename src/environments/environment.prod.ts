export const environment = {
  production: true,
  api: 'http://botapi.ensue.local/api/',
  googleProvider: 'https://botapi.ensue.local/auth/login/google?redirect_uri=https://hr.ensue.local/auth/callback',
  microsoftProvider: 'https://botapi.ensue.local/auth/login/microsoft?redirect_uri=https://hr.ensue.local/auth/callback',
};
