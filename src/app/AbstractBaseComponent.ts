import {Injectable, Injector, OnDestroy} from '@angular/core';
import {
  AjaxSpinnerService,
  NicoPageTitle,
  NicoSessionService,
  NicoStorageService,
  SmartValidationMessenger
} from '@system/services';
import {NicoHttpClient} from '@system/http-client';
import {ActivatedRoute, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Location} from '@angular/common';
import {MatDialog} from '@angular/material/dialog';
import {AppService, SnackbarService} from '@common/services';
import {MatDialogRef} from '@angular/material/dialog/dialog-ref';
import {ConfirmationDialogInterface} from '@common/components/confirmation-dialog/confirmation-dialog.interface';
import {ConfirmationDialogComponent} from '@common/components/confirmation-dialog/confirmation-dialog.component';
import {TranslateService} from '@ngx-translate/core';
import {SnackBarComponent} from '@common/components/snack-bar/snack-bar.component';
import {FilterQueryParamInterface} from '@common/constants';
import {NicoUtils} from '@system/utilities';
import {HttpParams} from '@angular/common/http';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Subject, Subscription} from 'rxjs';
import {Status} from '@common/enums/status.enums';
import {BreadcrumbService} from 'xng-breadcrumb';
import {NicoFileUploadEvent} from '@system/modules/nico-file-upload';
import {EmployeeDocument} from '@common/enums/employeeView.enums';
import {FormGroup} from '@angular/forms';
import {DateUtility} from '@common/utilities/date-utility';

@Injectable()

export abstract class AbstractBaseComponent implements OnDestroy {

  protected pageTitleService: NicoPageTitle;

  protected httpClient: NicoHttpClient;

  protected session: NicoSessionService;

  protected spinnerService: AjaxSpinnerService;

  protected snackbar: MatSnackBar;

  protected modalService: MatDialog;

  protected breadcrumbService: BreadcrumbService;

  protected router: Router;

  protected activatedRoute: ActivatedRoute;

  protected location: Location;

  protected translateService: TranslateService;

  protected storage: NicoStorageService;

  protected snackbarService: SnackbarService;
  /**
   * The page title for the component. It will be displayed on the tab/windows of the browser
   */
  protected pageTitle: string;

  public appService: AppService;

  public filterQueryParams: FilterQueryParamInterface;

  public filterHttpQueryParams: HttpParams;

  public sortByDropdownValues = [ ];

  /**
   * An observable to watch search text change
   */
  protected searchTextChange: Subject<string> = new Subject<string>();
  public searchTextChangeSubscription: Subscription;

  public sortOrderChange: Subject<string> = new Subject<string>();
  public sortOrderChangeSubscription: Subscription;

  public sortByChange: Subject<string> = new Subject<string>();
  public sortByChangeSubscription: Subscription;

  public publishStatusChange: Subject<string> = new Subject<string>();
  public publishStatusChangeSubscription: Subscription;

  public publishCheckLeaveChange: Subject<string> = new Subject<string>();
  public publishLeaveStatusChangeSubscription: Subscription;

  public publishLeaveChange: Subject<string> = new Subject<string>();
  public publishLeaveChangeSubscription: Subscription;

  /**
   * Switch variables
   */
  public switchState: boolean;

  /**
   * File Upload Event
   */
  public fileUploadEvent: NicoFileUploadEvent;

  public fileUploadFlag = true;

  /**
   * user settings from local storage
   */
  public userSettingsItems: any;

  public authUserItems: any;

  /**
   * Constructor
   */
  protected constructor(injector: Injector) {
    this.pageTitleService = injector.get(NicoPageTitle);
    this.httpClient = injector.get(NicoHttpClient);
    this.session = injector.get(NicoSessionService);
    this.spinnerService = injector.get(AjaxSpinnerService);
    this.snackbar = injector.get(MatSnackBar);
    this.modalService = injector.get(MatDialog);
    this.breadcrumbService = injector.get(BreadcrumbService);
    this.router = injector.get(Router);
    this.activatedRoute = injector.get(ActivatedRoute);
    this.location = injector.get(Location);
    this.appService = injector.get(AppService);
    this.translateService = injector.get(TranslateService);
    this.storage = injector.get(NicoStorageService);
    this.snackbarService = injector.get(SnackbarService);
  }

  public onComponentReady(): void {
    this.pageTitleService.setTitle(this.pageTitle);
    this.filterQueryParams = {
      ...(!NicoUtils.isNullOrUndefined(this.activatedRoute.snapshot.queryParams?.page) &&
        {page: this.activatedRoute.snapshot.queryParams.page}),
      ...(!NicoUtils.isNullOrUndefined(this.activatedRoute.snapshot.queryParams?.per_page) &&
        {per_page: this.activatedRoute.snapshot.queryParams.per_page}),
      ...(!NicoUtils.isNullOrUndefined(this.activatedRoute.snapshot.queryParams?.sort_order) &&
        {sort_order: this.activatedRoute.snapshot.queryParams.sort_order}),
      ...(!NicoUtils.isNullOrUndefined(this.activatedRoute.snapshot.queryParams?.sort_by) &&
        {sort_by: this.activatedRoute.snapshot.queryParams.sort_by}),
      ...(!NicoUtils.isNullOrUndefined(this.activatedRoute.snapshot.queryParams?.status) &&
        {status: this.activatedRoute.snapshot.queryParams.status}),
      ...(!NicoUtils.isNullOrUndefined(this.activatedRoute.snapshot.queryParams?.keyword) &&
        {keyword: ''})
    };
    if (this.sortByDropdownValues.length > 0) {
      this.filterQueryParams.sort_by = this.sortByDropdownValues[this.sortByDropdownValues.length - 1].value;
      this.filterQueryParams.sort_order = 'desc';
    }
    this.setFilterHttpQueryParams();
    // Search component
    this.subscribeSearchTextChange();
    this.subscribeSortOrderChange();
    this.subscribeSortByChange();
    this.subscribeIsLeaveChange();
    this.subscribePublishStatusChange();
  }

  // tslint:disable-next-line:use-lifecycle-interface contextual-lifecycle
  public ngOnInit(): void {
    this.userSettingsItems = this.storage.getItem('settings');
    this.translateService.get('dummyString').subscribe(_ => {
      this.onComponentReady();
    });
    this.userSettingsItems = this.storage.getItem('settings');
    this.authUserItems = this.storage.getItem('auth_user');
  }

  /**
   * Go one step back to the location
   */
  goBack(): void {
    this.location.back();
  }

  protected getLangString(key: string, locale?: string): any {
    if (!locale) {
      locale = this.translateService.getDefaultLang();
    }
    return this.translateService.translations[locale];
  }

  public log(data: any): void {
  }

  /**
   * Confirmation Dialog Box Trigger
   */
  public openConfirmationDialogBox(data?: ConfirmationDialogInterface): MatDialogRef<any> {
    return this.modalService.open(ConfirmationDialogComponent, {data});
  }

  /**
   * To handle form submission errors
   */
  public onFormSubmissionError(err: any, formId: string): void {
    const messenger = new SmartValidationMessenger();
    err.status === 417 ?
      messenger.attach(formId, err.error) :
      this.snackbarService.showErrorSnackBar(err.error.message);
  }

  /**
   * Show success snackbar
   */
  public showSuccessSnackBar(message: string): void {
    this.snackbarService.showSuccessSnackBar(message);
  }

  /**
   * Show error snackbar
   */
  public showErrorSnackBar(message: string): void {
    this.snackbarService.showErrorSnackBar(message);
  }

  /**
   * Show info snackbar
   */
  public showInfoSnackBar(message: string): void {
    this.snackbarService.showInfoSnackBar(message);
  }

  public setFilterHttpQueryParams(): void {
    this.filterHttpQueryParams = new HttpParams();

    Object.keys(this.filterQueryParams).map(key => {
      this.filterHttpQueryParams = this.filterHttpQueryParams.set(key, this.filterQueryParams[key]);
    });

    this.router.navigate([], {
      queryParams: this.filterQueryParams,
      queryParamsHandling: 'merge',
      preserveFragment: true
    });

    // Sets Query Params in URL bar
    // const urlTree = this.router.createUrlTree([], {
    //   queryParams: this.filterQueryParams,
    //   queryParamsHandling: 'merge',
    //   preserveFragment: true
    // });
    // this.location.go(urlTree.toString());
  }

  public getFilterHttpQueryParams(): HttpParams {
    return this.filterHttpQueryParams;
  }


  /**
   * Event listener for search text change in search-nav component
   */
  public onSearchTextChange(evt: string): void {
    this.searchTextChange.next(evt);
  }

  public onSortOrderChange($event): void {
    this.sortOrderChange.next($event);
  }

  public onSortByChange($event): void {
    this.sortByChange.next($event);
  }

  public onStatusChange($event): void {
    this.publishStatusChange.next($event);
  }

  public onLeaveStatusChange($event): void {
    this.publishCheckLeaveChange.next($event);
  }

  /**
   * Make a subscription to search Text change.
   * We should never run function that take longer time to execute directly when a model/text changes.
   * We need to have a debounce time and see if the text is distinct
   */
  protected subscribeSearchTextChange(): void {
    this.searchTextChangeSubscription = this.searchTextChange.pipe(debounceTime(500), distinctUntilChanged())
      .subscribe((value: string) => {
        this.filterQueryParams.keyword = value;
        this.filterQueryParams.page = 1;
        this.setFilterHttpQueryParams();
        this.getModels();
      });
  }

  protected subscribeSortOrderChange(): void {
    this.sortOrderChangeSubscription = this.sortOrderChange.subscribe((value: 'asc' | 'desc') => {
      this.filterQueryParams.sort_order = value;
      this.filterQueryParams.page = 1;
      this.setFilterHttpQueryParams();
      this.getModels();
    });
  }

  protected subscribeSortByChange(): void {
    this.sortByChangeSubscription = this.sortByChange.subscribe((value: string) => {
      this.filterQueryParams.sort_by = value;
      this.filterQueryParams.page = 1;
      this.setFilterHttpQueryParams();
      this.getModels();
    });
  }

  protected subscribeIsLeaveChange(): void {
    this.publishLeaveChangeSubscription = this.publishCheckLeaveChange.subscribe((value: string) => {
      this.filterQueryParams.on_leave = value === 'all' ? '' : Number(value);
      this.filterQueryParams.page = 1;
      this.setFilterHttpQueryParams();
      this.getModels();
    });
  }

  protected subscribePublishStatusChange(): void {
    this.publishStatusChangeSubscription = this.publishStatusChange.subscribe((value) => {
      this.filterQueryParams.status = value === 'all' ? '' : Number(value);
      // this.filterQueryParams.status = Number(value);
      // if (value === '2') {
      //   this.filterQueryParams.status = '';
      // }
      this.setFilterHttpQueryParams();
      this.getModels();
    });
  }

  /**
   * Pagination events
   */
  public onPageChange(event: number): void {
    this.filterQueryParams.page = Number(event);
    this.setFilterHttpQueryParams();
    this.getModels();
  }

  public onPageLimitChange(event: number): void {
    this.filterQueryParams.per_page = Number(event);
    this.filterQueryParams.page = 1;
    this.setFilterHttpQueryParams();
    this.getModels();
  }

  /**
   * View Toggle change
   */
  public getModels(): void {
  }

  /**
   * For Publish/Unpublish toggle switch in form
   */
  public onFormSwitchValueChange(formGroup?): void {
    this.switchState = !this.switchState;
    if (formGroup.value.status === Status.Published) {
      formGroup.get('status').setValue(Status.Unpublished);
    } else {
      formGroup.get('status').setValue(Status.Published);
    }
  }

  /**
   * For file upload event
   */
  public onFileSelect(evt: NicoFileUploadEvent, form: FormGroup): void {
    this.fileUploadEvent = evt;
    this.checkFormat(this.fileUploadEvent.payload.item(0).type);
    if (this.fileUploadFlag) {
      form.get('file').setValue(this.fileUploadEvent.payload.item(0));
      this.uploadFile();
    } else {
      form.get('file').setValue('');
    }
  }

  /**
   * Check if the file matches the specified formats
   */
  public checkFormat(extension: string): void {
    this.fileUploadFlag = false;
    for (const value in EmployeeDocument) {
      if (extension === EmployeeDocument[value]) {
        this.fileUploadFlag = true;
        break;
      }
    }
  }

  /**
   * Upload File
   */
  public uploadFile(): void {}

  /**
   * Find time in hours and minutes
   */
  public getHoursAndMinutes(seconds: number): string {
    let time = '';
    const hours = DateUtility.getHoursFromSeconds(seconds);
    const minutes = DateUtility.getMinutesFromSeconds(seconds);
    if (hours === 0) {
      if (minutes === 0) {
        time = this.translateService.instant('MOD_ATTENDANCE.ATTENDANCE_SUMMARY.HOURS_LABEL', {hours});
      } else {
        time = this.translateService.instant('MOD_ATTENDANCE.ATTENDANCE_SUMMARY.MINUTES_LABEL', {minutes});
      }
    } else {
      if (minutes === 0) {
        time = this.translateService.instant('MOD_ATTENDANCE.ATTENDANCE_SUMMARY.HOURS_LABEL', {hours});
      } else {
        time = this.translateService.instant('MOD_ATTENDANCE.ATTENDANCE_SUMMARY.HOURS_AND_MINUTES_LABEL', {
          hours,
          minutes
        });
      }
    }
    return time;
  }

  /**
   * Present Absent Pill
   */
  public getPresentAbsentPill(leaveIn: number, checkIn: number): any {
    if (leaveIn) {
      return {label: this.translateService.instant('MOD_ATTENDANCE.MOD_ADMIN_ATTENDANCE.LEAVE_LABEL'), color: 'warning'};
    }
    if (checkIn) {
      return {label: this.translateService.instant('MOD_ATTENDANCE.MOD_ADMIN_ATTENDANCE.PRESENT_LABEL'), color: 'success'};
    }
    else{
      return {label: this.translateService.instant('MOD_ATTENDANCE.MOD_ADMIN_ATTENDANCE.ABSENT_LABEL'), color: 'danger'};
    }
  }

  public ngOnDestroy(): void {
    if (this.searchTextChangeSubscription) {
      this.searchTextChangeSubscription.unsubscribe();
    }
    if (this.sortByChangeSubscription) {
      this.sortByChangeSubscription.unsubscribe();
    }
    if (this.sortOrderChangeSubscription) {
      this.sortOrderChangeSubscription.unsubscribe();
    }
    if (this.publishStatusChangeSubscription) {
      this.publishStatusChangeSubscription.unsubscribe();
    }
    if (this.publishLeaveStatusChangeSubscription) {
      this.publishLeaveStatusChangeSubscription.unsubscribe();
    }
    if (this.publishLeaveChangeSubscription) {
      this.publishLeaveChangeSubscription.unsubscribe();
    }
  }
}
