import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

/**
 * App Routes
 */
const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: 'manage',
    loadChildren: () => import('./modules/admin-module/admin.module').then(m => m.AdminModule),
    data: {breadcrumb: {skip: true}}
  },
  {
    path: 'web-components',
    loadChildren: () => import ('./modules/web-components/web-components.module').then(m => m.WebComponentsModule)
  },
  {path: '', redirectTo: '/manage/dashboard', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    anchorScrolling: 'enabled',
    scrollOffset: [0, 100],
    scrollPositionRestoration: 'enabled',
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
