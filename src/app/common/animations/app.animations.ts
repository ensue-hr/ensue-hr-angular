import {animate, animateChild, group, query, stagger, style, transition, trigger} from '@angular/animations';

export const ListAnimation = trigger('listAnimation', [
  transition('* <=> *', [
    query(':enter',
      [style({opacity: 0}), stagger('100ms', animate('600ms ease-out', style({opacity: 1})))],
      {optional: true, limit: 4}
    ),
    query(':leave',
      animate('300ms', style({opacity: 0})),
      {optional: true, limit: 4}
    )
  ])
]);

export const FadeInOutAnimation = trigger('fadeInOutAnimation', [
  transition(':enter', [
    style({opacity: 0}),
    animate('300ms', style({opacity: 1})),
  ]),
  transition(':leave', [
    style({opacity: 1}),
    animate('300ms', style({opacity: 0}))
  ])
]);

export const SlideInAnimation = trigger('slideInAnimation', [
  transition(':enter', [
    style({opacity: 0, transform: 'translateY(0.5rem)'}),
    animate('350ms', style({opacity: 1, transform: 'translateY(0)'})),
    animateChild()
  ]),
  transition(':leave', [
    style({opacity: 1}),
    animate('350ms', style({opacity: 0})),
    animateChild()
  ])
]);

export const AccordionAnimation = trigger('accordionAnimation', [
  transition(':enter', [
    style({height: 0, overflow: 'hidden'}),
    animate('300ms', style({height: '*'})),
  ]),
  transition(':leave', [
    style({overflow: 'hidden'}),
    animate('300ms', style({height: 0}))
  ])
]);

export const DrawerAnimation = trigger('drawerAnimation', [
  transition(':enter', [
    group([
      query('.drawer-overlay', [
        style({opacity: 0}),
        animate('500ms cubic-bezier(0.35, 0, 0.25, 1)', style({opacity: 1})),
      ], {optional: true}),
      query('.drawer-container.end', [
        style({right: '-100%'}),
        animate('300ms cubic-bezier(0.35, 0, 0.25, 1)', style({right: 0})),
      ], {optional: true}),
      query('.drawer-container.start', [
        style({left: '-100%'}),
        animate('300ms cubic-bezier(0.35, 0, 0.25, 1)', style({left: 0})),
      ], {optional: true}),
    ])
  ]),
  transition(':leave', [
    group([
      query('.drawer-overlay', [
        animate('500ms 100ms cubic-bezier(0.35, 0, 0.25, 1)', style({opacity: 0}))
      ], {optional: true}),
      query('.drawer-container.end', [
        animate('300ms cubic-bezier(0.35, 0, 0.25, 1)', style({right: '-100%'}))
      ], {optional: true}),
      query('.drawer-container.start', [
        animate('300ms cubic-bezier(0.35, 0, 0.25, 1)', style({left: '-100%'}))
      ], {optional: true}),
    ])
  ]),
]);

export const RouteAnimations = trigger('routeAnimations', [
  transition('* <=> *', [
    query(':enter, :leave', [
      style({
        opacity: 0,
        position: 'absolute',
        width: '100%',
        height: '100%'
      }),
    ], {optional: true}),
    group([
      query(':enter', [
        animate('0.4s ease',
          style({
            opacity: 1,
          })),
      ], {optional: true}),
    ])
  ])
]);
