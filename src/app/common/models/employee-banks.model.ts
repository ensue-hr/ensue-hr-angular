import {BaseResource} from '@system/datamodels';

export class EmployeeBanksModel extends BaseResource {
  public creatableAttributes = [
    'id',
    'name',
    'branch',
    'account_number',
    'status',
    'created_at',
    'updated_at'
  ];
}
