import {BaseResource} from '@system/datamodels';

export class AttendanceModel extends BaseResource {
  public creatableAttributes = [
    'employee_id',
    'attend_date',
    'check_in',
    'check_out',
    'leave_in',
    'leave_out',
    'office_time',
    'work_time',
    'break_time',
    'leave_time',
    'employee',
  ];
}

