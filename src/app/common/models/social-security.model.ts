import {BaseResource} from '@system/datamodels';

export class SocialSecurityModel extends BaseResource{
  public creatableAttributes = [
    'id',
    'title',
    'number',
    'start_date',
    'status',
    'end_date',
    'created_at',
    'updated_at'
  ];
}
