import {BaseResource} from '@system/datamodels';

export class EmployeeModel extends BaseResource{
  public creatableAttributes = [
    'id',
    'name',
    'email',
    'status',
    'avatar',
    'code',
    'position',
    'joined_at',
    'department',
    'created_at',
  ];
}
