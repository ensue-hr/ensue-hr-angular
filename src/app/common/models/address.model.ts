import {BaseResource} from '@system/datamodels';

export class AddressModel extends BaseResource {
  public creatableAttributes = [
    'id',
    'country',
    'state',
    'city',
    'street',
    'zip_code',
    'type',
    'created_at',
    'updated_at'
  ];

}
