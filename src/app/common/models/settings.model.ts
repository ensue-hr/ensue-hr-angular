import {BaseResource} from '@system/datamodels';

export class SettingModel extends BaseResource {
  public creatableAttributes = [
    'id',
    'key',
    'value',
    'type',
    'created_at',
    'updated_at'
  ];
}

