import {BaseResource} from '@system/datamodels';
import {AttendanceType} from '@common/enums/attendanceType.enum';

export class AttendanceDetailModel extends BaseResource {
  public activityType = AttendanceType;
  public creatableAttributes = [
    'id',
    'type',
    'browser',
    'device',
    'location',
    'ip',
    'attend_at',
    'reason',
    'status',
    'created_at',
    'updated_at',
    'parents'
  ];
  public typeName: string;
  public typeIcon: string;

  public create(obj: any): AttendanceDetailModel {
    const model = super.create(obj) as AttendanceDetailModel;
    model.getAttendanceActivityName();
    return model;
  }

  // @ts-ignore
  public getAttendanceActivityName(): string {
    switch (this.type) {
      case this.activityType.CheckIn:
        this.typeName = 'Check in';
        this.typeIcon = 'far fa-long-arrow-right';
        break;
      case this.activityType.CheckOut:
        this.typeName = 'Check out';
        this.typeIcon = 'far fa-long-arrow-right';
        break;
      case this.activityType.BreakIn:
        this.typeName = 'Work resumed';
        this.typeIcon = 'far fa-sign-in';
        break;
      case this.activityType.BreakOut:
        this.typeName = 'Break taken';
        this.typeIcon = 'far fa-sign-out';
        break;
      case this.activityType.LeaveIn:
        this.typeName = 'Leave in';
        this.typeIcon = 'far fa-sign-out';
        break;
      case this.activityType.LeaveOut:
        this.typeName = 'Leave out';
        this.typeIcon = 'far fa-sign-in';
        break;
    }
  }
}
