import {BaseResource} from '@system/datamodels';

export class SummaryModel extends BaseResource{
  public creatableAttributes = [];
  constructor() {
    super();
    this.setDynamicAttributes();
  }

  public setDynamicAttributes(): void {
    const {leaves_type} = JSON.parse(localStorage.getItem('settings'));
    leaves_type.push('total');

    for (const leaves of leaves_type) {
      this.creatableAttributes.push(leaves);
    }
  }

}
