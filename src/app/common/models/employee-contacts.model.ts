import {BaseResource} from '@system/datamodels';
import {EmployeeContact} from '@common/enums/employeeView.enums';

export class EmployeeContactsModel extends BaseResource {
  public contactType = EmployeeContact;
  public creatableAttributes = [
    'id',
    'number',
    'type',
    'status',
    'created_at',
    'updated_at'
  ];
  public contactTypeIcon: string;
  public contactTypeName: string;

  create(obj: any): EmployeeContactsModel {
    const model = super.create(obj) as EmployeeContactsModel;
    model.generateIconForContactType();
    return model;
  }

  public generateIconForContactType(): void {
    switch (this.type) {
      case this.contactType.Personal: {
        this.contactTypeName = 'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_CONTACTS.PERSONAL_CONTACT_LABEL';
        this.contactTypeIcon = 'far fa-mobile';
        break;
      }
      case this.contactType.Home: {
        this.contactTypeName = 'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_CONTACTS.HOME_CONTACT_LABEL';
        this.contactTypeIcon = 'far fa-home';
        break;
      }
      case this.contactType.Office: {
        this.contactTypeName = 'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_CONTACTS.OFFICE_CONTACT_LABEL';
        this.contactTypeIcon = 'far fa-building';
        break;
      }
    }
  }
}
