import {BaseResource, BaseResourceInterface} from '@system/datamodels';

export class AttendanceReportModel extends BaseResource{
  // public creatableAttributes = [
  //   'employees',
  //   'present',
  //   'leaves',
  //   'absent',
  //   'on_time_login',
  //   'late_login',
  // ];
  public create(obj: any): BaseResourceInterface {
    return super.create(obj, true);
  }
}
