import {BaseResource} from '@system/datamodels';
import {AuthenticatableInterface} from '@system/interfaces';
import {NicoCollection} from '@system/utilities';
import {EmployeeContactsModel} from '@common/models/employee-contacts.model';
import {EmployeeBanksModel} from '@common/models/employee-banks.model';

export class UserModel extends BaseResource implements AuthenticatableInterface {
  public creatableAttributes = [
    'created_at',
    'email',
    'name',
    'first_name',
    'id',
    'last_name',
    'middle_name',
    'status',
    'updated_at',
    'avatar',
    'code',
    'personal_email',
    'position',
    'joined_at',
    'detached_at',
    'gender',
    'dob',
    'marital_status',
    'pan_no',
    'department_id',
    'department',
    'addresses',
    'bank',
    'contacts',
    'social_securities',
    'allocated_leaves',
  ];

  getEmail(): string {
    return this.email;
  }

  getId(): string {
    return this.id;
  }

  create(obj: any): UserModel {
    const model = super.create(obj) as UserModel;
    if (obj.contacts) {
      model.contacts = (new EmployeeContactsModel()).createFromCollection(obj.contacts);
    }
    if (obj.bank) {
      model.bank = (new EmployeeBanksModel()).create(obj.bank);
    }
    return model;
  }

}
