import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'team'
})
export class TeamPipe implements PipeTransform{

  // tslint:disable-next-line:typedef
  transform(value: any){
    if (value){
      return value + ' Team';
    }
    return value;
  }
}
