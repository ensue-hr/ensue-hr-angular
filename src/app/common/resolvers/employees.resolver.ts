import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';

import { Observable} from 'rxjs';
import { EmployeesService} from '@common/services';

@Injectable({
  providedIn: 'root'
})
export class EmployeesResolver implements Resolve<Observable<string>> {
  constructor(public employeesService: EmployeesService) {
  }
  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    return this.employeesService.getDetail(route.params);
  }
}
