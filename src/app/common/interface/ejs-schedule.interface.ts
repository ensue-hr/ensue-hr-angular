export interface EjsScheduleInterface {
  StartTime: Date;
  EndTime: Date;
  WorkHours: string;
  Leave?: string;
}
