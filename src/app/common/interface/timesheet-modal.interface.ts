import {EmployeeModel} from '@common/models';

export interface TimesheetModalInterface {
  employeeId: string;
  selectedDate: string;
  workTime: string;
  breakTime: string;
}
