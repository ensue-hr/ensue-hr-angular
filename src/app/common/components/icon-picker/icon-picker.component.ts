import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {FontAwesomeIconList} from '@common/components/icon-picker/fontAwesomeIconList';
import {NicoUtils} from '@system/utilities';

@Component({
  selector: 'app-icon-picker',
  templateUrl: './icon-picker.component.html',
  styleUrls: ['./icon-picker.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class IconPickerComponent implements OnInit, OnDestroy {

  @Input() isIconPopoverOpen: boolean;
  @Input() showIconName: boolean;
  @Input() selectedIcon: string;
  @Input() iconSize: string;

  @ViewChild('ipSearchInput') searchInputElement: ElementRef;
  @Output() iconSelect: EventEmitter<string> = new EventEmitter<string>();

  public icons: string [] = FontAwesomeIconList;
  public filteredIconList: string [] = [];
  public searchText: string;
  private documentClickListener: any;

  constructor(protected elementRef: ElementRef) {
  }

  public ngOnInit(): void {
    this.searchText = '';
    this.filteredIconList = this.icons;
  }

  public init(): void {
    this.isIconPopoverOpen = NicoUtils.isNullOrUndefined(this.isIconPopoverOpen) ? false : this.isIconPopoverOpen;
    this.showIconName = NicoUtils.isNullOrUndefined(this.showIconName) ? false : this.showIconName;
    this.selectedIcon = NicoUtils.isNullOrUndefined(this.showIconName) ? null : this.selectedIcon;
    this.iconSize = NicoUtils.isNullOrUndefined(this.iconSize) ? '1rem' : this.iconSize;
  }

  public onIconInputFocus($event): any {
    if (this.isIconPopoverOpen) {
      return;
    }
    $event.stopPropagation();
    this.registerDocumentClick();
    this.isIconPopoverOpen = true;
    setTimeout(() => {
      this.searchInputElement.nativeElement.focus();
    }, 10);
  }

  public onIconSelection($event, icon?: string): void {
    $event.stopPropagation();
    $event.preventDefault();
    this.closeIconPickerDropdown();
    if (NicoUtils.isNullOrUndefined(icon)) {
      this.iconSelect.emit(null);
    } else {
      this.selectedIcon = icon;
      this.iconSelect.emit(this.selectedIcon);
    }
  }

  public onIconSearch(): void {
    if (this.searchText === '') {
      this.filteredIconList = this.icons;
    } else {
      this.filteredIconList = this.icons.filter(icon => icon.indexOf(this.searchText) > -1);
    }
  }

  private registerDocumentClick(): void {
    this.documentClickListener = (event: any) => {
      if (!this.elementRef.nativeElement.contains(event.target)) {
        this.closeIconPickerDropdown();
      }
    };
    document.addEventListener('click', this.documentClickListener);
  }

  public closeIconPickerDropdown(): void {
    this.isIconPopoverOpen = false;
    document.removeEventListener('click', this.documentClickListener);
  }

  public ngOnDestroy(): void {
    document.removeEventListener('click', this.documentClickListener);
  }
}
