import {Component, Input, OnInit} from '@angular/core';
import {NicoUtils} from '@system/utilities';
import * as moment from 'moment';

@Component({
  selector: 'app-notification-row',
  templateUrl: './notification-row.component.html',
  styleUrls: ['./notification-row.component.css']
})

export class NotificationRowComponent implements OnInit {

  @Input() notificationReadStatus: boolean;
  @Input() type: 'success' | 'danger' | 'warning' | 'info';
  @Input() item = {
    title: 'Notification Title',
    subtitle: 'Subtitle here',
    message: 'Some message here...',
    dateTime: moment('12-Feb-2020 18:11:22')
  };

  constructor() {
  }

  ngOnInit(): void {
    this.type = NicoUtils.isNullOrUndefined(this.type) ? 'info' : this.type;
    this.notificationReadStatus = NicoUtils.isNullOrUndefined(this.notificationReadStatus) ? false : this.notificationReadStatus;
  }
}

