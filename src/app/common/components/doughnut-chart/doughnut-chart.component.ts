import {Component, Input, OnInit} from '@angular/core';
import {NicoUtils} from '@system/utilities';

@Component({
  selector: 'app-doughnut-chart',
  templateUrl: './doughnut-chart.component.html',
  styleUrls: ['./doughnut-chart.component.scss']
})
export class DoughnutChartComponent implements OnInit {

  @Input() fillValue: number;
  @Input() value: string;
  @Input() label: string;
  @Input() color: 'primary' | 'accent' | 'success' | 'danger' | 'warning' | 'info';

  constructor() { }

  ngOnInit(): void {
    this.fillValue = NicoUtils.isNullOrUndefined(this.fillValue) ? 0 : this.fillValue;
    this.value = NicoUtils.isNullOrUndefined(this.value) ? 'Value' : this.value;
  }
}
