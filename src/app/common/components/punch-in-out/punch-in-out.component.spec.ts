import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';
import {PunchInOutComponent} from './punch-in-out.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {HttpClient, HttpHandler} from '@angular/common/http';
import {AttendanceService, UserDetailService} from '@common/services';
import {MockUserDetailInfoService} from '@common/testing-resources/mock-services';
import {MockAttendanceService} from '@common/testing-resources/mock-services/MockAttendanceService';
import {DateUtility} from '@common/utilities/date-utility';
import {AttendanceDetailModel, UserModel} from '@common/models';
import {IpAddressService} from '@common/services/ip-address.service';
import {PunchInAttendance} from './punch-in-attendance.interface';


describe('PunchInOutComponent', () => {
  let component: PunchInOutComponent;
  let fixture: ComponentFixture<PunchInOutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PunchInOutComponent],
      providers: [
        ...MOCK_PROVIDERS,
        HttpClient,
        HttpHandler,
        IpAddressService,
        {provide: UserDetailService, useClass: MockUserDetailInfoService},
        {provide: AttendanceService, useClass: MockAttendanceService}
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        ReactiveFormsModule
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PunchInOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get Employee Details on init', fakeAsync(async () => {
    spyOn(component, 'getEmployeeDetails');
    component.onComponentReady();
    fixture.whenStable();
    expect(component.getEmployeeDetails).toHaveBeenCalled();
    flush();
  }));

  it('should get attendance data on init', fakeAsync(async () => {
    spyOn(component, 'getModels');
    await fixture.detectChanges();
    await component.getEmployeeDetails();
    await fixture.whenStable();
    expect(component.getModels).toHaveBeenCalled();
  }));

  it('should set check in time if the user has already checked in', () => {
    component.setCheckInTime('2021-09-29 08:39:37');
    fixture.detectChanges();
    expect(component.checkInTime).toEqual(DateUtility.utcToLocalFormat('2021-09-29 08:39:37', 'HH:mm'));
  });

  it('should show time in hours in the chart (html)', () => {
    spyOn(component, 'calculateTimeFromCheckIn');
    component.setCheckInTime('2021-09-29 08:39:37', '2021-09-29 18:39:37');
    fixture.detectChanges();
    expect(component.calculateTimeFromCheckIn).toHaveBeenCalled();
  });

  it('should set punch in type depending upon the last user status', () => {
    const attendanceArray: AttendanceDetailModel[] = [
      {
        id: 10,
        type: 'check_in',
        browser: 'Chrome',
        device: null,
        location: '27.7172453 : 85.3239605',
        ip: '27.34.68.46',
        attend_at: '2021-09-29 08:39:37',
        reason: null,
        status: 2,
        created_at: '2021-09-29T08:39:37.000000Z',
        updated_at: '2021-09-29T08:39:37.000000Z',
        parents: []
      }] as unknown as AttendanceDetailModel[];
    component.setPunchInType(attendanceArray);
    expect(component.checkIn).toEqual(false);
    expect(component.breakIn).toEqual(false);
  });

  it('should add punch-in activity', fakeAsync (async () => {
    spyOn(component, 'getModels');
    component.attendanceData = {id: 1} as unknown as PunchInAttendance;
    await fixture.detectChanges();
    await fixture.whenStable();
    await component.postAttendanceActivity('check_out');
    fixture.whenStable();
    expect(component.getModels).toHaveBeenCalled();
  }));

  it('should show error in case of error in posting punch-in data', fakeAsync (async () => {
    spyOn(component, 'showErrorSnackBar');
    component.attendanceData = {id: 1} as unknown as PunchInAttendance;
    await fixture.detectChanges();
    await fixture.whenStable();
    await component.postAttendanceActivity('check_in');
    fixture.whenStable();
    expect(component.showErrorSnackBar).toHaveBeenCalled();
  }));

  it ('should get location if available', fakeAsync (async () => {
    spyOn(component, 'storeLocationInformation');
    component.employeeModel = {id: 1} as unknown as UserModel;
    await fixture.detectChanges();
    await fixture.whenStable();
    await component.getModels();
    expect(component.storeLocationInformation).toHaveBeenCalled();
  }));

  it('should show break in button in case of a user breaking out', () => {
    component.checkIn = false;
    fixture.detectChanges();
    component.breakIn = true;
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('button');
    expect(button.innerHTML).toEqual('MOD_DASHBOARD.MOD_ATTENDANCE.MOD_PUNCH_IN_OUT.BREAK_IN_BUTTON_LABEL');
  });

  it('should show break in button in case of a user breaking in', () => {
    component.checkIn = false;
    fixture.detectChanges();
    component.breakIn = false;
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('button');
    expect(button.innerHTML).toEqual('MOD_DASHBOARD.MOD_ATTENDANCE.MOD_PUNCH_IN_OUT.BREAK_OUT_BUTTON_LABEL');
  });

  it('should show check out button after checking in', () => {
    component.checkIn = false;
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelectorAll('button')[1];
    expect(button.innerHTML).toEqual('MOD_DASHBOARD.MOD_ATTENDANCE.MOD_PUNCH_IN_OUT.CHECK_OUT_BUTTON_LABEL');
  });

  it('should hide all buttons after checking out', () => {
    component.checkIn = true;
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('button');
    expect(button).toBeDefined();
  });

  it('should change chart color to warning in case of overtime', () => {
    component.setChartColor(8.5);
    expect(component.chartColor).toEqual('warning');
  });

  it('should set chart color', () => {
    component.setChartColor(8);
    expect(component.chartColor).toEqual('primary');
  });

  it('should set chart color to danger in case of underwork', () => {
    component.setChartColor(6);
    expect(component.chartColor).toEqual('danger');
  });

});
