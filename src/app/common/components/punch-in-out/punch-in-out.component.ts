import {Component, Injector, Input, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {AttendanceDetailService, AttendanceService, UserDetailService} from '@common/services';
import {AttendanceDetailModel, UserModel} from '@common/models';
import {HttpParams} from '@angular/common/http';
import {DateUtility} from '@common/utilities/date-utility';
import {NicoUtils, PaginatedNicoCollection} from '@system/utilities';
import {AttendanceType} from '@common/enums/attendanceType.enum';
import {ClientJS} from 'clientjs';
import {PunchInAttendance} from './punch-in-attendance.interface';
import {AbstractBaseComponent} from '../../../AbstractBaseComponent';

@Component({
  selector: 'app-punch-in-out',
  templateUrl: './punch-in-out.component.html',
  styleUrls: ['./punch-in-out.component.scss']
})
export class PunchInOutComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  @Input() showCurrentDate = true;

  /**
   * Enum
   */
  public attendanceEnum = AttendanceType;

  /**
   * Employee id
   */
  public employeeModel: UserModel;

  /**
   * Attendance model
   */
  public attendanceData: PunchInAttendance = {type: '', ip: '', location: '', device: '', browser: '', fingerprint: ''};

  public attendanceModel: PaginatedNicoCollection<AttendanceDetailModel>;

  public checkIn = true;

  public breakIn: boolean;

  public type: string;

  public today = new Date();

  public checkInTime: string;

  public leaveInModel: AttendanceDetailModel;

  public leaveOutModel: AttendanceDetailModel;

  public isLeave = false;

  public isCheckedOut = false;

  /**
   * Chart variables
   */
  public timerInHours = '';

  public fillValueForChart: number;

  public chartColor: 'primary' | 'accent' | 'success' | 'danger' | 'warning' | 'info' = 'primary';

  /**
   * Subscriptions
   */
  public punchInOutSubscription: Subscription;

  public employeeSubscription: Subscription;

  public attendanceChangeSubscription: Subscription;

  public intervalInc: number;

  /**
   * Constructor
   */
  constructor(protected injector: Injector,
              public attendanceService: AttendanceService,
              public attendanceDetailService: AttendanceDetailService,
              public userDetailService: UserDetailService) {
    super(injector);
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.getEmployeeDetails();
    this.attendanceChangeSubscription = this.attendanceService.watchObservableChange().subscribe(response => {
      this.getModels();
    });
  }

  public getEmployeeDetails(): void {
    this.employeeSubscription = this.userDetailService.get().subscribe(employee => {
      this.employeeModel = employee;
      this.getModels();
    // }, error => {
    //   if (error.error) {
    //     this.showErrorSnackBar(error.error.message);
    //   }
    });
  }

  /**
   * Get Models
   */
  public getModels(): void {
    let params: HttpParams = new HttpParams();
    params = params.set('employee_id', this.employeeModel?.id);
    params = params.set('date', String(DateUtility.localToUTCFormat(new Date(), 'YYYY-MM-DD')));
    this.punchInOutSubscription = this.attendanceDetailService.getEmployeeAttendance(params).subscribe(attendanceModel => {
      this.attendanceModel = attendanceModel;
      this.storeLocationInformation();
      if (this.attendanceModel.length > 0) {
        this.checkForALeave();
        this.checkIfCheckedIn();
        this.setPunchInType(this.attendanceModel.all());
        const isCheckOut = this.attendanceModel.first(checkOutModel => checkOutModel.type === this.attendanceEnum.CheckOut);
        isCheckOut ?
          this.setCheckInTime(this.attendanceModel.first(attendanceDetail => attendanceDetail.type === this.attendanceEnum.CheckIn).attend_at,
            this.attendanceModel.first(attendanceDetail => attendanceDetail.type === this.attendanceEnum.CheckOut).attend_at) :
          this.setCheckInTime(this.attendanceModel.first(attendanceDetail => attendanceDetail.type === this.attendanceEnum.CheckIn)?.attend_at);
        if (isCheckOut) {
          this.checkIn = false;
        }
      } else {
        this.timerInHours = '0 hrs';
      }
    // }, error => {
    //   this.showErrorSnackBar(error.error.message);
    });
  }

  /**
   * Check if the user has checked in
   */
  public checkIfCheckedIn(): void {
    this.checkIn = NicoUtils.isNullOrUndefined(this.attendanceModel.first(checkIn => checkIn.type === this.attendanceEnum.CheckIn));
  }

  /**
   * Check if a leave is taken
   */
  public checkForALeave(): void {
    this.leaveInModel = this.attendanceModel.first(leaveIn => leaveIn.type === this.attendanceEnum.LeaveIn);
    this.leaveOutModel = this.attendanceModel.first(leaveOut => leaveOut.type === this.attendanceEnum.LeaveOut);
    this.isLeave = DateUtility.getDateBetween(this.leaveInModel?.attend_at, this.leaveOutModel?.attend_at, DateUtility.getMomentDate());
  }

  /**
   * Set punch-in type and check-in time for timer
   */
  public setCheckInTime(checkInTime?: string, checkOutTime?: string): void {
    this.isCheckedOut = !(NicoUtils.isNullOrUndefined(checkOutTime));
    clearInterval(this.intervalInc);
    this.checkInTime = DateUtility.utcToLocalFormat(checkInTime, 'HH:mm');
    const checkIn = Date.parse(DateUtility.utcToLocalFormat(checkInTime));
    if (!checkOutTime) {
      this.intervalInc = setInterval(() => {
        this.calculateTimeFromCheckIn(checkIn, Date.now());
      }, 1000);
    } else {
      this.calculateTimeFromCheckIn(checkIn, Date.parse(DateUtility.utcToLocalFormat(checkOutTime)));
    }
  }

  /**
   * Calculate time from check in
   */
  public calculateTimeFromCheckIn(startTime: number, checkOutTime: number): void {
    const timer = checkOutTime - startTime;
    const hours = timer / 3600000;
    this.timerInHours = String((Math.round(hours * 2) / 2).toFixed(1));
    this.fillValueForChart = Number(this.timerInHours) * 100 / 8;
    this.formatTimerString();
  }

  /**
   * Format time string to be displayed in the chart
   */
  public formatTimerString(): void {
    this.timerInHours = this.timerInHours.split('.')[1] === '0' ?
      this.timerInHours.split('.')[0] :
      this.timerInHours;
    this.setChartColor(Number(this.timerInHours));
    this.timerInHours += this.translateService.instant('MOD_DASHBOARD.MOD_ATTENDANCE.MOD_PUNCH_IN_OUT.HRS_LABEL');
  }

  /**
   * Set chart color according to hours spent
   */
  public setChartColor(timerInHours: number): void {
    this.chartColor = timerInHours > 8 ? 'warning' : this.checkIn ? timerInHours === 8 ? 'primary' : 'danger' : 'primary';
  }

  /**
   * Set punch-in type for displaying respective in html
   */
  public setPunchInType(attendanceData: AttendanceDetailModel[]): void {
    switch (attendanceData[attendanceData.length - 1].type) {
      case this.attendanceEnum.CheckIn: {
        this.breakIn = false;
        this.checkIn = false;
        break;
      }
      case this.attendanceEnum.CheckOut: {
        this.breakIn = false;
        this.checkIn = true;
        break;
      }
      case this.attendanceEnum.BreakIn: {
        this.breakIn = false;
        this.checkIn = false;
        break;
      }
      case this.attendanceEnum.BreakOut: {
        this.breakIn = true;
        this.checkIn = false;
        break;
      }
    }
  }

  /**
   * Add punch-in activity
   */
  public postAttendanceActivity(attendanceType: string): void {
    const client = new ClientJS();
    this.attendanceData.type = attendanceType;
    this.attendanceData.browser = client.getBrowser();
    this.attendanceData.device = client.getDevice();
    this.attendanceData.fingerprint = client.getFingerprint();
    this.punchInOutSubscription = this.attendanceService.post(this.attendanceData).subscribe(() => {
      // this.getModels();
      this.attendanceService.setattendanceChange();
    }, error => {
      this.showErrorSnackBar(error.error.message);
    });
  }

  /**
   * Set location information
   */
  public storeLocationInformation(): void {
    const promise = new Promise(resolve => {
      resolve(this.getLocation());
    });
    promise.catch(error => {
      this.showErrorSnackBar(error);
    });
  }

  /**
   * Get location
   */
  public getLocation(): void {
    let location;
    navigator.geolocation.getCurrentPosition(position => {
      location = position.coords.latitude + ',' + position.coords.longitude;
      this.attendanceData.location = location;
    });
  }

  /**
   * On Destroy
   */
  public ngOnDestroy(): void {
    if (this.punchInOutSubscription != null) {
      this.punchInOutSubscription.unsubscribe();
    }
    if (this.attendanceChangeSubscription != null) {
      this.attendanceChangeSubscription.unsubscribe();
    }
  }
}

