import {Component, EventEmitter, HostBinding, Input, OnInit, Output} from '@angular/core';
import {NicoUtils} from '@system/utilities';

@Component({
  selector: 'app-pill',
  template: `
    <span
      class="pill {{'pill-'+backgroundType}} {{isHoverEnabled ? 'with-hover-style' : ''}} {{isActive ? 'active' : ''}} {{className ? className : ''}}"
      [ngClass]="'pill-'+color +' '+ 'size-'+size">
      <ng-container *ngIf="!isCustomContent">
        <span>{{label}}</span>
        <span class="remove-icon" *ngIf="removable" (click)="onRemoveClick($event)">
          <i class="font-icon far fa-times"></i>
        </span>
      </ng-container>
      <ng-template [ngIf]="isCustomContent">
        <ng-content></ng-content>
      </ng-template>
    </span>
  `,
  styles: [`:host {
    display: inline-flex;
  }`]
})
export class PillComponent implements OnInit {

  @HostBinding('class.app-pill-element') add = true;
  /**
   * Label for pill
   */
  @Input() label: string;
  /**
   * Defines the hover style should be applied on the pill component
   */
  @Input() isHoverEnabled: boolean;
  /**
   * Defines the active status for the pill
   */
  @Input() isActive: boolean;
  /**
   * Background color for pill
   */
  @Input() color: 'light' | 'success' | 'danger' | 'warning' | 'info' | string;
  /**
   * Background type
   */
  @Input() backgroundType: 'translucent' | 'solid';
  /**
   * Determines the visibility for close option on pill
   */
  @Input() removable: boolean;
  /**
   * Size option for pill
   */
  @Input() size: 'default' | 'medium' | 'small';
  /**
   * Custom content
   */
  @Input() isCustomContent: boolean;

  @Input() className: string;

  @Output() remove: EventEmitter<void>;

  constructor() {
    this.remove = new EventEmitter<void>();
  }

  ngOnInit(): void {
    this.label = NicoUtils.isNullOrUndefined(this.label) ? '' : this.label;
    this.color = NicoUtils.isNullOrUndefined(this.color) ? 'light' : this.color;
    this.backgroundType = NicoUtils.isNullOrUndefined(this.backgroundType) ? 'translucent' : this.backgroundType;
    this.removable = NicoUtils.isNullOrUndefined(this.removable) ? false : this.removable;
    this.size = NicoUtils.isNullOrUndefined(this.size) ? 'default' : this.size;
    this.isHoverEnabled = !NicoUtils.isNullOrUndefined(this.isActive) ?
      true : (NicoUtils.isNullOrUndefined(this.isHoverEnabled) ? false : this.isHoverEnabled);
    this.isActive = NicoUtils.isNullOrUndefined(this.isActive) ? false : this.isActive;
    this.isCustomContent = NicoUtils.isNullOrUndefined(this.isCustomContent) ? false : this.isCustomContent;
    this.className = NicoUtils.isNullOrUndefined(this.className) ? '' : this.className;
  }

  public onRemoveClick(event: MouseEvent): void {
    event.preventDefault();
    event.stopPropagation();
    this.remove.emit();
  }
}
