import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {NicoUtils} from '@system/utilities';

@Component({
  selector: 'app-avatar',
  template: `
    <span class="avatar {{avatarClassName}}" [style]="avatarStyles">
      <img *ngIf="imageLink" [src]="imageLink" alt="profile avatar" class="avatar-img">
      <span *ngIf="username && !imageLink" class="avatar-initials">{{userInitials}}</span>
    </span>
  `,
  styles: [`:host {
    display: inline-block;
  }`]
})

export class AvatarComponent implements OnInit, OnChanges {
  /**
   * Avatar image's source / link
   */
  @Input() imageLink?: string;
  /**
   * Custom additional classname for avatar (eg: 'my-avatar'})
   */
  @Input() avatarClassName?: string;
  /**
   * Custom styles for avatar (eg: {borderRadius: '0.75rem'})
   */
  @Input() avatarStyles?: any;
  /**
   * Username for avatar display
   */
  @Input() username?: string;

  public userInitials: string;

  constructor() {
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (!NicoUtils.isNullOrUndefined(this.username)) {
      const nameArr: Array<string> = this.username.split(' ');
      this.userInitials = (nameArr[0].charAt(0) + (nameArr.length > 1 ? nameArr[nameArr.length - 1].charAt(0) : '')).toUpperCase();
    }
  }

  public ngOnInit(): void {
    this.imageLink = NicoUtils.isNullOrUndefined(this.imageLink) ? null : this.imageLink;
    this.avatarClassName = NicoUtils.isNullOrUndefined(this.avatarClassName) ? '' : this.avatarClassName;
    this.avatarStyles = NicoUtils.isNullOrUndefined(this.avatarStyles) ? {} : this.avatarStyles;
  }
}
