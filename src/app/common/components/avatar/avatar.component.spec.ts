import {ComponentFixture, TestBed} from '@angular/core/testing';
import {AvatarComponent} from './avatar.component';

describe('Avatar Component ', () => {
  let component: AvatarComponent;
  let fixture: ComponentFixture<AvatarComponent>;
  let element: Element;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AvatarComponent]
    }).compileComponents().then();
    fixture = TestBed.createComponent(AvatarComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
  });
  it('should create avatar component', () => {
    expect(component).toBeTruthy();
  });

  // it('should set imageLink to fallback Image when input is not given', () => {
  //   fixture.detectChanges();
  //   expect(component.imageLink).toEqual(component.fallbackImage);
  // });

  it('should set avatarClassName to an element in the template', () => {
    component.avatarClassName = 'test-class';
    fixture.detectChanges();
    expect(element.querySelector('.test-class')).not.toBeNull();
  });

  it('should set the span style to the style avatarStyles', () => {
    component.avatarStyles = {height: '30px'};
    fixture.detectChanges();
    const span = element.querySelector('span');
    expect(span.style.height).toEqual('30px');
  });

});
