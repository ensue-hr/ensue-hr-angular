import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-user-pill',
  templateUrl: './user-pill.component.html',
  styles: [`
    :host {
      display: inline-flex;
    }
  `]
})

export class UserPillComponent implements OnInit {

  @Input() userData: UserAvatarPillInterface;

  constructor() {
  }

  ngOnInit(): void {

  }
}


export interface UserAvatarPillInterface {
  value: string;
  imageLink: string;
  pillSize?: 'default' | 'medium' | 'small';
}
