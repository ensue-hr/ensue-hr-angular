import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeclineModalComponent } from './decline-modal.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {ActivatedRoute} from '@angular/router';

import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MockMatDialog} from '@common/testing-resources/mock-services';
import {MockMatDialogRef} from '@common/testing-resources/mock-services/MockMatDialogRef';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {NgxMatDatetimePickerModule, NgxMatNativeDateModule} from '@angular-material-components/datetime-picker';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {LeaveModel} from '@common/models/leave.model';
import {LeaveService} from '@common/services/leave.service';
import {MockLeaveService} from '@common/testing-resources/mock-services/MockLeaveService';
import {NicoStorageService} from '@system/services';

describe('DeclineModalComponent', () => {
  let component: DeclineModalComponent;
  let fixture: ComponentFixture<DeclineModalComponent>;
  let storageService: NicoStorageService;
  let service: LeaveService;
  const mockData = {
    avatar: undefined,
    code: undefined,
    created_at: '2021-09-17T06:54:35.000000Z',
    days: 3,
    department: undefined,
    description: 'mero khutta bacyo',
    email: undefined,
    employee: {id: 1, name: 'Manish Ratna Shakya', email: 'manish.shakya@ensue.us',
      avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7w-VUkvVobuC_P3TImDJalLsfOGGijkNRmA&usqp=CAU', code: 'EN200701'},
    end_at: '2021-11-23 18:14:59',
    id: '9',
    is_emergency: false,
    name: undefined,
    requested: {id: 20, name: 'Angel Ratna Roman', email: 'angel@gmail.com', avatar: null, code: 'EN202106'},
    start_at: '2021-11-20 18:15:00',
    status: 1,
    title: 'khutta bhachyo',
    type: 'sick',
    updated_at: '2021-09-17T06:54:35.000000Z',
  } as unknown as LeaveModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeclineModalComponent ],
      providers: [
        ...MOCK_PROVIDERS,
        {
          provide: ActivatedRoute,
          useValue:
            {
              snapshot: {
                queryParams: {
                  page: 1, sort_order: 'asc', per_page: 16
                },
                parent: {
                  params: {
                    id: 1
                  }
                }
              }
            }
        },
        {provide: LeaveService, useClass: MockLeaveService},
        {provide: MatDialog, useClass: MockMatDialog},
        {provide: MatDialogRef, useClass: MockMatDialogRef},
        {provide: MAT_DIALOG_DATA, useValue: {model: mockData}},
      ],
      imports: [
        TranslateModule,
        RouterTestingModule,
        ReactiveFormsModule,
        NgxMatDatetimePickerModule,
        NgxMatNativeDateModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeclineModalComponent);
    storageService = TestBed.inject(NicoStorageService);
    service = TestBed.inject(LeaveService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should set page title to leave details', () => {
    component.ngOnInit();
    expect(component.pageTitle).toEqual('MOD_LEAVE.LEAVE_DETAIL_LABEL');
  });

  it('should have reason to post decline form', () => {
    const submitButton: DebugElement =
      fixture.debugElement.query(By.css('button[type=submit]'));
    submitButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(submitButton.attributes.disabled).toBe('');
  });

  it('should post data on save ', () => {
      const dummyData = 'hello this is just a test';
      service.onCancelDeclineLeave(mockData, dummyData, true).subscribe(response => {
          expect(response.message).toEqual(dummyData);
      });
  });

  it('should call on submit on click save ', () => {
    spyOn(component, 'onConfirmDecline');
    const submitButton: DebugElement =
      fixture.debugElement.query(By.css('button[type=submit]'));
    submitButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.onConfirmDecline).toHaveBeenCalled();
  });

});
