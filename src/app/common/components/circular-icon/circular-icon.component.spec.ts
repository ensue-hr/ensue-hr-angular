import {ComponentFixture, TestBed} from '@angular/core/testing';
import {CircularIconComponent} from './circular-icon.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
// @ts-ignore
import {IconEnum} from '@common/constants';

describe('Circular icon component', () => {
  let component: CircularIconComponent;
  let fixture: ComponentFixture<CircularIconComponent>;
  let element: Element;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CircularIconComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents().then();
    fixture = TestBed.createComponent(CircularIconComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
  });

  it('should create circular icon component', () => {
    expect(component).toBeTruthy();
  });

  it('should have config property defined', () => {
    expect(component.config).toBeDefined();
  });

  it('should have default config property set', () => {
    fixture.detectChanges();
    expect(component.config).toEqual({
      containerClassName: '',
      iconName: 'far fa-search',
      color: 'accent',
      withIndicator: false,
      className: ''
    });
  });

  // it('should have className set for outer element as color and size', () => {
  //   fixture.detectChanges();
  //   expect(element.querySelector('.s32')).not.toBeNull();
  //   expect(element.querySelector('.default')).not.toBeNull();
  // });

  it('should not show indicator when config.withIndicator is false', () => {
    fixture.detectChanges();
    expect(element.querySelector('.indicator')).toBeNull();
    component.config.withIndicator = true;
    fixture.detectChanges();
    expect(element.querySelector('.indicator')).not.toBeNull();
  });

  it('should have element for an icon', () => {
    fixture.detectChanges();
    expect(element.querySelector('.font-icon')).not.toBeNull();
  });

});
