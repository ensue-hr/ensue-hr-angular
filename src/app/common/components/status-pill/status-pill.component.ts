import {Component, Input, OnInit} from '@angular/core';
import {Status} from '@common/enums/status.enums';

@Component({
  selector: 'app-status-pill',
  templateUrl: './status-pill.component.html',
  styleUrls: ['./status-pill.component.scss']
})
export class StatusPillComponent implements OnInit {

  @Input() status: any;
  public genericStatus = Status;

  constructor() { }

  ngOnInit(): void {
  }

}
