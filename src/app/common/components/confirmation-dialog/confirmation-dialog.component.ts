import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ConfirmationDialogInterface} from '@common/components/confirmation-dialog/confirmation-dialog.interface';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-confirm-delete',
  templateUrl: './confirmation-dialog.component.html',
})
export class ConfirmationDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ConfirmationDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ConfirmationDialogInterface,
              protected translateService: TranslateService
  ) { }


  public ngOnInit(): void {
    this.data.title = this.data?.title ?? this.translateService.instant('MOD_COMMON.MOD_CONFIRMATION_DIALOG.TITLE');
    this.data.message = this.data?.message ?? this.translateService.instant('MOD_COMMON.MOD_CONFIRMATION_DIALOG.MESSAGE');
  }

  public closeModal(apply: boolean): void {
    this.dialogRef.close(apply);
  }
}
