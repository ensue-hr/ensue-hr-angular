import {ComponentFixture, TestBed} from '@angular/core/testing';
import {BadgeComponent} from './badge.component';

describe('Badge component', () => {
  let component: BadgeComponent;
  let fixture: ComponentFixture<BadgeComponent>;
  let element: Element;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BadgeComponent]
    }).compileComponents().then();
    fixture = TestBed.createComponent(BadgeComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
  });

  it('should create badge component', () => {
    expect(component).toBeTruthy();
  });

  it('should have the component.label in the template somewhere', () => {
    component.label = 'Test Text';
    fixture.detectChanges();
    expect(element.textContent).toContain(component.label);
  });

  it('should have template contain the color as class', () => {
    component.color = 'primary';
    fixture.detectChanges();
    expect(element.querySelector('span').className).toContain('badge-' + component.color);
  });

});
