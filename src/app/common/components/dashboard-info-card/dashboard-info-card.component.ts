import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-dashboard-info-card',
  templateUrl: './dashboard-info-card.component.html',
  styleUrls: ['./dashboard-info-card.component.scss']
})
export class DashboardInfoCardComponent implements OnInit {

  @Input() icon = 'far fa-circle';
  @Input() color: 'primary' | 'accent' | 'success' | 'danger' | 'warning' | 'info' = 'primary';
  @Input() label = 'Label';
  @Input() value = 'Value';

  constructor() {
  }

  ngOnInit(): void {
  }
}

