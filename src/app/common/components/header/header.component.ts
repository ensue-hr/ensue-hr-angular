import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NicoSessionService} from '@system/services';
import {FlyMenuItemInterface} from '@system/components';
import {Router} from '@angular/router';
import {AppService} from '@common/services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() sidenavOpen: EventEmitter<boolean> = new EventEmitter<boolean>();
  public lightTheme: boolean;
  public user: any;

  constructor(protected session: NicoSessionService, protected router: Router, protected appService: AppService) {
  }

  ngOnInit(): void {
    this.user = this.session.getUser();
    this.lightTheme = this.appService.isLightMode();
  }

  /**
   * Toggle Light/Dark Theme
   */
  public onThemeToggle(): void {
    this.lightTheme = !this.lightTheme;
    this.appService.enableLightMode(this.lightTheme);
  }

  /**
   * Toggle Sidenav
   */
  public toggleSidenav($event): void {
    $event.stopPropagation();
    this.sidenavOpen.emit(true);
  }

  /**
   * Add menu items on action fly menu
   */
  public getAddOnMenuItems(): FlyMenuItemInterface[] {
    return [
      {name: 'user_profile', label: 'User Profile', active: true},
      {name: 'logout', label: 'Logout', active: true}
    ];
  }

  /**
   * For Fly menu action
   */
  public onFlyMenuAction(evt: any): void {
    switch (evt) {
      case 'user_profile':
        this.router.navigate(['manage/account']);
        break;
      case 'logout':
        this.router.navigate(['auth/logout']);
        break;
    }
  }
}
