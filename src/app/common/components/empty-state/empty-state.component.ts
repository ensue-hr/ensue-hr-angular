import {Component, Input, OnInit} from '@angular/core';
import {NicoUtils} from '@system/utilities';

@Component({
  selector: 'app-empty-state',
  templateUrl: './empty-state.component.html',
  styleUrls: ['./empty-state.component.css'],
})
export class EmptyStateComponent implements OnInit {
  /**
   * Defines the type for the empty state
   */
  @Input() type: 'default' | 'box' | 'notification' | 'search' | 'chart';
  /**
   * Title for empty state
   */
  @Input() title: string;
  /**
   * Message for empty state
   */
  @Input() message: string;
  /**
   * Custom classname for additional customization
   */
  @Input() className: string;

  constructor() {
  }

  ngOnInit(): void {
    this.type = NicoUtils.isNullOrUndefined(this.type) ? 'default' : this.type;
    this.title = NicoUtils.isNullOrUndefined(this.title) ? null : this.title;
    this.message = NicoUtils.isNullOrUndefined(this.message) ? 'Sorry, no data found' : this.message;
    this.className = NicoUtils.isNullOrUndefined(this.className) ? '' : this.className;
  }
}
