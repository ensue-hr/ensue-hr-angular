import {ComponentFixture, TestBed} from '@angular/core/testing';

import {EmptyStateComponent} from './empty-state.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('EmptyStateComponent', () => {
  let component: EmptyStateComponent;
  let fixture: ComponentFixture<EmptyStateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmptyStateComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmptyStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create empty state component', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize value of type on init', () => {
    expect(component.type).toEqual('default');
  });
});
