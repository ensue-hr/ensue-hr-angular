import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-notification-drawer',
  templateUrl: './notification-drawer.component.html',
  styleUrls: ['./notification-drawer.component.css']
})
export class NotificationDrawerComponent implements OnInit {

  @Output() drawerOpenEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  public closeDrawer($event): void {
    $event.stopPropagation();
    this.drawerOpenEvent.emit(false);
  }
}
