import {Component, Input, OnInit} from '@angular/core';
import {NicoUtils} from '@system/utilities';

@Component({
  selector: 'app-static-spinner',
  templateUrl: './static-spinner.component.html',
})
export class StaticSpinnerComponent implements OnInit {

  /**
   *  CSS specific sizes
   */
  @Input() size: string;

  public styles: string;

  constructor() {
  }

  ngOnInit(): void {
    if (!NicoUtils.isNullOrUndefined(this.size)) {
      this.styles = `height: ${this.size}; width: ${this.size}`;
    }
  }
}
