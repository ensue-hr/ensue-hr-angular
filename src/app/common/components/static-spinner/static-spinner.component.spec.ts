import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticSpinnerComponent } from './static-spinner.component';

describe('StaticSpinnerComponent', () => {
  let component: StaticSpinnerComponent;
  let fixture: ComponentFixture<StaticSpinnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StaticSpinnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticSpinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
