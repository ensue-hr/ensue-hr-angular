import {SideNavComponent} from './side-nav.component';
import {ComponentFixture, TestBed} from '@angular/core/testing';

import {RouterTestingModule} from '@angular/router/testing';
import {BankDetailEmployeeComponent} from '../../../modules/admin-module/pages/employees/components/view-detail-employee/components/bank-detail-employee/bank-detail-employee.component';
import {EmployeeBanksService} from '@common/services';
import {MockEmployeesService} from '@common/testing-resources/mock-services/MockEmployeesService';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {ActivatedRoute} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('SideNavComponent', () => {
  let component: SideNavComponent;
  let fixture: ComponentFixture<SideNavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BankDetailEmployeeComponent ],
      providers: [
        {provide: EmployeeBanksService, useClass: MockEmployeesService},
        ...MOCK_PROVIDERS,
        {
          provide: ActivatedRoute,
          useValue:
            {
              snapshot: {
                queryParams: {
                  page: 1, sort_order: 'asc', per_page: 16
                },
                parent: {
                  params: {
                    id: 1
                  }
                }
              }
            }
        },
      ],
      imports: [
        TranslateModule,
        RouterTestingModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]

    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create sidenav component', () => {
    expect(component).toBeTruthy();
  });

  it('should toggle sidenav', () => {
    const $event = new MouseEvent('click');
    const isOpen = component.isOpen;
    component.toggleSideNav($event);
    expect(component.isOpen).toEqual(!isOpen);
  });
});
