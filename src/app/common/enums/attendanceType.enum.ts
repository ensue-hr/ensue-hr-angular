export enum AttendanceType {
  CheckIn = 'check_in',
  CheckOut = 'check_out',
  BreakIn = 'break_in',
  BreakOut = 'break_out',
  LeaveIn = 'leave_in',
  LeaveOut = 'leave_out'
}
