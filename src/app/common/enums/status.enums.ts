export enum Status{
  Published = 2,
  Unpublished = 1
}

export enum EmployeeStatus {
  Active = 1,
  Inactive = 0
}

export enum LeaveStatus {
  Cancelled= 3,
  Pending = 1,
  Approved = 2,
  Declined = 0,
}
