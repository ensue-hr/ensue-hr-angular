export class MockNicoStorageService {
  public value: any;
  setItem(key, value, params?: any): void {
    this.value = value;
  }
  getItem(key, params?: any): string {
    return this.value;
  }
}
