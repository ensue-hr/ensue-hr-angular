import {defer, Observable} from 'rxjs';

export class MockSettingService {
  public get(params?: any): Observable<any> {
    return defer(() => Promise.resolve([
      {
        id: 6,
        key: 'attendance_employee_update_before_days',
        value: '1',
        type: 'integer',
        created_at: '2021-08-31T04:47:56.000000Z',
        updated_at: '2021-08-31T04:47:56.000000Z'
      },
      {
        id: 5,
        key: 'attendance_employee_update_enable',
        value: '1',
        type: 'boolean',
        created_at: '2021-08-31T04:47:56.000000Z',
        updated_at: '2021-08-31T04:47:56.000000Z'
      },
      {
        id: 3,
        key: 'leaves_can_request_before_days',
        value: '5',
        type: 'integer',
        created_at: '2021-08-31T04:47:55.000000Z',
        updated_at: '2021-08-31T04:47:55.000000Z'
      },
      {
        id: 2,
        key: 'leaves_max_request_days',
        value: '10',
        type: 'integer',
        created_at: '2021-08-31T04:47:55.000000Z',
        updated_at: '2021-08-31T04:47:55.000000Z'
      },
      {
        id: 1,
        key: 'leaves_type',
        value: [
          'sick',
          'casual',
          'maternity',
          'paternity',
          'bereavement',
          'compensatory'
        ],
        type: 'array',
        created_at: '2021-08-31T04:47:55.000000Z',
        updated_at: '2021-08-31T04:47:55.000000Z'
      },
      {
        id: 4,
        key: 'system_fiscal_date_start',
        value: '07-16',
        type: 'string',
        created_at: '2021-08-31T04:47:56.000000Z',
        updated_at: '2021-08-31T04:47:56.000000Z'
      }
    ]));
  }
}

