import {defer, Observable} from 'rxjs';
import {PaginatedNicoCollection} from '@system/utilities';

export class MockAttendanceService {
  public getEmployeeAttendance(params?: any): Observable<PaginatedNicoCollection<any>> {
    return defer(() => Promise.resolve({
      listChangeListeners: {},
      items: [
        {
          id: 10,
          type: 'check_in',
          browser: 'Chrome',
          device: null,
          location: '27.7172453 : 85.3239605',
          ip: '27.34.68.46',
          attend_at: '2021-09-29 08:39:37',
          reason: null,
          status: 2,
          created_at: '2021-09-29T08:39:37.000000Z',
          updated_at: '2021-09-29T08:39:37.000000Z',
          parents: []
        }
      ],
      currentPage: 1,
      nextPageUrl: null,
      perPage: 1
    } as unknown as PaginatedNicoCollection<any>));
  }

  public setattendanceChange(): void {
  }

  public watchObservableChange(): Observable<any> {
    return defer(() => Promise.resolve());
  }

  public post(params: any): Observable<any> {
    if (params.type === 'check_out') {
      return defer(() => Promise.resolve({}));
    } else {
      return defer(() => Promise.reject({error: {message: 'undefined'}}));
    }
  }

  public editTimeLog(params: any): Observable<any> {
    if (params.reason === 'test') {
      return defer(() => Promise.resolve({}));
    } else {
      return defer(() => Promise.reject({error: {message: 'undefined'}}));
    }
  }

  public getAttendanceSummary(params?: any): Observable<any> {
    return defer(() => Promise.reject({error: {message: 'undefined'}}));
  }
}
