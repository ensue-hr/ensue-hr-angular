export class MockSearchService {
  public toggleCollapsedView(evt: MouseEvent): void {
  }

  public onSelectItemPick(evt: any): void {
  }

  public onSearchValueChange(): void {
  }

  public onSearchTextClear(): void {
  }

  public onSortByChange(): void {
  }

  public onStatusChange(): void {
  }

  public toggleSortOrder($event, order: 'asc' | 'desc'): void {

  }

  public onToggleView($event): void {

  }
}
