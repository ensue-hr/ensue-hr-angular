import {defer, Observable} from 'rxjs';

export class MockFormBuilder {

  public controls = {
    username: '',
    password: '',
    setValue(str): string {
      return str;
    }
  };

  public group(params?: any): {} {
    return {username: '', password: ''};
  }

  public createForm(): void {}

  public getRawValue(): Observable<any> {
    return defer(() => Promise.resolve({}));
  }

  public setValue(str): string {
    return str;
  }

}
