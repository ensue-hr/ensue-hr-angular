import {defer, Observable} from 'rxjs';

export class MockLoginService {
  public post(params?: any): Observable<any> {
    if (params.username === 'validUsername') {
      return defer(() => Promise.resolve({
        status: '200'
      }));
    } else {
      return defer(() => Promise.reject({}));
    }
  }

  public put(params?: any): Observable<any> {
    return defer(() => Promise.resolve( {
      user: 'present'
    }));
  }
}
