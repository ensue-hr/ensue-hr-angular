import {Observable, of} from 'rxjs';
import {Params} from '@angular/router';

export class MockActivatedRouteService {
  public queryParams: Observable<Params> = of({email: 'value', token: 'test'});
}
