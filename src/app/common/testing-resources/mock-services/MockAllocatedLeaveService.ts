import {defer, Observable} from 'rxjs';
import {AllocatedLeaveModel} from '@common/models/allocated-leaves.model';

export class MockAllocatedLeaveService {
  mockData = {
    created_at: '2021-08-24T09:05:28.000000Z',
    days: 10,
    end_date: '2023-07-15',
    id: 31,
    start_date: '2022-07-16',
    status: 1,
    title: 'test1',
    type: 'casual',
    updated_at: '2021-08-24T09:05:28.000000Z'} as unknown as AllocatedLeaveModel;

  public post(body: any, params?: any): Observable<any> {
    this.mockData.push(body);
    return defer(() => Promise.resolve(this.mockData));
  }
  public saveAllocatedLeave(id: string, body: any, params?: any): Observable<any> {
    return defer(() => Promise.resolve(this.mockData));
  }

  public getAllocatedLeaves(): Observable<any>{
    return defer(() => Promise.resolve(this.mockData));
  }
  public put(params?: any): Observable<any> {
    return defer(() => Promise.resolve({
      user: 'present'
    }));
  }

  public toggleStatusAllocatedLeave(id: string, model: AllocatedLeaveModel, params?: any): Observable<any> {
    return defer(() => Promise.resolve( {
      data: this.mockData
    }));
  }

  public delete(occasion: any): Observable<any>{
    return defer(() => Promise.resolve({
      body: 'success'
    }));
  }


}
