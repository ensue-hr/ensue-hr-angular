import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export class HrValidators {
  /**
   * To ensure password is minimum 8 characters long, has at least one number, one lowercase letter, one uppercase
   * letter and one special character.
   */
  public static validatePassword(control: AbstractControl): any {
    const passwordRegex: RegExp = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,}$/;
    const testPasswordAgainstRegex = passwordRegex.test(control.value);
    return testPasswordAgainstRegex ? null : {badPassword: {value: control.value}};
  }

  /**
   * To check if two given input fields match
   */
  public static fieldsMatch(against: AbstractControl): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return control.value === against.value ? null : {fieldMismatch: {value: control.value}};
    };
  }
}

