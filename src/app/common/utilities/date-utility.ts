import * as moment from 'moment-timezone';
import {Moment} from 'moment';

export class DateUtility{

  public static getMomentDate(numberOfDays?: any): Moment {
    return moment();
  }

  public static getMomentDateInUTC(numberOfDays?: any): any {
    const utcMoment = moment.utc();
    return new Date(utcMoment.format());
  }

  public static getMomentObject(date: string, format: string = 'YYYY-MM-DD HH:mm:ss'): any{
    return moment(date, format);
  }

  public static setDateFormat(date: any, format: string): string{
    if (date) {
      return moment(date).format(format);
    }
    return '';
  }

  public static localToUTC(date: any, format: string = 'YYYY-MM-DD HH:mm:ss'): any {
    const localeDate = new Date(date);
    return moment.utc(moment(localeDate)).format(format);
  }

  public static localToUTCFormat(date: any, format: string = 'YYYY-MM-DD HH:mm:ss'): string {
    const localeDate = new Date(date);
    return moment.utc(moment(localeDate)).format(format);
  }

  public static utcToLocalFormat(date: any, format: string = 'YYYY-MM-DD HH:mm:ss', timezone: string = 'Asia/Kathmandu'): string {
    return moment.utc(date).tz(timezone).format(format);
  }

  public static getDateBetween(startDate: any, endDate: any, checkDate: any): any {
    return moment(checkDate).isBetween(startDate, endDate);
  }

  public static addDate(date: any, numberOfDays: any, unit: string): any {
    return moment(date).add(numberOfDays, unit);
  }

  public static getCurrentUnixTime(date?: any): any {
    return moment.unix(moment().unix()).utc();
  }

  public static unixToDateTime(unixTime: any, format: string = 'YYYY-MM-DD HH:mm:ss'): any {
    return moment.unix(unixTime).format(format);
  }
  public static getMonthStartDate(): any {
    return moment().startOf('month');
  }

  public static substractDate( subtractDays: number, date?: any, format: string = 'YYYY-MM-DD'): any {
    return moment(date).subtract(subtractDays, 'days').format(format);
  }

  public static getHoursFromSeconds(seconds: number): number {
    return Math.floor(seconds / 3600);
  }

  public static getMinutesFromSeconds(seconds: number): number {
    const hours = DateUtility.getHoursFromSeconds(seconds);
    return Math.floor((seconds - (hours * 3600)) / 60);
  }

  public static getTimeFromHours(decimalTimeString: string): any {
    const n = new Date(0, 0);
    n.setMinutes(+decimalTimeString * 60);
    return n.toTimeString().slice(0, 5);
  }

  public static getUtcDateTime(dateTime: any, format: string = 'YYYY-MM-DD HH:mm:ss'): any {
    return moment.utc(dateTime, format);
  }

  public static timeToSeconds(dateTime: any): number {
    return moment.duration(dateTime).asSeconds();
  }
}

