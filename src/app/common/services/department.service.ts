import { Injectable } from '@angular/core';
import {AbstractBaseService} from '@common/services/abstract-base.service';
import {BaseResource} from '@system/datamodels';
import {NicoHttpClient} from '@system/http-client';
import {DepartmentModel} from '@common/models/department.model';
import { HttpParams } from '@angular/common/http';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService extends AbstractBaseService{


  private departmentDetailChangeSubject = new Subject<string>();

  protected baseModel: BaseResource;

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new DepartmentModel();
  }

  public watchUserChange(): Observable<any> {
    return this.departmentDetailChangeSubject.asObservable();
  }

  public setUserDetailChange(model?: any): void {
    this.departmentDetailChangeSubject.next(model);
  }

  public getResourceName(): string {
    return 'departments';
  }

  public changeStatus(occasion: any, params?: HttpParams): Observable<any>{
      return this.httpClient.put(this.resourceUrl + `/${occasion.id}/` + `status`, {params, });
  }

  public getDetail(occasion: any, params?: HttpParams): Observable<any> {
    return this.httpClient.get(this.resourceUrl + `/${occasion.id}`, {params});
  }

  public changeHOD(occasion: any, hodID, params?: HttpParams): Observable<any> {
    return this.httpClient.put(this.resourceUrl + `/${occasion}/` + `hod`, hodID, {params});
  }

  public saveDepartment(data: DepartmentModel, model?: any, httpParams?: HttpParams): Observable<DepartmentModel>{
    if (model){
      return this.put(model, data, httpParams, `${this.resourceUrl}/${model.id}`);
    }
    else {
      return this.post(data, httpParams, `${this.resourceUrl}`);
    }
  }

  public deleteDepartment(id: number, httpParams?: HttpParams): Observable<DepartmentModel> {
    return this.delete(id, httpParams, `${this.resourceUrl}/${id}`);
  }
}
