import { Injectable } from '@angular/core';
import {AbstractBaseService} from '@common/services/abstract-base.service';
import {NicoHttpClient} from '@system/http-client';
import {UserModel} from '@common/models';
import {BaseResource} from '@system/datamodels';

@Injectable({
  providedIn: 'root'
})
export class LoginService extends AbstractBaseService{

  protected baseModel: BaseResource;

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new UserModel();
  }

  getResourceName(): string {
    return 'auth/login';
  }
}
