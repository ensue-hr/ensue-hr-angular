import {AbstractBaseService} from '@common/services/abstract-base.service';
import {UserModel} from '@common/models';
import {NicoHttpClient} from '@system/http-client';
import {BaseResource} from '@system/datamodels';

export class ChangePasswordService extends AbstractBaseService {
  protected baseModel: BaseResource;

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new UserModel();
  }
  getResourceName(): string {
    return 'auth/change-password';
  }
}
