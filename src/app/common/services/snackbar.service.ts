import {SnackBarComponent} from '@common/components/snack-bar/snack-bar.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  /**
   * Constructor
   */
  constructor(protected snackbar: MatSnackBar) {
  }

  /**
   * Set snackbar contents, time and type
   */
  public snackBarMessage(title: string, message: string, type: 'success' | 'danger' | 'warning' | 'info', timeout?: number): void {
    timeout = timeout ? timeout : 7000;
    this.snackbar.openFromComponent(SnackBarComponent, {
      verticalPosition: 'top',
      horizontalPosition: 'end',
      duration: timeout,
      data: {type, message, title}
    });
  }

  /**
   * Snackbars
   */
  public showSuccessSnackBar(message: string, timeout?: number): void {
    this.snackBarMessage('Success', message, 'success', timeout);
  }

  public showErrorSnackBar(message: string, timeout?: number): void {
    this.snackBarMessage('Error', message, 'danger', timeout);
  }

  public showWarningSnackBar(message: string, timeout?: number): void {
    this.snackBarMessage('Warning', message, 'warning', timeout);
  }

  public showInfoSnackBar(message: string, timeout?: number): void {
    this.snackBarMessage('Info', message, 'info', timeout);
  }
}
