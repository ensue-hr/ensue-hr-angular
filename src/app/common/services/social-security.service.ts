import { Injectable } from '@angular/core';
import {AbstractBaseService} from '@common/services/abstract-base.service';
import {BaseResource} from '@system/datamodels';
import {NicoHttpClient} from '@system/http-client';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import {SocialSecurityModel} from '@common/models/social-security.model';
import {PaginatedNicoCollection} from '@system/utilities';
import {DateUtility} from "@common/utilities/date-utility";

@Injectable({
  providedIn: 'root'
})
export class SocialSecurityService extends AbstractBaseService{

  protected baseModel: BaseResource;
  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new SocialSecurityModel();
  }


  public getResourceName(): string {
    return 'employees';
  }

  public getSocialSecurity(id: string, params?: HttpParams): Observable<PaginatedNicoCollection<SocialSecurityModel>> {
    return this.get(params, `${this.resourceUrl}/${id}/social-securities`);
  }

  public postSocialSecurity(id: string, body: SocialSecurityModel, params?: HttpParams): Observable<SocialSecurityModel>{
    return this.post(body, params, `${this.resourceUrl}/${id}/social-securities`);
  }

  public deleteSocialSecurity(id: string, model: SocialSecurityModel, params?: HttpParams): Observable<any>{
    return this.delete(model, params, `${this.resourceUrl}/${id}/social-securities/${model.id}`);
  }

  public saveSocialSecurity(
              id: string,
              data: SocialSecurityModel,
              model?: SocialSecurityModel,
              params?: HttpParams): Observable<SocialSecurityModel>{

    let postData = {};
    if (data.end_date){
      postData = {...data,
        start_date: DateUtility.setDateFormat(data.start_date._d, 'YYYY-MM-DD'),
        end_date: DateUtility.setDateFormat(data.end_date._d, 'YYYY-MM-DD')};
    }
    else{
      postData = {...data, start_date: DateUtility.setDateFormat(data.start_date._d, 'YYYY-MM-DD')};

    }
    if (model) {
      return this.put(model, postData, params, `${this.resourceUrl}/${id}/social-securities/${model.id}`);
    }
    else{
      return this.post(postData, params, `${this.resourceUrl}/${id}/social-securities`);
    }

  }
  public putSocialSecurity(
        id: string,
        model: SocialSecurityModel,
        data: SocialSecurityModel,
        params?: HttpParams
  ): Observable<SocialSecurityModel>{
    return this.put(model, data, params, `${this.resourceUrl}/${id}/social-securities/${model.id}`);
  }

  public changeSocialSecurityStatus(id: string, model: SocialSecurityModel): Observable<any>{
    return this.httpClient.put(`${this.resourceUrl}/${id}/social-securities/${model.id}/status`);
  }
}
