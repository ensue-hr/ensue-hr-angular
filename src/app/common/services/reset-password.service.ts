import {AbstractBaseService} from '@common/services/abstract-base.service';
import {NicoHttpClient} from '@system/http-client';
import {Injectable} from '@angular/core';
import {UserModel} from '@common/models';
import {BaseResource} from '@system/datamodels';

@Injectable({
  providedIn: 'root'
})
export class ResetPasswordService extends AbstractBaseService {

  protected baseModel: BaseResource;

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new UserModel();
  }

  getResourceName(): string {
    return 'password/reset';
  }

}
