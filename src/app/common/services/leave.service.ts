import {AbstractBaseService} from '@common/services/abstract-base.service';
import {Injectable} from '@angular/core';
import {BaseResource} from '@system/datamodels';
import {NicoHttpClient} from '@system/http-client';
import {HttpParams} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {PaginatedNicoCollection} from '@system/utilities';
import {RequestLeaveModel} from '@common/models/request-leave.model';
import {LeaveModel} from '@common/models/leave.model';
import {DateUtility} from '@common/utilities/date-utility';
import {AppRouteConstants} from '@common/constants/appRouteConstants';

@Injectable({
  providedIn: 'root'
})
export class LeaveService extends AbstractBaseService {

  private userDetailChangeSubject = new Subject<string>();

  protected baseModel: BaseResource;

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new LeaveModel();
  }


  getResourceName(): string {
    return 'leaves';
  }


  public saveRequestLeave(body: RequestLeaveModel, model?: any): Observable<LeaveModel>{
    const data = {...body,
      is_emergency: JSON.parse(body.is_emergency),
      start_at: DateUtility.localToUTCFormat(body.start_at, 'YYYY-MM-DD HH:mm:ss'),
      end_at: DateUtility.localToUTCFormat(body.end_at, 'YYYY-MM-DD HH:mm:ss')
    };
    if (model){
        return this.put(model, data, null, `${this.resourceUrl}/${model.id}`);
      }
    else {
        return this.post(data, null, this.resourceUrl);
      }
    }

  public saveEmployeeRequestLeave(body: RequestLeaveModel, model?: any): Observable<LeaveModel> {
    const data = {...body,
      is_emergency: body.is_emergency !== 'false',
      start_at: DateUtility.localToUTCFormat(body.start_at, 'YYYY-MM-DD HH:mm:ss'),
      end_at: DateUtility.localToUTCFormat(body.end_at, 'YYYY-MM-DD HH:mm:ss')
    };

    return this.post(data, null, `${this.resourceUrl}/emergency`);
  }

  public getLeaves(params?: HttpParams): Observable<PaginatedNicoCollection<LeaveModel>> {
    return this.get(params, this.resourceUrl);
  }

  public getLeavesDetail(id: string, params?: HttpParams): Observable<LeaveModel>{
    return this.get(params, `${this.resourceUrl}/${id}`);
  }

  public onApproveLeave(model: LeaveModel, params?: HttpParams): Observable<LeaveModel>{
    return this.put(model, model.id, params, `${this.resourceUrl}/${model.id}/approve`);
  }

  public onCancelDeclineLeave(model: LeaveModel, data: any, state: boolean, params?: HttpParams): Observable<LeaveModel>{
    if (state) {
      return this.put(model, data, params, `${this.resourceUrl}/${model.id}/cancel`);
    }
    else{
      return this.put(model, data, params, `${this.resourceUrl}/${model.id}/decline`);
    }
  }

  public onCancellationLeave(id: string, body: LeaveModel, params?: HttpParams): Observable<LeaveModel> {
    const data = {...body,
      start_at: DateUtility.localToUTCFormat(body.start_at, 'YYYY-MM-DD HH:mm:ss'),
      end_at: DateUtility.localToUTCFormat(body.end_at, 'YYYY-MM-DD HH:mm:ss')
    };
    return this.post(data, params, `${this.resourceUrl}/${id}/cancellation`);
  }

}
