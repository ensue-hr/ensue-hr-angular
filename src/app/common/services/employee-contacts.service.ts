import {AbstractBaseService} from '@common/services/abstract-base.service';
import {Injectable} from '@angular/core';
import {BaseResource} from '@system/datamodels';
import {NicoHttpClient} from '@system/http-client';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EmployeeBanksModel, EmployeeContactsModel} from '@common/models';
import {PaginatedNicoCollection} from '@system/utilities';

@Injectable({
  providedIn: 'root'
})
export class EmployeeContactsService extends AbstractBaseService {

  protected baseModel: BaseResource;

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new EmployeeContactsModel();
  }

  getResourceName(): string {
    return 'employees';
  }

  public getContacts(id: any, params?: HttpParams): Observable<PaginatedNicoCollection<EmployeeContactsModel>> {
    return this.get(params, this.resourceUrl + `/${id}` + '/contacts');
  }

  public saveContact(body: EmployeeContactsModel, id: any, contactModel?: any): Observable<PaginatedNicoCollection<EmployeeBanksModel>> {
    if (contactModel) {
      return this.put(contactModel, body, null, this.resourceUrl + `/${id}` + '/contacts' + `/${contactModel.id}`);
    } else {
      return this.post(body, null, this.resourceUrl + `/${id}` + '/contacts');
    }
  }

  public deleteContact(id: any, contactModel: any): Observable<PaginatedNicoCollection<EmployeeContactsModel>> {
    return this.delete(contactModel, null, this.resourceUrl + `/${id}` + '/contacts' + `/${contactModel.id}`);
  }

  public toggleContactStatus(id: any, contactModel: any): Observable<EmployeeContactsModel> {
    return this.put(contactModel, null, null, this.resourceUrl + `/${id}` + '/contacts' + `/${contactModel.id}` + '/status');
  }
}
