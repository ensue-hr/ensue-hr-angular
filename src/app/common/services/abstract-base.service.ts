import {Injectable} from '@angular/core';
import {NicoHttpClient} from '@system/http-client';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {NicoCollection, NicoUtils, PaginatedNicoCollection} from '@system/utilities';
import {BaseResource, BaseResourceInterface} from '@system/datamodels';
import {RestfulApiInterface} from './restful-api.interface';
import {HttpParams} from '@angular/common/http';
import {isArray} from 'rxjs/internal-compatibility';
import {environment} from '../../../environments/environment';

@Injectable()
export abstract class AbstractBaseService implements RestfulApiInterface {

  protected abstract baseModel: BaseResource;

  protected resourceUrl = '';

  protected env: any;

  public abstract getResourceName(): string;

  protected constructor(protected httpClient: NicoHttpClient) {
    this.env = environment;
    this.generateResourceUrl();
  }

  protected generateResourceUrl(): void {
    this.resourceUrl = this.env.api + this.getResourceName();
  }

  public getResourceUrl(): string {
    return this.resourceUrl;
  }

  public get(params?: HttpParams, url?: string, isDynamic?: boolean): Observable<any> {

    if (NicoUtils.isNullOrUndefined(url)) {
      url = this.resourceUrl;
    }
    return this.httpClient.get(url, {params, }).pipe(map((response: any) => {

      if (isDynamic) {
        return response;
      } else if (response instanceof NicoCollection || response instanceof Array || isArray(response) || response.data) {
        if (response.data) {
          const coll = this.baseModel.createFromCollection(response.data as any) as NicoCollection<BaseResourceInterface>;
          const ret = new PaginatedNicoCollection();
          ret.setItems(coll.all());
          ret.currentPage = response.current_page;
          ret.total = response.total;
          ret.nextPageUrl = response.next_page_url;
          ret.perPage = response.per_page;
          ret.prevPageUrl = response.previous_page_url;
          return ret;
        }
        return this.baseModel.createFromCollection(response as any) as NicoCollection<BaseResourceInterface>;
      } else {
        return this.baseModel.create(response) as BaseResourceInterface;
      }
    }));
  }

  public post(body, params?: HttpParams, url?: string): Observable<any> {
    if (NicoUtils.isNullOrUndefined(url)) {
      url = this.resourceUrl;
    }
    return this.httpClient.post(url, body, {params,});
  }

  public delete(model: any, params?: HttpParams, url?: string): Observable<any> {
    if (NicoUtils.isNullOrUndefined(url)) {
      url = this.resourceUrl + `/${model.id}`;
    }
    return this.httpClient.delete(url, {params});
  }

  public put(model: any, data: any, params?: HttpParams, url?: string): Observable<any> {
    if (NicoUtils.isNullOrUndefined(url)) {
      url = this.resourceUrl + `/${model.id}`;
    }
    return this.httpClient.put(url, data, {params});
  }
}
