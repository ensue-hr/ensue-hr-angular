import { Injectable } from '@angular/core';
import {AbstractBaseService} from '@common/services/abstract-base.service';
import {NicoHttpClient} from '@system/http-client';
import {BaseResource} from '@system/datamodels';
import {AllocatedLeaveModel} from '@common/models/allocated-leaves.model';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {PaginatedNicoCollection} from '@system/utilities';

@Injectable({
  providedIn: 'root'
})
export class AllocatedLeaveService extends AbstractBaseService{

  protected baseModel: BaseResource;

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new AllocatedLeaveModel();
  }

  getResourceName(): string {
    return 'employees';
  }

  public getAllocatedLeaves(id: string, httpParams?: HttpParams): Observable<PaginatedNicoCollection<AllocatedLeaveModel>> {
    return this.get(httpParams, `${this.resourceUrl}/${id}/allocated-leaves`);
  }

  public saveAllocatedLeave(
    id: string,
    body: AllocatedLeaveModel,
    model?: AllocatedLeaveModel,
    httpParams?: HttpParams): Observable<AllocatedLeaveModel>{
    if (model){
      return this.put(model, body, httpParams, `${this.resourceUrl}/${id}/allocated-leaves/${model.id}`);
    }
    else{
      return this.post(body, httpParams, `${this.resourceUrl}/${id}/allocated-leaves`);
    }
  }

  public deleteAllocatedLeave(id: string, model: AllocatedLeaveModel, httpParams?: HttpParams): Observable<boolean>{
    return this.delete(model, httpParams, `${this.resourceUrl}/${id}/allocated-leaves/${model.id}`);
  }

  public toggleStatusAllocatedLeave(id: string, model: AllocatedLeaveModel, httpParams?: HttpParams): Observable<AllocatedLeaveModel>{
    return this.put(model, null, null, `${this.resourceUrl}/${id}/allocated-leaves/${model.id}/status`);
  }
}
