import {AbstractBaseService} from '@common/services/abstract-base.service';
import {Injectable} from '@angular/core';
import {UserModel} from '@common/models';
import {BaseResource} from '@system/datamodels';
import {NicoHttpClient} from '@system/http-client';

@Injectable({
  providedIn: 'root'
})
export class UserService extends AbstractBaseService {

  protected baseModel: BaseResource;

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new UserModel();
  }

  getResourceName(): string {
    return 'auth/me';
  }

}
