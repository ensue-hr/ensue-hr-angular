import {AbstractBaseService} from '@common/services/abstract-base.service';
import {Injectable} from '@angular/core';
import {BaseResource} from '@system/datamodels';
import {NicoHttpClient} from '@system/http-client';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EmployeeBanksModel, EmployeeContactsModel} from '@common/models';
import {PaginatedNicoCollection} from '@system/utilities';

@Injectable({
  providedIn: 'root'
})
export class EmployeeBanksService extends AbstractBaseService {

  protected baseModel: BaseResource;

  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new EmployeeBanksModel();
  }

  getResourceName(): string {
    return 'employees';
  }

  public getBanks(id: any, params?: HttpParams): Observable<PaginatedNicoCollection<EmployeeBanksModel>> {
    return this.get(params, this.resourceUrl + `/${id}` + '/banks');
  }

  public saveBank(body: EmployeeBanksModel, id: any, bankModel?: any): Observable<PaginatedNicoCollection<EmployeeBanksModel>> {
    if (bankModel) {
      return this.put(bankModel, body, null, this.resourceUrl + `/${id}` + '/banks' + `/${bankModel.id}`);
    } else {
      return this.post(body, null, this.resourceUrl + `/${id}` + '/banks');
    }
  }

  public getBank(id: any, bankId: any, params?: HttpParams): Observable<PaginatedNicoCollection<EmployeeBanksModel>> {
    return this.get(params, this.resourceUrl + `/${id}` + '/banks' + `/${bankId}`);
  }

  public deleteBank(id: any, bankModel: any): Observable<PaginatedNicoCollection<EmployeeBanksModel>> {
    return this.delete(bankModel, null, this.resourceUrl + `/${id}` + '/banks' + `/${bankModel.id}`);
  }

  public toggleBankStatus(id: any, bankModel: any): Observable<EmployeeContactsModel> {
    return this.put(bankModel, null, null, this.resourceUrl + `/${id}` + '/banks' + `/${bankModel.id}` + '/status');
  }

}
