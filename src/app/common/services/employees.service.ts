import {AbstractBaseService} from '@common/services/abstract-base.service';
import {Injectable} from '@angular/core';
import {BaseResource} from '@system/datamodels';
import {EmployeeModel} from '@common/models/employee.model';
import {NicoHttpClient} from '@system/http-client';
import {HttpParams} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {DateUtility} from '@common/utilities/date-utility';
import {NicoCollection} from '@system/utilities';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService extends AbstractBaseService {

  private userDetailChangeSubject = new Subject<any>();

  protected baseModel: BaseResource;
  constructor(protected httpClient: NicoHttpClient) {
    super(httpClient);
    this.baseModel = new EmployeeModel();
  }

  getResourceName(): string {
    return 'employees';
  }

  public watchUserChange(): Observable<any> {
    return this.userDetailChangeSubject.asObservable();
  }

  public setUserDetailChange(model: EmployeeModel): void {
    this.userDetailChangeSubject.next(model);
  }

  public getDetail(occasion: any, params?: HttpParams): Observable<any> {
    return this.httpClient.get(this.resourceUrl + `/${occasion.id}`, {params});
  }

  public saveEmployee(data: EmployeeModel, model?: any, httpParams?: HttpParams): Observable<any>{
    const body = {...data, dob: DateUtility.setDateFormat(data.dob._d, 'YYYY-MM-DD'),
      joined_at: DateUtility.setDateFormat(data.joined_at._d, 'YYYY-MM-DD')};
    if (model.id){
      return this.put(model, body, httpParams, `${this.resourceUrl}/${model.id}`);
    }
    else {
      return this.post(body, httpParams, this.resourceUrl);
    }
  }

  public getManagers(): Observable<NicoCollection<EmployeeModel>> {
    return this.httpClient.get(this.resourceUrl + '/managers');
  }

}
