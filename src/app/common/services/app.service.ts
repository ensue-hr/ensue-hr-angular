import {EventEmitter, Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {NicoStorageService} from '@system/services';

@Injectable({providedIn: 'root'})
export class AppService {

  protected STORE_KEY_LIGHT_MODE = 'is.app.theme.lightMode';

  public lightModeSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  constructor(protected storage: NicoStorageService) {
  }

  /**
   * Enable light mode
   * lightMode
   */
  public enableLightMode(lightMode: boolean): void {

    this.storage.setItem(this.STORE_KEY_LIGHT_MODE, lightMode ? '1' : '0');
    this.lightModeSubject.next(lightMode);

    if (lightMode) {
      document.documentElement.setAttribute('data-theme', 'theme-light');
    } else {
      document.documentElement.setAttribute('data-theme', 'theme-dark');
    }

  }

  public isLightMode(): boolean {
    return this.storage.getItem(this.STORE_KEY_LIGHT_MODE) !== '0';
  }

  public isDarkMode(): boolean {
    return !this.isLightMode();
  }

  public setThemeModeFromStorage(): void {
    const val = this.storage.getItem(this.STORE_KEY_LIGHT_MODE);
    this.enableLightMode(val !== '0');
  }


}
