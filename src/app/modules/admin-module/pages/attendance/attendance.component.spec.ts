import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendanceComponent } from './attendance.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {AttendanceService} from '@common/services';
import {MockAttendanceService} from '@common/testing-resources/mock-services/MockAttendanceService';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MockMatDialog} from '@common/testing-resources/mock-services';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {EditPunchHistoryComponent} from './components/staff-dashboard/components/edit-punch-history/edit-punch-history.component';

describe('AttendanceComponent', () => {
  let component: AttendanceComponent;
  let fixture: ComponentFixture<AttendanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EditPunchHistoryComponent],
      providers: [
        ...MOCK_PROVIDERS,
        {provide: AttendanceService, useClass: MockAttendanceService},
        { provide: MatDialogRef, useValue: MockMatDialog },
        { provide: MAT_DIALOG_DATA, useValue: {} },
      ],
      imports: [
        TranslateModule,
        RouterTestingModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
