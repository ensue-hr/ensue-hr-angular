// import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';
//
// import { EditPunchHistoryComponent } from './edit-punch-history.component';
// import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
// import {AttendanceService} from '@common/services';
// import {MockMatDialog} from '@common/testing-resources/mock-services';
// import {MockAttendanceService} from '@common/testing-resources/mock-services/MockAttendanceService';
// import {RouterTestingModule} from '@angular/router/testing';
// import {TranslateModule} from '@ngx-translate/core';
// import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
// import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
// import {EmployeeContactsModel} from '@common/models';
// import {CommonModule} from '@angular/common';
//
// fdescribe('EditPunchHistoryComponent', () => {
//   let component: EditPunchHistoryComponent;
//   let fixture: ComponentFixture<EditPunchHistoryComponent>;
//
//   beforeEach(async () => {
//     await TestBed.configureTestingModule({
//       declarations: [EditPunchHistoryComponent],
//       providers: [
//         ...MOCK_PROVIDERS,
//         {provide: AttendanceService, useClass: MockAttendanceService},
//         { provide: MatDialogRef, useClass: MockMatDialog },
//         { provide: MAT_DIALOG_DATA, useValue: {} },
//       ],
//       imports: [
//         TranslateModule,
//         RouterTestingModule,
//         CommonModule,
//       ],
//       schemas: [
//         CUSTOM_ELEMENTS_SCHEMA,
//       ]
//     })
//       .compileComponents();
//   });
//
//   beforeEach(() => {
//     fixture = TestBed.createComponent(EditPunchHistoryComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
//
//   it('should set page title', fakeAsync (async () => {
//     component.onComponentReady();
//     expect(component.pageTitle).toEqual('MOD_DASHBOARD.MOD_VIEW_PUNCH_HISTORY.MOD_EDIT_PUNCH_HISTORY.EDIT_LOG_TITLE');
//     flush();
//   }));
//
//   it('should initialize form', () => {
//     spyOn(component, 'createForm');
//     component.ngOnInit();
//     expect(component.createForm).toHaveBeenCalled();
//   });
//
//   it('should check initial form values in case of add', () => {
//     const contactForm = component.editPunchHistoryForm;
//     const contactFormValues = {
//       reason: '',
//       attend_at: '',
//     };
//     expect(contactForm.value).toEqual(contactFormValues);
//   });
//
//   it('should set form values in case of edit', () => {
//     component.modal = {
//       content: {
//         reason: '',
//         attend_at: '2020-12-12 12:12:12',
//       },
//       empId: 1
//     } as unknown as EmployeeContactsModel;
//     component.onComponentReady();
//     const contactForm = component.editPunchHistoryForm;
//     const contactFormValues = {
//       reason: '',
//       attend_at: new Date('Sat Dec 12 2020 17:57:12 GMT+0545 (Nepal Time)'),
//     };
//     expect(contactForm.value).toEqual(contactFormValues);
//   });
//
//   it('should call showSuccessSnackBar in case of success', fakeAsync(async () => {
//     spyOn(component, 'showSuccessSnackBar');
//     component.onComponentReady();
//     component.editPunchHistoryForm.setValue({
//       attend_at: '2020-12-12 12:12:12',
//       reason: 'test'
//     });
//     fixture.detectChanges();
//     await component.onFormSubmit();
//     await fixture.whenStable();
//     expect(component.showSuccessSnackBar).toHaveBeenCalled();
//     flush();
//   }));
//
//   it('should disable submit button until conditions are met', () => {
//     component.createForm();
//     const button = fixture.debugElement.nativeElement.querySelectorAll('button')[1].getAttribute('disabled');
//     expect(button).toMatch('');
//   });
//
//   it('should activate button after conditions are met', () => {
//     component.onComponentReady();
//     component.editPunchHistoryForm.patchValue({reason: 'test', attend_at: '2020-12-12 12:12:12'});
//     fixture.detectChanges();
//     const button = fixture.debugElement.nativeElement.querySelectorAll('button')[1].getAttribute('disabled');
//     expect(button).toBeDefined();
//   });
//
//   it('should call showErrorSnackBar in case of success', fakeAsync(async () => {
//     spyOn(component, 'showErrorSnackBar');
//     component.onComponentReady();
//     component.editPunchHistoryForm.setValue({
//       attend_at: '2020-12-12 12:12:12',
//       reason: 'test2'
//     });
//     fixture.detectChanges();
//     await component.onFormSubmit();
//     await fixture.whenStable();
//     expect(component.showErrorSnackBar).toHaveBeenCalled();
//     flush();
//   }));
// });
