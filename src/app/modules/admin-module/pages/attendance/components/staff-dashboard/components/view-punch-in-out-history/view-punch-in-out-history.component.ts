import {Component, Injector, Input, OnDestroy, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {AttendanceService, UserDetailService} from '@common/services';
import {MatDialog} from '@angular/material/dialog';
import {DateUtility} from '@common/utilities/date-utility';
import {Subscription} from 'rxjs';
import {AttendanceDetailModel, UserModel} from '@common/models';
import {PaginatedNicoCollection} from '@system/utilities';
import {AttendanceType} from '@common/enums/attendanceType.enum';
import {EditPunchHistoryComponent} from '../edit-punch-history/edit-punch-history.component';
import {AccordionAnimation} from '@common/animations/app.animations';

@Component({
  selector: 'app-view-punch-in-out-history',
  templateUrl: './view-punch-in-out-history.component.html',
  styleUrls: ['./view-punch-in-out-history.component.scss'],
  animations: [AccordionAnimation]
})
export class ViewPunchInOutHistoryComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Enum
   */
  public attendanceEnum = AttendanceType;

  /**
   * Employee id
   */
  @Input() employeeModel: UserModel;

  /**
   * Attendance model
   */

  @Input() attendanceModel: PaginatedNicoCollection<AttendanceDetailModel>;

  /**
   * Css classes
   */
  public activityClass: string;

  public checkOutRedClass: string;

  /**
   * Today
   */
  public today: string;

  /**
   * Subscriptions
   */
  public punchInOutSubscription: Subscription;

  public employeeSubscription: Subscription;

  public attendanceChangeSubscription: Subscription;

  /**
   * Constructor
   */
  constructor(protected injector: Injector,
              public attendanceService: AttendanceService,
              public userDetailService: UserDetailService,
              public dialog: MatDialog) {
    super(injector);
    this.today = DateUtility.setDateFormat(DateUtility.getMomentDate(), 'YYYY-MM-DD');
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_DASHBOARD.HTML_TITLE');
    super.onComponentReady();
  }

  /**
   * Get local check in time
   */
  public getLocalTime(date: string): string {
    return DateUtility.utcToLocalFormat(date, 'HH:mm');
  }

  /**
   * Find class for check in/out icons
   */
  public rotateTypeIcon(model: AttendanceDetailModel): string {
    let activityClass = '';
    if (model.typeIcon === 'far fa-long-arrow-right') {
      activityClass = model.type === this.attendanceEnum.CheckIn ? 'rotate-45 check-in' : 'rotate--45 check-out';
      this.checkOutRedClass = model.type === this.attendanceEnum.CheckOut ? 'all-red' : '';
    } else {
      activityClass = '';
    }
    return activityClass;
  }

  /**
   * Rotate check in out arrow icons
   */
  public rotateCheckInOutArrows(model: AttendanceDetailModel): string {
    if (model.isOpen) {
      return 'rotate-90';
    }
    return 'rotate-0';
  }

  /**
   * Show accordion in case of check in and out
   */
  public showAccordionForCheckInOut(attendance: AttendanceDetailModel): boolean {
    if ((attendance?.type === this.attendanceEnum.CheckIn || attendance?.type === this.attendanceEnum.CheckOut) || attendance?.parents?.length > 0) {
      return true;
    }
    return false;
  }

  /**
   * Show info block in case of check in and out
   */
  public showInfoBlockForCheckInOut(attendance: AttendanceDetailModel): boolean {
    if (attendance?.type === this.attendanceEnum.CheckIn || attendance?.type === this.attendanceEnum.CheckOut) {
      return true;
    }
    return false;
  }

  /**
   * Add/edit new contact (Open drawer)
   */
  public onEditAttendance(model: AttendanceDetailModel): void {
    const modalDialog = this.dialog.open(EditPunchHistoryComponent, {
      data: {content: model, attendanceId: this.employeeModel?.id},
      panelClass: ['modal-drawer-view', 'modal-xs'],
      disableClose: true
    });
    modalDialog.afterClosed().subscribe(reload => {
      if (reload) {
        this.attendanceService.setattendanceChange();
      }
    });
  }

  /**
   * Hide edit button in case of old history
   */
  public hideEditButton(attendance: AttendanceDetailModel): boolean {
    if (!(attendance.type === this.attendanceEnum.LeaveOut || attendance.type === this.attendanceEnum.LeaveIn)) {
      if ((DateUtility.utcToLocalFormat(attendance.attend_at, 'YYYY-MM-DD') === DateUtility.setDateFormat(new Date(), 'YYYY-MM-DD')) ||
      DateUtility.utcToLocalFormat(attendance.attend_at, 'YYYY-MM-DD') === DateUtility.setDateFormat(DateUtility.getMomentDate().subtract(JSON.parse(this.userSettingsItems).attendance_employee_update_before_days, 'day'), 'YYYY-MM-DD')) {
        return true;
      }
    }
    return false;
  }

  /**
   * Info message for check in or check out
   */
  public assignMessageForCheckInOut(attendance: AttendanceDetailModel): string {
    let message = '';
    const ip = attendance.ip ? attendance.ip : '';
    const location = attendance.location ? this.translateService.instant('MOD_ATTENDANCE.FROM_LABEL') + attendance.location : '';
    const time = DateUtility.utcToLocalFormat(attendance.attend_at, 'HH:mm');
    message = attendance.type === this.attendanceEnum.CheckIn ?
      this.translateService.instant('MOD_ATTENDANCE.CHECK_IN_INFO_LABEL', {
        browser: attendance.browser,
        ip,
        time,
        location
      }) :
      this.translateService.instant('MOD_ATTENDANCE.CHECK_OUT_INFO_LABEL', {
        browser: attendance.browser,
        ip,
        time,
        location
      });
    return message;
  }

  /**
   * Toggle accordion
   */
  public toggleAccordion($event, data): void {
    $event.stopPropagation();
    data.isOpen = !data.isOpen;
  }

  /**
   * Generate message for edit history
   */
  public showPreviousChange(attendance: AttendanceDetailModel): string {
    if (attendance.parents.length > 1) {
      return this.translateService.instant('MOD_ATTENDANCE.TIME_CHANGE_LABEL', {
        from: DateUtility.utcToLocalFormat(attendance?.parents[attendance?.parents.length - 1]?.attend_at, 'HH:mm'),
        to: DateUtility.utcToLocalFormat(attendance?.parents[attendance?.parents.length - 2]?.attend_at, 'HH:mm'),
        reason: attendance.reason
      }) + this.translateService.instant('MOD_ATTENDANCE.SECOND_TIME_CHANGE_LABEL', {
        from: DateUtility.utcToLocalFormat(attendance?.parents[attendance?.parents.length - 2]?.attend_at, 'HH:mm'),
        to: DateUtility.utcToLocalFormat(attendance?.attend_at, 'HH:mm'),
        reason: attendance.reason
      });
    }
    return this.translateService.instant('MOD_ATTENDANCE.TIME_CHANGE_LABEL',
      {
        from: DateUtility.utcToLocalFormat(attendance?.parents[attendance?.parents.length - 1]?.attend_at, 'HH:mm'),
        to: DateUtility.utcToLocalFormat(attendance?.attend_at, 'HH:mm'),
        reason: attendance.reason
      });
  }

  /**
   * On destroy
   */
  public ngOnDestroy(): void {
    if (this.attendanceChangeSubscription != null) {
      this.attendanceChangeSubscription.unsubscribe();
    }

    if (this.punchInOutSubscription != null) {
      this.punchInOutSubscription.unsubscribe();
    }

    if (this.employeeSubscription != null) {
      this.employeeSubscription.unsubscribe();
    }
  }

}
