import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkTimeChartComponent } from './work-time-chart.component';
import {ViewPunchInOutHistoryComponent} from '../../../view-punch-in-out-history/view-punch-in-out-history.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('WorkTimeChartComponent', () => {
  let component: WorkTimeChartComponent;
  let fixture: ComponentFixture<WorkTimeChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WorkTimeChartComponent],
      providers: [
        ...MOCK_PROVIDERS,
      ],
      imports: [
        TranslateModule
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkTimeChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create work time chart component', () => {
    expect(component).toBeTruthy();
  });

  it('should set chart options', () => {
    spyOn(component, 'setChartOptions');
    component.onComponentReady();
    expect(component.setChartOptions).toHaveBeenCalled();
  });
});
