import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';
import { StaffDashboardComponent } from './staff-dashboard.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('StaffDashboardComponent', () => {
  let component: StaffDashboardComponent;
  let fixture: ComponentFixture<StaffDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StaffDashboardComponent],
      providers: [
        ...MOCK_PROVIDERS,
      ],
      imports: [
        TranslateModule
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get Employee Details on init', fakeAsync(async () => {
    spyOn(component, 'getEmployeeDetails');
    await component.onComponentReady();
    await fixture.detectChanges();
    fixture.whenStable();
    flush();
    expect(component.getEmployeeDetails).toHaveBeenCalled();
  }));

  it('should get attendance data on init', fakeAsync(async () => {
    spyOn(component, 'getModels');
    await fixture.detectChanges();
    await component.getEmployeeDetails();
    await fixture.whenStable();
    expect(component.getModels).toHaveBeenCalled();
  }));
});
