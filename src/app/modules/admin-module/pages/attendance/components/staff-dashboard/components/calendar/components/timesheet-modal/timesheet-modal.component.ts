import {Component, Inject, Injector, Input, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AttendanceDetailModel, EmployeeContactsModel} from '@common/models';
import {AttendanceDetailService, AttendanceService} from '@common/services';
import {FormBuilder} from '@angular/forms';
import {AbstractBaseComponent} from '../../../../../../../../../../AbstractBaseComponent';
import {HttpParams} from '@angular/common/http';
import {DateUtility} from '@common/utilities/date-utility';
import {Subscription} from 'rxjs';
import {PaginatedNicoCollection} from '@system/utilities';
import {TimesheetModalInterface} from '@common/interface/timesheet-modal.interface';

@Component({
  selector: 'app-timesheet-modal',
  templateUrl: './timesheet-modal.component.html',
  styleUrls: ['./timesheet-modal.component.scss']
})
export class TimesheetModalComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  public punchInOutSubscription: Subscription;

  public attendanceModel: PaginatedNicoCollection<AttendanceDetailModel>;

  /**
   * Constructor
   */
  constructor(
    protected injector: Injector,
    @Inject(MAT_DIALOG_DATA) public modal: TimesheetModalInterface,
    public dialogRef: MatDialogRef<TimesheetModalComponent>,
    public attendanceDetailService: AttendanceDetailService,
    public attendanceService: AttendanceService,
    public formBuilder: FormBuilder) {
    super(injector);
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_DASHBOARD.MOD_VIEW_PUNCH_HISTORY.MOD_EDIT_PUNCH_HISTORY.EDIT_LOG_TITLE');
    super.onComponentReady();
    this.getModels();
  }

  /**
   * Get attendance models
   */
  public getModels(): void {
    let params: HttpParams = new HttpParams();
    params = params.set('employee_id', this.modal?.employeeId);
    params = params.set('date', DateUtility.setDateFormat(this.modal?.selectedDate, 'YYYY-MM-DD'));
    this.punchInOutSubscription = this.attendanceDetailService.getEmployeeAttendance(params).subscribe(attendanceModel => {
      this.attendanceModel = attendanceModel;
    });
  }

  /**
   * On destroy
   */
  public ngOnDestroy(): void {
    if (this.punchInOutSubscription != null) {
      this.punchInOutSubscription.unsubscribe();
    }
  }
}
