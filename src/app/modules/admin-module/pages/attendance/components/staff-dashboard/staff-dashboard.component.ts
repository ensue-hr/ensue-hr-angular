import {Component, Injector, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  AttendanceDetailService,
  AttendanceService,
  AttendanceSummaryService,
  UserDetailService
} from '@common/services';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';
import {HttpParams} from '@angular/common/http';
import {DateUtility} from '@common/utilities/date-utility';
import {Subscription} from 'rxjs';
import {AttendanceCalendarModel, AttendanceDetailModel, AttendanceSummaryModel, UserModel} from '@common/models';
import {NicoCollection, PaginatedNicoCollection} from '@system/utilities';
import {AttendanceCalendarService} from '@common/services/attendance-calendar.service';
import {MatDialog} from '@angular/material/dialog';
import {ChartMonthlyRecordComponent} from './components/chart-monthly-record/chart-monthly-record.component';

@Component({
  selector: 'app-staff-dashboard',
  templateUrl: './staff-dashboard.component.html',
  styleUrls: ['./staff-dashboard.component.scss']
})
export class StaffDashboardComponent extends AbstractBaseComponent implements OnInit, OnDestroy {
  @ViewChild('chartMonth') chartMonth: ChartMonthlyRecordComponent;

  /**
   * Models
   */
  public employeeModel: UserModel;

  public attendanceModel: PaginatedNicoCollection<AttendanceDetailModel>;

  public calendarModel: NicoCollection<AttendanceCalendarModel>;

  public summaryModel: PaginatedNicoCollection<AttendanceSummaryModel>;

  /**
   * Subscriptions
   */
  public punchInOutSubscription: Subscription;

  public employeeSubscription: Subscription;

  public attendanceChangeSubscription: Subscription;

  public calendarSubscription: Subscription;

  public summarySubscription: Subscription;

  /**
   * Work and break hours
   */
  public workHours: string;

  public breakHours: string;

  /**
   * From calendar
   */
  public startDate = DateUtility.setDateFormat(DateUtility.getMomentDate().startOf('month'), 'YYYY-MM-DD');

  public endDate = DateUtility.setDateFormat(new Date(), 'YYYY-MM-DD');

  public today = new Date();

  /**
   * Constructor
   */
  constructor(protected injector: Injector,
              public attendanceService: AttendanceService,
              public attendanceDetailService: AttendanceDetailService,
              public userDetailService: UserDetailService,
              public attendanceCalendarService: AttendanceCalendarService,
              public attendanceSummaryService: AttendanceSummaryService,
              public dialog: MatDialog) {
    super(injector);
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.getEmployeeDetails();
    this.attendanceChangeSubscription = this.attendanceService.watchObservableChange().subscribe(response => {
      this.getModels();
    });
  }

  /**
   * Get summary details
   */
  public getSummaryModels(): void {
    let params: HttpParams = new HttpParams();
    params = params.set('employee_id', this.employeeModel?.id);
    params = params.set('start_date', this.startDate);
    params = params.set('end_date', this.endDate);
    this.summarySubscription = this.attendanceSummaryService.getAttendanceSummary(params).subscribe(summaryModel => {
      this.summaryModel = summaryModel;
    });
  }

  /**
   * Get Employee Details
   */
  public getEmployeeDetails(): void {
    this.employeeSubscription = this.userDetailService.get().subscribe(employee => {
      this.employeeModel = employee;
      this.getModels();
      this.getSummaryModels();
    });
  }

  /**
   * Get attendance models
   */
  public getModels(): void {
    let params: HttpParams = new HttpParams();
    params = params.set('employee_id', this.employeeModel?.id);
    params = params.set('date', DateUtility.setDateFormat(new Date(), 'YYYY-MM-DD'));
    this.punchInOutSubscription = this.attendanceDetailService.getEmployeeAttendance(params).subscribe(attendanceModel => {
      this.attendanceModel = attendanceModel;
      if (this.attendanceModel?.length > 0) {
        this.attendanceModel.forEach(model => {
          model.isOpen = false;
        });
      }
      this.getCalendarData();
    });
  }

  /**
   * Get work and break hours
   */
  public getWorkAndBreakHours(): void {
    if (this.calendarModel.length > 0) {
      this.workHours = this.translateService.instant('MOD_ATTENDANCE.TOTAL_WORKING_HOUR_LABEL',
        {hour: this.calendarModel.getItem(this.calendarModel.length - 1).check_out ?
            this.getHoursAndMinutes(this.calendarModel.getItem(this.calendarModel.length - 1).work_time) :
            this.translateService.instant('MOD_ATTENDANCE.EMPTY_DATE_LABEL')});
      this.breakHours = this.translateService.instant('MOD_ATTENDANCE.TOTAL_BREAK_HOUR_LABEL',
        {hour: this.calendarModel.getItem(this.calendarModel.length - 1).check_out ?
            this.getHoursAndMinutes(this.calendarModel.getItem(this.calendarModel.length - 1).break_time) :
            this.translateService.instant('MOD_ATTENDANCE.EMPTY_DATE_LABEL')});
    }
  }

  /**
   * Get calendar data
   */
  public getCalendarData(): void {
    let params: HttpParams = new HttpParams();
    params = params.set('employee_id', this.employeeModel?.id);
    params = params.set('start_date', this.startDate);
    params = params.set('end_date', this.endDate);
    this.calendarSubscription = this.attendanceCalendarService.getCalendarDetails(params, this.employeeModel?.id).subscribe(calendarModel => {
      this.calendarModel = calendarModel;
      this.getWorkAndBreakHours();
      this.chartMonth?.resetChart();
    });
  }

  /**
   * Get start date from calendar
   */
  public getStartEndDate($event): void {
    this.startDate = $event.split(':')[0];
    this.endDate = $event.split(':')[1];
    this.getCalendarData();
    this.getSummaryModels();
  }

  public ngOnDestroy(): void {
    if (this.summarySubscription != null) {
      this.summarySubscription.unsubscribe();
    }

    if (this.punchInOutSubscription != null) {
      this.punchInOutSubscription.unsubscribe();
    }

    if (this.employeeSubscription != null) {
      this.employeeSubscription.unsubscribe();
    }

    if (this.attendanceChangeSubscription != null) {
      this.attendanceChangeSubscription.unsubscribe();
    }

    if (this.calendarSubscription != null) {
      this.calendarSubscription.unsubscribe();
    }
  }
}
