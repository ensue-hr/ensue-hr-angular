import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import { TimesheetModalComponent } from './timesheet-modal.component';
import {ViewPunchInOutHistoryComponent} from '../../../view-punch-in-out-history/view-punch-in-out-history.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MockMatDialogRef} from '@common/testing-resources/mock-services/MockMatDialogRef';

describe('TimesheetModalComponent', () => {
  let component: TimesheetModalComponent;
  let fixture: ComponentFixture<TimesheetModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TimesheetModalComponent],
      providers: [
        ...MOCK_PROVIDERS,
        { provide: MAT_DIALOG_DATA, useValue: {} },
        {provide: MatDialogRef, useClass: MockMatDialogRef},
      ],
      imports: [
        TranslateModule
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimesheetModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get attendance data on init', fakeAsync(async () => {
    spyOn(component, 'getModels');
    component.onComponentReady();
    expect(component.getModels).toHaveBeenCalled();
  }));
});
