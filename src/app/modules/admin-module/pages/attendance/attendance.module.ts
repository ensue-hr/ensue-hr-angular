import { NgModule } from '@angular/core';
import {HrCommonModule} from '@common/hr-common.module';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {AttendanceComponent} from './attendance.component';
import {ATTENDANCE_ROUTES} from './attendance-routes.module';
import {EditPunchHistoryComponent} from './components/staff-dashboard/components/edit-punch-history/edit-punch-history.component';
import {ViewPunchInOutHistoryComponent} from './components/staff-dashboard/components/view-punch-in-out-history/view-punch-in-out-history.component';
import { StaffDashboardComponent } from './components/staff-dashboard/staff-dashboard.component';
import {NgxMatTimepickerModule} from '@angular-material-components/datetime-picker';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ScheduleModule} from '@syncfusion/ej2-angular-schedule';
import { AttendanceSummaryComponent } from './components/staff-dashboard/components/attendance-summary/attendance-summary.component';
import {ChartMonthlyRecordComponent} from './components/staff-dashboard/components/chart-monthly-record/chart-monthly-record.component';
import {CalendarComponent} from './components/staff-dashboard/components/calendar/calendar.component';
import { TimesheetModalComponent } from './components/staff-dashboard/components/calendar/components/timesheet-modal/timesheet-modal.component';
import { WorkTimeChartComponent } from './components/staff-dashboard/components/calendar/components/work-time-chart/work-time-chart.component';
import { ChartDailyAttendanceComponent } from './components/staff-dashboard/components/chart-daily-attendance/chart-daily-attendance.component';

@NgModule({
  declarations: [
    AttendanceComponent,
    ViewPunchInOutHistoryComponent,
    EditPunchHistoryComponent,
    StaffDashboardComponent,
    CalendarComponent,
    ChartMonthlyRecordComponent,
    AttendanceSummaryComponent,
    TimesheetModalComponent,
    WorkTimeChartComponent,
    ChartDailyAttendanceComponent,
  ],
    imports: [
        HrCommonModule,
        RouterModule.forChild(ATTENDANCE_ROUTES),
        TranslateModule.forChild({
            extend: true
        }),
        NgxMatTimepickerModule,
        MatTooltipModule,
        ScheduleModule,
    ],
  providers: [],
})

export class AttendanceModule { }
