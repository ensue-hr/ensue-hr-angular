import {
  AfterViewInit,
  Component,
  Inject,
  Injector,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild
} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';
import {Subscription} from 'rxjs';
import {EmployeesService} from '@common/services/employees.service';
import {DepartmentService} from '@common/services/department.service';
import {DepartmentModel} from '@common/models';
import {Gender} from '@common/enums/gender.enums';
import {EmployeeModel} from '@common/models/employee.model';
import {MaritalStatus} from '@common/enums/maritalStatus.enums';
import {DateUtility} from '@common/utilities/date-utility';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AdvancedSelectConfigInterface} from '@system/components';
import {environment} from '../../../../../../../environments/environment';
import {Status} from '@common/enums/status.enums';
import {NicoUtils} from '@system/utilities';


@Component({
  selector: 'app-add-edit-employee',
  templateUrl: './add-edit-employee.component.html',
})

export class AddEditEmployeeComponent extends AbstractBaseComponent implements OnInit, OnDestroy, AfterViewInit {
  private departmentSubscription: Subscription;
  public departmentSelectionFormError = false;
  public pageTitle = '';
  private oldTitle = this.pageTitleService.getTitle().split('-')[1];
  public genders = [
    {id: Gender.Male, gender: this.translateService.instant('MOD_EMPLOYEES.MOD_ADD_EDIT_EMPLOYEE.MALE_LABEL')},
    {id: Gender.Female, gender: this.translateService.instant('MOD_EMPLOYEES.MOD_ADD_EDIT_EMPLOYEE.FEMALE_LABEL')},
    {id: Gender.Others, gender: this.translateService.instant('MOD_EMPLOYEES.MOD_ADD_EDIT_EMPLOYEE.OTHER_LABEL')},
  ];

  public maritalStatus = [
    {
      id: MaritalStatus.Married,
      status: this.translateService.instant('MOD_EMPLOYEES.MOD_ADD_EDIT_EMPLOYEE.MARRIED_LABEL')
    },
    {
      id: MaritalStatus.Single,
      status: this.translateService.instant('MOD_EMPLOYEES.MOD_ADD_EDIT_EMPLOYEE.UNMARRIED_LABEL')
    },
  ];
  public model: EmployeeModel;
  private addEmployeeSubscription: Subscription;
  private editEmployeeSubscription: Subscription;
  public employeeForm: FormGroup;
  public employeeFormID: string;
  public departmentModels: DepartmentModel;
  public today = new Date();
  protected env: any;
  @ViewChild('dropDownItemTemplate') dropDownItemTemplate: TemplateRef<any>;
  public url = '';
  public advanceSelectQueryParams = `&status=${Status.Published}`;
  public config: AdvancedSelectConfigInterface = {};

  constructor(
    injector: Injector,
    private employeeService: EmployeesService,
    private departmentService: DepartmentService,
    public dialogRef: MatDialogRef<AddEditEmployeeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    super(injector);
    this.today.setDate(this.today.getDate());
    this.env = environment;
  }

  public onSubmitForm(): void {
    this.addEmployeeSubscription = this.employeeService.saveEmployee(this.employeeForm.value, {id: this.data?.id})
      .subscribe(response => {
        this.dialogRef.close(response);
        NicoUtils.isNullOrUndefined(this.data) ?
          this.showSuccessSnackBar(this.translateService.instant('MOD_EMPLOYEES.MOD_ADD_EDIT_EMPLOYEE.ADD_SUCCESS_MESSAGE_LABEL')) :
          this.showSuccessSnackBar(this.translateService.instant('MOD_EMPLOYEES.MOD_ADD_EDIT_EMPLOYEE.EDIT_SUCCESS_MESSAGE_LABEL'));
      }, error => {
        this.onFormSubmissionError(error, this.employeeFormID);
      });
  }

  public onComponentReady(): void {
    if (this.data) {
      this.pageTitle = this.translateService.instant('MOD_EMPLOYEES.MOD_ADD_EDIT_EMPLOYEE.EDIT_PAGE_TITLE');
    } else {
      this.pageTitle = this.translateService.instant('MOD_EMPLOYEES.MOD_ADD_EDIT_EMPLOYEE.ADD_PAGE_TITLE');
    }
    super.onComponentReady();
    this.getDepartmentModels();
    this.createForm();
    this.url = this.env.api + 'departments';
    this.employeeFormID = 'employee-form-id';
  }

  public createForm(): void {
    this.employeeForm = new FormGroup({
      first_name: new FormControl(null, [Validators.required]),
      last_name: new FormControl(null, [Validators.required]),
      middle_name: new FormControl(null),
      email: new FormControl(null, [Validators.required, Validators.email,
        Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
      ]),
      personal_email: new FormControl(null, [Validators.required, Validators.email,
        Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
      ]),
      position: new FormControl(null, [Validators.required]),
      joined_at: new FormControl(null, [Validators.required]),
      gender: new FormControl(Gender.Male),
      dob: new FormControl(null, [Validators.required]),
      marital_status: new FormControl(MaritalStatus.Single),
      pan_no: new FormControl(null),
      department_id: new FormControl(null, [Validators.required])
    });

    if (this.data) {
      this.setFormData(this.data);
    }
  }

  private setFormData(dataModel: EmployeeModel): void {
    this.employeeForm.patchValue(dataModel);
    this.employeeForm.get('joined_at').setValue(DateUtility.getMomentObject(dataModel.joined_at));
    this.employeeForm.get('dob').setValue(DateUtility.getMomentObject(dataModel.dob));
    this.employeeForm.controls.email.disable();
  }

  public getDepartmentModels(): void {
    this.departmentSubscription = this.departmentService.get().subscribe(models => {
      this.departmentModels = models;
    });
  }

  public ngOnDestroy(): void {
    this.pageTitleService.setTitle(this.oldTitle);
    if (this.addEmployeeSubscription != null) {
      this.addEmployeeSubscription.unsubscribe();
    }
    if (this.departmentSubscription != null) {
      this.departmentSubscription.unsubscribe();
    }
    if (this.editEmployeeSubscription != null) {
      this.editEmployeeSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    this.config = {
      itemTemplate: this.dropDownItemTemplate,
      selectInfoLabel: this.translateService.instant('MOD_EMPLOYEES.MOD_ADD_EDIT_EMPLOYEE.SELECT_DEPARTMENT_LABEL'),
      equalityCheckProperty: 'id',
      disableEmptyField: true
    };
  }

  public setDepartmentId(department): void {
    if (department) {
      this.employeeForm.get('department_id').setValue(department.id);
      this.departmentSelectionFormError = false;
    } else {
      this.departmentSelectionFormError = true;
    }
  }
}
