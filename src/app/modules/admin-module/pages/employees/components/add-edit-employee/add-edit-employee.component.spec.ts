import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';

import { AddEditEmployeeComponent } from './add-edit-employee.component';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService, NicoStorageService} from '@system/services';
import {EmployeesService} from '@common/services/employees.service';
import {MockEmployeesService} from '@common/testing-resources/mock-services/MockEmployeesService';
import {NicoHttpClient} from '@system/http-client';
import {
  MockMatDialog,
  MockMatSnackBar, MockMissingTranslationHandler,
  MockNicoHttpClient,
  MockNicoPageTitle,
  MockNicoSessionService,
  MockNicoStorageService
} from '@common/testing-resources/mock-services';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MockFormGroup} from '@common/testing-resources/mock-services/MockFormGroup';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {
  DEFAULT_LANGUAGE,
  MissingTranslationHandler,
  TranslateCompiler,
  TranslateFakeCompiler,
  TranslateFakeLoader,
  TranslateLoader, TranslateModule, TranslateParser,
  TranslateService,
  TranslateStore, USE_DEFAULT_LANG, USE_EXTEND, USE_STORE
} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {EmployeeModel} from '@common/models/employee.model';
import {By} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {MockMatDialogRef} from '@common/testing-resources/mock-services/MockMatDialogRef';
import {BreadcrumbService} from 'xng-breadcrumb';

describe('AddEditEmployeeComponent', () => {
  let component: AddEditEmployeeComponent;
  let fixture: ComponentFixture<AddEditEmployeeComponent>;
  let htmlError: HttpErrorResponse;
  let pageTitleService: NicoPageTitle;
  let service: EmployeesService;
  const mockData = {
    avatar: null,
    code: 'EN200701',
    created_at: '2021-07-20T08:19:36.000000Z',
    department: 'Human Resources',
    department_id: 1,
    email: 'manish.shakya@ensue.us',
    id: 1,
    joined_at: '2007-01-01',
    name: 'Manish Ratna Shakya',
    position: 'Junior Frontend Developer',
    pan_number: '123123123',
    status: 1
  } as unknown as EmployeeModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditEmployeeComponent ],
      providers: [
        {provide: EmployeesService, useClass: MockEmployeesService},
        {provide: NicoHttpClient, useClass: MockNicoHttpClient},
        {provide: FormGroup, useClass: MockFormGroup},
        {provide: NicoStorageService, useClass: MockNicoStorageService},
        {provide: NicoPageTitle, useClass: MockNicoPageTitle},
        {provide: NicoSessionService, useClass: MockNicoSessionService},
        {provide: MatSnackBar, useClass: MockMatSnackBar},
        {provide: MatDialog, useClass: MockMatDialog},
        {provide: MatDialogRef, useValue: MockMatDialogRef},
        {provide: MAT_DIALOG_DATA, useValue: null},
        {provide: HttpErrorResponse, useValue: {headers: {}, error: {message: 'Test', status: '417'}}},
        {provide: Router, useValue: jasmine.createSpyObj('Router', ['navigate'])},
        {provide: ActivatedRoute, useValue:  { snapshot: {queryParams: {page: 1}}}},
        FormBuilder,
        AjaxSpinnerService,
        TranslateService,
        TranslateStore,
        {provide: TranslateLoader, useClass: TranslateFakeLoader},
        {provide: TranslateCompiler, useClass: TranslateFakeCompiler},
        TranslateParser,
        {provide: MissingTranslationHandler, useClass: MockMissingTranslationHandler},
        {provide: USE_DEFAULT_LANG, useValue: undefined},
        {provide: USE_STORE, useValue: undefined},
        {provide: USE_EXTEND, useValue: undefined},
        {provide: DEFAULT_LANGUAGE, useValue: undefined},
        {provide: BreadcrumbService, useValue: undefined}
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        CommonModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditEmployeeComponent);
    pageTitleService = TestBed.inject(NicoPageTitle);
    htmlError = TestBed.inject(HttpErrorResponse);
    component = fixture.componentInstance;
    service = TestBed.inject(EmployeesService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change the title as Edit employee on init', () => {
    component.data = {id: 1};
    component.ngOnInit();
    expect(component.pageTitle).toEqual('MOD_EMPLOYEES.MOD_ADD_EDIT_EMPLOYEE.EDIT_PAGE_TITLE');
  });

  it('should change the title as Add employee on init', () => {
    component.ngOnInit();
    expect(component.pageTitle).toEqual('MOD_EMPLOYEES.MOD_ADD_EDIT_EMPLOYEE.ADD_PAGE_TITLE');
  });

  it('should change the html title as Edit employee', () => {
    component.data = {id: 1};
    fixture.detectChanges();
    component.ngOnInit();
    const title: DebugElement =
      fixture.debugElement.query(By.css('.modal-title'));
    expect(title.nativeElement.outerText).toEqual('MOD_EMPLOYEES.MOD_ADD_EDIT_EMPLOYEE.EDIT_TITLE');
  });

  it('should change the html title as Add employee', () => {
    const title: DebugElement =
      fixture.debugElement.query(By.css('.modal-title'));
    expect(title.nativeElement.outerText).toEqual('MOD_EMPLOYEES.MOD_ADD_EDIT_EMPLOYEE.ADD_TITLE');
  });

  it('should create form on init', () => {
    spyOn(component, 'createForm');
    component.ngOnInit();
    expect(component.createForm).toHaveBeenCalled();
  });

  it('should post data on save', fakeAsync(async () => {
    spyOn(component, 'onSubmitForm');
    component.onSubmitForm();
    expect(component.onSubmitForm).toHaveBeenCalled();
    service.saveEmployee(mockData).subscribe( response => {
    });
    service.get().subscribe(response => {
      expect(response.length).toBe(2);
    });
    flush();
  }));

  it('should throw error on invalid data', fakeAsync(() => {
    const dummyData = {
      avatar: null,
      code: 'EN200701',
      department: 'Human Resources',
      department_id: 1,
      email: 'manish.shakya@ensue.us',
      id: 1,
      joined_at: '2007-01-01',
      name: 'Manish Ratna Shakya',
      position: 'Junior Frontend Developer',
      pan_number: '123',
      status: 1
    } as unknown as EmployeeModel;
    service.saveEmployee(dummyData).subscribe(response => {
      expect(response.error).toEqual(htmlError.error);
    });
  }));


  it('save button should be disabled if the form is not valid', () => {
    component.employeeForm.setValue({
      department_id: 1,
      email: null,
      joined_at: '',
      first_name: '',
      middle_name: '',
      last_name: '',
      position: '',
      personal_email: '',
      gender: 2,
      dob: '',
      marital_status: 1,
      pan_no: null
    } );
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('.save')).nativeElement.disabled).toBe(true);
  });

  it('save button enable if the form is valid', () => {
    component.employeeForm.setValue({
      department_id: 1,
      email: 'manish@gmail.com',
      joined_at: '2015-01-01',
      first_name: 'manish',
      middle_name: 'ratna',
      last_name: 'shakya',
      position: 'jr hr',
      personal_email: 'manish@gmail.com',
      gender: 2,
      dob: '2015-01-01',
      marital_status: 1,
      pan_no: 123321123
    } );
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('.save')).nativeElement.disabled).not.toBe(true);
  });

  it('should call onSubmit on click save btn', () => {
    spyOn(component, 'onSubmitForm');
    const submitButton: DebugElement =
      fixture.debugElement.query(By.css('button[type=submit]'));
    fixture.detectChanges();
    submitButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.onSubmitForm).toHaveBeenCalledTimes(1);
  });

  it('should close drawer on cancel form', () => {
    const cancelBtn: DebugElement =
      fixture.debugElement.query(By.css('.cancel-btn'));
    cancelBtn.triggerEventHandler('click', null);
    expect(cancelBtn.nativeElement.getAttribute('mat-dialog-close')).toEqual('');
  });
});
