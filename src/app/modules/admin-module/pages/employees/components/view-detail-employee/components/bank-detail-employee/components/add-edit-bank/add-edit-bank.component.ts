import {Component, Inject, Injector, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EmployeeContactsModel} from '@common/models';
import {Status} from '@common/enums/status.enums';
import {AbstractBaseComponent} from '../../../../../../../../../../AbstractBaseComponent';
import {NicoUtils} from '@system/utilities';
import {EmployeeBanksService} from '@common/services';

@Component({
  selector: 'app-add-edit-bank',
  templateUrl: './add-edit-bank.component.html',
  styleUrls: ['./add-edit-bank.component.scss']
})
export class AddEditBankComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Page Title
   */
  public pageTitle = '';

  /**
   * Subscription
   */
  public addBankSubscription: Subscription;

  /**
   * Form
   */
  public bankForm: FormGroup;

  public bankFormId = 'bank-form';

  public switchState = false;

  /**
   * Constructor
   */
  constructor(
    protected injector: Injector,
    @Inject(MAT_DIALOG_DATA) public modal: EmployeeContactsModel,
    public dialogRef: MatDialogRef<AddEditBankComponent>,
    public employeeService: EmployeeBanksService) {
    super(injector);
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.modal.content ?
      this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_BANKS.EDIT_BANK_PAGE_TITLE') :
      this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_BANKS.ADD_BANK_PAGE_TITLE');
    super.onComponentReady();
    this.createForm();
  }

  /**
   * Create Form
   */
  public createForm(): void {
    this.bankForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      branch: new FormControl('', [Validators.required]),
      account_number: new FormControl('', [Validators.required]),
      status: new FormControl(Status.Published, [Validators.required]),
    });
    if (this.modal.content) {
      this.bankForm.patchValue(this.modal.content);
      this.switchState = this.modal.content.stats === Status.Published;
    }
  }

  /**
   * On Form Submit
   */
  public onFormSubmit(): void {
    this.addBankSubscription = this.employeeService.saveBank(this.bankForm.getRawValue(), this.modal.empId, this.modal.content)
      .subscribe(response => {
        this.dialogRef.close(response);
        NicoUtils.isNullOrUndefined(this.modal.content) ?
          this.showSuccessSnackBar(this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_BANKS.ADD_SUCCESS_MESSAGE_LABEL')) :
          this.showSuccessSnackBar(this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_BANKS.EDIT_SUCCESS_MESSAGE_LABEL'));
      }, error => {
        this.onFormSubmissionError(error, this.bankFormId);
      });
  }

  /**
   * On Destroy
   */
  public ngOnDestroy(): void {
    if (this.addBankSubscription != null) {
      this.addBankSubscription.unsubscribe();
    }
    this.pageTitleService.setTitle(this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_BANKS.PAGE_TITLE'));
  }

}
