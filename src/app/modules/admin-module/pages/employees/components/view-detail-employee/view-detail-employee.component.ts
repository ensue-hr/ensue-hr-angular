import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';
import {EmployeeModel} from '@common/models';
import {Subscription} from 'rxjs';
import {EmployeesService} from '@common/services';

@Component({
  selector: 'app-view-detail-employee',
  templateUrl: './view-detail-employee.component.html',
})
export class ViewDetailEmployeeComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  public model: EmployeeModel;
  public viewDetailEmployeeSubscription: Subscription;
  public employeeSubscription: Subscription;

  public componentsList = [
    {name: this.translateService
        .instant( 'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.BASIC_INFORMATION_LABEL'), link: 'basic-info', icons: 'far fa-info-circle'},
    {name: this.translateService.instant( 'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.ADDRESS_LABEL'),
      link: 'address', icons: 'far fa-home'},
    {name: this.translateService.instant( 'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.CONTACTS_LABEL'), link: 'contact', icons: 'far fa-phone-alt'},
    {name: this.translateService.instant( 'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.BANK_LABEL'),
      link: 'bank-details', icons: 'far fa-piggy-bank'},
    {name: this.translateService.instant( 'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.SOCIAL_SECURITY_LABEL'),
      link: 'social-security' , icons: 'far fa-user-lock'},
    {name: this.translateService.instant( 'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.DOCUMENTS_LABEL'), link: 'documents' , icons: 'far fa-file'},
    {name: this.translateService.instant( 'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.ALLOCATED_LEAVES_LABEL'),
      link: 'allocated-leaves' , icons: 'far fa-calendar-minus'},
    {name: this.translateService.instant( 'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.HISTORIES_LABEL'), link: 'histories' , icons: 'far fa-history'},
    ];

  constructor(injector: Injector, private employeeService: EmployeesService) {
    super(injector);
    this.breadcrumbService.set('@employeeName', ' ');
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.getModel();
    this.employeeSubscription = this.employeeService.watchUserChange().subscribe(model => {
      this.model = model;
      this.breadcrumbService.set('@employeeName', this.model?.name);
    });
  }

  public getModel(): void{
    this.viewDetailEmployeeSubscription = this.activatedRoute.data.subscribe(model => {
      this.model = model.employeeResolver;
      this.breadcrumbService.set('@employeeName', this.model?.name);
    });
  }

  ngOnDestroy(): void {
  }
}
