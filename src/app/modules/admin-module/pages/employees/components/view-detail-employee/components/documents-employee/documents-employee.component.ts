import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {Status} from '@common/enums/status.enums';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {MatDialog} from '@angular/material/dialog';
import {EmployeeDocumentsService} from '@common/services/employee-documents.service';
import {EmployeeDocumentsModel} from '@common/models';
import {MatDialogRef} from '@angular/material/dialog/dialog-ref';
import {Subscription} from 'rxjs';
import {AddEditDocumentsComponent} from './components/add-edit-documents/add-edit-documents.component';
import {PaginatedNicoCollection} from '@system/utilities';
import {FlyMenuItemInterface} from '@system/components';
import {environment} from '../../../../../../../../../environments/environment';

@Component({
  selector: 'app-documents-employee',
  templateUrl: './documents-employee.component.html',
  styleUrls: ['./documents-employee.component.scss']
})
export class DocumentsEmployeeComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Enum
   */
  public status = Status;

  /**
   * Environment
   */
  public env: any;

  /**
   * Models
   */
  public documentModel: PaginatedNicoCollection<EmployeeDocumentsModel>;

  /**
   * Page Title
   */
  public pageTitle = '';

  /**
   * Subscriptions
   */
  public documentSubscription: Subscription;

  public documentDeleteSubscription: Subscription;

  /**
   * MatDialogRef
   */
  public dialogRef: MatDialogRef<EmployeeDocumentsModel>;

  /**
   * Fly menu addons
   */
  public flyMenuAddOns: FlyMenuItemInterface[] = [
    {
      name: 'edit',
      label: this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.EDIT_BUTTON_LABEL'),
      active: true,
      faIcon: 'far fa-pen'
    },
    {
      name: 'remove',
      label: this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.REMOVE_BUTTON_LABEL'),
      active: true,
      faIcon: 'far fa-trash'
    }
  ];

  /**
   * Constructor
   */
  constructor(protected injector: Injector,
              public employeesService: EmployeeDocumentsService,
              protected dialog: MatDialog) {
    super(injector);
    this.env = environment;
  }

  /**
   * Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_DOCUMENTS.PAGE_TITLE');
    super.onComponentReady();
    this.getModels();
  }

  /**
   * Get all documents
   */
  public getModels(): void {
    this.documentSubscription = this.employeesService.getDocuments(this.activatedRoute.snapshot.parent.params.id).subscribe(documents => {
      this.documentModel = documents;
    }, error => {
    });
  }

  public getFlyMenuAdOns(): FlyMenuItemInterface[] {
    return [...this.flyMenuAddOns];
  }

  /**
   * Fly menu cases
   */
  public onFlyMenuAction(event: any, model: EmployeeDocumentsModel): void {
    switch (event) {
      case 'remove':
        this.onDelete(model);
        break;

      case 'edit':
        this.onAddEditDocument(model);
        break;
    }
  }

  /**
   * Add/edit new contact (Open drawer)
   */
  public onAddEditDocument(model?: EmployeeDocumentsModel): void {
    const modalDialog = this.dialog.open(AddEditDocumentsComponent, {
      data: {content: model, empId: this.activatedRoute.snapshot.parent.params.id},
      panelClass: ['modal-drawer-view', 'modal-xs'],
      disableClose: true
    });
    modalDialog.afterClosed().subscribe(reload => {
      if (reload) {
        this.getModels();
      }
    });
  }

  /**
   * On Document delete
   */
  public onDelete(model: EmployeeDocumentsModel): void {
    this.dialogRef = this.openConfirmationDialogBox({
      title: this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_DOCUMENTS.DELETE_MODAL_TITLE'),
      message: this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_DOCUMENTS.DELETE_MODAL_MESSAGE')
    });
    this.dialogRef.afterClosed().subscribe(confirmation => {
      if (confirmation) {
        this.documentDeleteSubscription = this.employeesService.
        deleteDocument(this.activatedRoute.snapshot.parent.params.id, model).subscribe(response => {
          this.dialogRef.close(true);
          this.getModels();
        });
      }
    });
  }

}
