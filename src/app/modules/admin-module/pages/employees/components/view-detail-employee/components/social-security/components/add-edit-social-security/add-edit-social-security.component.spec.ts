import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';
import { AddEditSocialSecurityComponent } from './add-edit-social-security.component';
import {NicoHttpClient} from '@system/http-client';
import {
  MockMatDialog,
  MockMatSnackBar, MockMissingTranslationHandler,
  MockNicoHttpClient,
  MockNicoPageTitle,
  MockNicoSessionService,
  MockNicoStorageService
} from '@common/testing-resources/mock-services';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MockFormGroup} from '@common/testing-resources/mock-services/MockFormGroup';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService, NicoStorageService} from '@system/services';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MockMatDialogRef} from '@common/testing-resources/mock-services/MockMatDialogRef';
import {ActivatedRoute, Router} from '@angular/router';
import {
  DEFAULT_LANGUAGE,
  MissingTranslationHandler,
  TranslateCompiler,
  TranslateFakeCompiler,
  TranslateFakeLoader,
  TranslateLoader, TranslateModule, TranslateParser,
  TranslateService,
  TranslateStore, USE_DEFAULT_LANG, USE_EXTEND, USE_STORE
} from '@ngx-translate/core';
import {CommonModule} from '@angular/common';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserModule, By} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {SocialSecurityService} from '@common/services/social-security.service';
import {MockSocialSecurity} from '@common/testing-resources/mock-services/MockSocialSecurityService';
import {SocialSecurityModel} from '@common/models/social-security.model';
import {BreadcrumbService} from 'xng-breadcrumb';

describe('AddEditSocialSecurityComponent', () => {
  let component: AddEditSocialSecurityComponent;
  let fixture: ComponentFixture<AddEditSocialSecurityComponent>;
  let service: SocialSecurityService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditSocialSecurityComponent ],
      providers: [
        {provide: SocialSecurityService, useClass: MockSocialSecurity},
        {provide: NicoHttpClient, useClass: MockNicoHttpClient},
        {provide: FormGroup, useClass: MockFormGroup},
        {provide: NicoStorageService, useClass: MockNicoStorageService},
        {provide: NicoPageTitle, useClass: MockNicoPageTitle},
        {provide: NicoSessionService, useClass: MockNicoSessionService},
        {provide: MatSnackBar, useClass: MockMatSnackBar},
        {provide: MatDialog, useClass: MockMatDialog},
        {provide: MatDialogRef, useValue: MockMatDialogRef},
        {provide: MAT_DIALOG_DATA, useValue: {id: 1}},
        {provide: Router, useValue: jasmine.createSpyObj('Router', ['navigate'])},
        {provide: ActivatedRoute, useValue:  { snapshot: {parent: {params: {id: 1}}}}},
        FormBuilder,
        AjaxSpinnerService,
        TranslateService,
        TranslateStore,
        {provide: TranslateLoader, useClass: TranslateFakeLoader},
        {provide: TranslateCompiler, useClass: TranslateFakeCompiler},
        TranslateParser,
        {provide: MissingTranslationHandler, useClass: MockMissingTranslationHandler},
        {provide: USE_DEFAULT_LANG, useValue: undefined},
        {provide: USE_STORE, useValue: undefined},
        {provide: USE_EXTEND, useValue: undefined},
        {provide: DEFAULT_LANGUAGE, useValue: undefined},
        {provide: BreadcrumbService, useValue: undefined},

      ],
      imports: [
        CommonModule,
        RouterTestingModule,
        TranslateModule,
        BrowserModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditSocialSecurityComponent);
    service = TestBed.inject(SocialSecurityService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change html title to add social security', () => {
    component.ngOnInit();
    expect(component.pageTitle).toEqual('MOD_SOCIAL_SECURITY.MOD_ADD_EDIT_SOCIAL_SECURITY.HTML_TITLE_ADD');

  });

  it('should change html title to edit social security', () => {
    component.modal.model = {id: 1};
    component.ngOnInit();
    expect(component.pageTitle).toEqual('MOD_SOCIAL_SECURITY.MOD_ADD_EDIT_SOCIAL_SECURITY.HTML_TITLE_EDIT');
  });

  it('should set add page title in case of add', fakeAsync (async () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_SOCIAL_SECURITY.MOD_ADD_EDIT_SOCIAL_SECURITY.HTML_TITLE_ADD');
    flush();
  }));
  it('should change page title to add social security', () => {
    const title = fixture.debugElement.query(By.css('.modal-title'));
    expect(title.nativeElement.outerText).toEqual('MOD_SOCIAL_SECURITY.MOD_ADD_EDIT_SOCIAL_SECURITY.HTML_TITLE_ADD');

  });

  it('should change page title to edit social security', () => {
    component.modal.model = {id: 1};
    component.ngOnInit();
    fixture.detectChanges();
    const title = fixture.debugElement.query(By.css('.modal-title'));
    expect(title.nativeElement.outerText).toEqual('MOD_SOCIAL_SECURITY.MOD_ADD_EDIT_SOCIAL_SECURITY.HTML_TITLE_EDIT');
  });

  it('should disable submit button on error', () => {
    component.socialSecurityForm.setValue({
      title: null,
      number: null,
      start_date: null,
      end_date: null,
      status: '2'
    });
    const saveBtn = fixture.debugElement.query(By.css('button[type=submit]'));
    expect(saveBtn.nativeElement.disabled).toBe(true);
  });

  it('should be empty form on initial', () => {
    const initialValue = {
      title: null,
      number: null,
      start_date: null,
      end_date: null,
      status: 2
    };
    expect(component.socialSecurityForm.value).toEqual(initialValue);
  });

  it('should post data on save', () => {
    const dummyData = {
      title: 'manish',
      number: '123123123',
      start_date: '2021-02-02',
      end_date: null,
      status: 1
    } as unknown as SocialSecurityModel;
    service.postSocialSecurity('1', dummyData).subscribe(response => {
      expect(response.body).toBe(true);
    });
  });
});
