import {
  ComponentFixture,
  fakeAsync,
  flush,
  TestBed,
} from '@angular/core/testing';
import { HistoryEmployeeComponent } from './history-employee.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MockEmployeesService} from '@common/testing-resources/mock-services/MockEmployeesService';
import {EmployeeHistoryService} from '@common/services';
import {EmployeeHistoriesModel} from '@common/models/employee-histories.model';
import {MockHistoryModel} from '@common/testing-resources/mock-model/MockHistoryModel';

describe('HistoryEmployeeComponent', () => {
  let component: HistoryEmployeeComponent;
  let fixture: ComponentFixture<HistoryEmployeeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoryEmployeeComponent ],
      providers: [
        ...MOCK_PROVIDERS,
        {
          provide: ActivatedRoute,
          useValue:
            {
              snapshot: {
                queryParams: {
                  page: 1, sort_order: 'asc', per_page: 16
                },
                parent: {
                  params: {
                    id: 1
                  }
                }
              }
            }
        },
        {provide: EmployeeHistoryService, useClass: MockEmployeesService},
        {provide: EmployeeHistoriesModel, useClass: MockHistoryModel}
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        ReactiveFormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set page title', fakeAsync (async () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_HISTORIES.PAGE_TITLE');
    flush();
  }));

  it('should open add attendance-history drawer on clicking new button', () => {
    spyOn(component, 'onToggleEmployeeActivation');
    component.onComponentReady();
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    fixture.whenStable();
    expect(component.onToggleEmployeeActivation).toHaveBeenCalled();
  });

  // it('should retrieve data', fakeAsync (async () => {
  //   spyOn(component.employeesService, 'getHistories').and.returnValue(
  //     of({comment: 'test comment', url: 'test', thumbnail: 'test.png'}
  //     ) as unknown as Observable<PaginatedNicoCollection<EmployeeHistoriesModel>>);
  //   component.onComponentReady();
  //   await fixture.whenStable();
  //   await discardPeriodicTasks();
  //   await flush();
  //   await flushMicrotasks();
  //   await tick(15000);
  //   expect(component.historyModel).not.toBeNull();
  // }));


});
