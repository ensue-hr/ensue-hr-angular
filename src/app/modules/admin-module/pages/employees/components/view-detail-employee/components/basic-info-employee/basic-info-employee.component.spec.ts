import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import { BasicInfoEmployeeComponent } from './basic-info-employee.component';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService, NicoStorageService} from '@system/services';
import {EmployeesService} from '@common/services/employees.service';
import {MockEmployeesService} from '@common/testing-resources/mock-services/MockEmployeesService';
import {NicoHttpClient} from '@system/http-client';
import {
  MockMatDialog,
  MockMatSnackBar, MockMissingTranslationHandler,
  MockNicoHttpClient,
  MockNicoPageTitle,
  MockNicoSessionService,
  MockNicoStorageService
} from '@common/testing-resources/mock-services';
import {FormBuilder, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {MockFormGroup} from '@common/testing-resources/mock-services/MockFormGroup';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ActivatedRoute} from '@angular/router';
import {
  DEFAULT_LANGUAGE,
  MissingTranslationHandler,
  TranslateCompiler,
  TranslateFakeCompiler,
  TranslateFakeLoader,
  TranslateLoader, TranslateModule, TranslateParser,
  TranslateService,
  TranslateStore, USE_DEFAULT_LANG, USE_EXTEND, USE_STORE
} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {EmployeeModel} from '@common/models';
import {of} from 'rxjs';
import {BreadcrumbService} from 'xng-breadcrumb';
import {MockBreadcrumService} from '@common/testing-resources/mock-services/MockBreadcrumService';

describe('BasicInfoEmployeeComponent', () => {
  let component: BasicInfoEmployeeComponent;
  let fixture: ComponentFixture<BasicInfoEmployeeComponent>;
  let pageTitleService: NicoPageTitle;
  let service: EmployeesService;
  const mockData = {
    avatar: null,
    code: 'EN200701',
    created_at: '2021-07-20T08:19:36.000000Z',
    department: 'Human Resources',
    department_id: 1,
    email: 'manish.shakya@ensue.us',
    id: 1,
    joined_at: '2007-01-01',
    name: 'Manish Ratna Shakya',
    position: 'Junior Frontend Developer',
    status: 1
  } as unknown as EmployeeModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BasicInfoEmployeeComponent ],
      providers: [
        {provide: EmployeesService, useClass: MockEmployeesService},
        {provide: NicoHttpClient, useClass: MockNicoHttpClient},
        {provide: FormGroup, useClass: MockFormGroup},
        {provide: NicoStorageService, useClass: MockNicoStorageService},
        {provide: NicoPageTitle, useClass: MockNicoPageTitle},
        {provide: NicoSessionService, useClass: MockNicoSessionService},
        {provide: MatSnackBar, useClass: MockMatSnackBar},
        {provide: MatDialog, useClass: MockMatDialog},
        {provide: MatDialogRef, useValue: MockMatDialog},
        {provide: ActivatedRoute, useValue: { snapshot: {parent: {params: {id: 1}}}}},
        FormBuilder,
        AjaxSpinnerService,
        TranslateService,
        TranslateStore,
        {provide: TranslateLoader, useClass: TranslateFakeLoader},
        {provide: TranslateCompiler, useClass: TranslateFakeCompiler},
        TranslateParser,
        {provide: MissingTranslationHandler, useClass: MockMissingTranslationHandler},
        {provide: USE_DEFAULT_LANG, useValue: undefined},
        {provide: USE_STORE, useValue: undefined},
        {provide: USE_EXTEND, useValue: undefined},
        {provide: DEFAULT_LANGUAGE, useValue: undefined},
        {provide: BreadcrumbService, useValue: MockBreadcrumService}
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        ReactiveFormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]

    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicInfoEmployeeComponent);
    component = fixture.componentInstance ;
    service = TestBed.inject(EmployeesService);
    pageTitleService = TestBed.inject(NicoPageTitle);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });



  it('should open edit modal on click edit ', () => {
    component.model = mockData;
    fixture.detectChanges();
    spyOn(component, 'onEdit');
    const editBtn: DebugElement =
      fixture.debugElement.query(By.css('#edit-btn'));
    editBtn.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.onEdit).toHaveBeenCalled();
  });

  it('should change page title on init', () => {
    component.ngOnInit();
    expect(component.pageTitle).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.PAGE_TITLE');
  });

  it('should have appropriate personal information labels', () => {
    component.model = mockData;
    component.isPersonalInformation = true;
    fixture.detectChanges();
    const genderTitle: DebugElement =
      fixture.debugElement.query(By.css('.gender_label'));
    const emailTile: DebugElement =
      fixture.debugElement.query(By.css('.email_label'));
    const dobTitle: DebugElement =
      fixture.debugElement.query(By.css('.dob_label'));
    const statusTitle: DebugElement =
      fixture.debugElement.query(By.css('.status_label'));
    const panTitle: DebugElement =
      fixture.debugElement.query(By.css('.pan_label'));
    expect(genderTitle.nativeElement.outerText).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.GENDER_LABEL');
    expect(emailTile.nativeElement.outerText).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.PERSONAL_EMAIL_LABEL');
    expect(dobTitle.nativeElement.outerText).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.DATE_OF_BIRTH_LABEL');
    expect(statusTitle.nativeElement.outerText).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.MARTIAL_STATUS_LABEL');
    expect(panTitle.nativeElement.outerText).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.PAN_STATUS_LABEL');
  });

  it('should reload in case of edit', fakeAsync (() => {
    spyOn(component.dialog, 'open')
      .and
      .returnValue({afterClosed: () => of(true)} as unknown as MatDialogRef<any>);
    spyOn(component, 'getModel');
    fixture.detectChanges();
    component.onEdit();
    fixture.detectChanges();
    fixture.whenStable();
    expect(component.getModel).toHaveBeenCalled();
  }));

});
