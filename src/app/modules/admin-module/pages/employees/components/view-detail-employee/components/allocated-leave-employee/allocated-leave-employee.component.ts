import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {AllocatedLeaveService} from '@common/services/allocated-leaves.service';
import {PaginatedNicoCollection} from '@system/utilities';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {AllocatedLeaveModel} from '@common/models/allocated-leaves.model';
import {AddEditAllocatedLeaveComponent} from './components/add-edit-allocated-leave/add-edit-allocated-leave.component';
import {Status} from '@common/enums/status.enums';
import {FlyMenuItemInterface} from '@system/components';
import {EmployeeBanksModel} from '@common/models';
import {Subscription} from 'rxjs';
import * as moment from 'moment';

// @ts-ignore
@Component({
  selector: 'app-allocated-leave-employee',
  templateUrl: './allocated-leave-employee.component.html',
})
export class AllocatedLeaveEmployeeComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  public model: PaginatedNicoCollection<AllocatedLeaveModel>;
  public employeeId = this.activatedRoute.snapshot.parent.params;
  public deleteAllocatedLeaveSubscription: Subscription;
  public allocatedLeaveSubscription: Subscription;
  public statusToggleSubscription: Subscription;
  public switchStatus: boolean;
  public status = Status;
  public pageTitle = '';

  public dialogRef: MatDialogRef<AllocatedLeaveEmployeeComponent>;
  public flyMenuAddOns: FlyMenuItemInterface[] = [
    {
      name: 'edit',
      label: this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.EDIT_BUTTON_LABEL'),
      active: true,
      faIcon: 'far fa-pen'
    },
    {
      name: 'remove',
      label: this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.REMOVE_BUTTON_LABEL'),
      active: true,
      faIcon: 'far fa-trash'
    }
  ];

  constructor(injector: Injector, protected dialog: MatDialog, public allocatedLeaveService: AllocatedLeaveService) {
    super(injector);
    this.switchStatus = false;
  }

  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_ALLOCATED_LEAVE.PAGE_TITLE');
    super.onComponentReady();
    this.getModels();
  }

  public getModels(): void {
    this.filterQueryParams.sort_by = 'start_date';
    this.filterQueryParams.sort_order = 'desc';
    this.setFilterHttpQueryParams();
    this.allocatedLeaveSubscription = this.allocatedLeaveService
      .getAllocatedLeaves(this.employeeId.id, this.getFilterHttpQueryParams()).subscribe(model => {
      this.model = model;
    } );
  }

  public onAddEditLeave(model?: AllocatedLeaveModel): void {
    const modalDialog = this.dialog.open(AddEditAllocatedLeaveComponent, {
      data: { content: model, empId: this.employeeId.id},
      panelClass: ['modal-drawer-view', 'modal-xs'],
      disableClose: true
    });
    modalDialog.afterClosed().subscribe(reload => {
      if (reload) {
        this.getModels();
      }
    });
  }

  public onFlyMenuAction(event: any, model: EmployeeBanksModel): void {
    switch (event) {
      case 'remove':
        this.onRemove(model);
        break;

      case 'edit':
        if (this.getCurrentDate() < model.start_date || model.status === Status.Unpublished){
          this.onAddEditLeave(model);
        }
        else{
          this.showErrorSnackBar(this.translateService.instant('MOD_ALLOCATED_LEAVE.NO_MODIFIED_ERROR'));
        }
        break;
    }
  }

  public getFlyMenuAdOns(): FlyMenuItemInterface[] {
    return [...this.flyMenuAddOns];
  }

  public onRemove(model: AllocatedLeaveModel): void {
    this.dialogRef = this.openConfirmationDialogBox({
      title: this.translateService.instant('MOD_ALLOCATED_LEAVE.DELETE_MODAL_TITLE'),
      message: this.translateService.instant('MOD_ALLOCATED_LEAVE.DELETE_MODAL_MESSAGE')
    });
    this.dialogRef.afterClosed().subscribe(confirmation => {
      if (confirmation) {
        this.deleteAllocatedLeaveSubscription = this.allocatedLeaveService.
        deleteAllocatedLeave(this.employeeId.id, model).subscribe(response => {
          this.dialogRef.close(true);
          this.getModels();
        });
      }
    });
  }

  public getCurrentDate(): any {
    return moment().format('YYYY-MM-DD');
  }

  public ngOnDestroy(): void {
    if (this.allocatedLeaveSubscription != null){
      this.allocatedLeaveSubscription.unsubscribe();
    }
    if (this.deleteAllocatedLeaveSubscription != null){
      this.deleteAllocatedLeaveSubscription.unsubscribe();
    }
    if (this.statusToggleSubscription != null){
      this.statusToggleSubscription.unsubscribe();
    }
  }
}
