import {Component, Inject, Injector, OnDestroy, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../../../../../AbstractBaseComponent';
import {Subscription} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EmployeeContactsModel} from '@common/models';
import {EmployeeContact} from '@common/enums/employeeView.enums';
import {Status} from '@common/enums/status.enums';
import {NicoUtils} from '@system/utilities';
import {EmployeeContactsService} from '@common/services/employee-contacts.service';

@Component({
  selector: 'app-add-edit-contact',
  templateUrl: './add-edit-contact.component.html',
  styleUrls: ['./add-edit-contact.component.scss']
})
export class AddEditContactComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Page Title
   */
  public pageTitle = '';

  /**
   * Subscription
   */
  public addContactSubscription: Subscription;

  /**
   * Form
   */
  public contactForm: FormGroup;

  public contactFormId = 'contact-form';

  public switchState = false;

  public contactType = EmployeeContact;

  /**
   * Constructor
   */
  constructor(
    protected injector: Injector,
    @Inject(MAT_DIALOG_DATA) public modal: EmployeeContactsModel,
    public dialogRef: MatDialogRef<AddEditContactComponent>,
    public employeeService: EmployeeContactsService) {
    super(injector);
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.modal.content ?
      this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_CONTACTS.EDIT_CONTACT_PAGE_TITLE') :
      this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_CONTACTS.ADD_CONTACT_PAGE_TITLE');
    super.onComponentReady();
    this.createForm();
  }

  /**
   * Create Form
   */
  public createForm(): void {
    this.contactForm = new FormGroup({
      number: new FormControl('', [Validators.required]),
      type: new FormControl(this.contactType.Personal, [Validators.required]),
      status: new FormControl(Status.Published, [Validators.required]),
    });
    if (this.modal.content) {
      this.contactForm.patchValue(this.modal.content);
      this.switchState = this.modal.content.status === Status.Published;
    }
  }

  /**
   * On Form Submit
   */
  public onFormSubmit(): void {
    this.addContactSubscription = this.employeeService.saveContact(this.contactForm.getRawValue(), this.modal.empId, this.modal.content)
      .subscribe(response => {
        this.dialogRef.close(response);
        NicoUtils.isNullOrUndefined(this.modal.content) ?
          this.showSuccessSnackBar(this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_CONTACTS.ADD_SUCCESS_MESSAGE_LABEL')) :
          this.showSuccessSnackBar(this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_CONTACTS.EDIT_SUCCESS_MESSAGE_LABEL'));
        }, error => {
        this.onFormSubmissionError(error, this.contactFormId);
      });
  }

  /**
   * On Destroy
   */
  public ngOnDestroy(): void {
    if (this.addContactSubscription != null) {
      this.addContactSubscription.unsubscribe();
    }
    this.pageTitleService.setTitle(this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_CONTACTS.PAGE_TITLE'));
  }

}
