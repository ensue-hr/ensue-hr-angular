import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {FlyMenuItemInterface} from '@system/components';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {AddressModel} from '@common/models/address.model';
import {PaginatedNicoCollection} from '@system/utilities';
import {AddressService} from '@common/services/address.service';
import {MatDialog} from '@angular/material/dialog';
import {AddEditAddressComponent} from './components/add-edit-address/add-edit-address.component';
import {AddressType} from '@common/enums/addressType.enum';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-employee-address',
  templateUrl: './employee-address.component.html',
})
export class EmployeeAddressComponent extends AbstractBaseComponent implements OnInit, OnDestroy{

  public addressSubscription: Subscription;
  public addressDeleteSubscription: Subscription;
  public pageTitle = '';
  public addressType = AddressType;
  public models: PaginatedNicoCollection<AddressModel>;
  public dialogRef: any;
  public employeeId: any;


  public flyMenuAddOns: FlyMenuItemInterface[] = [
    {
      name: 'edit',
      label: this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.EDIT_BUTTON_LABEL'),
      active: true,
      faIcon: 'far fa-pen'
    },
    {
      name: 'remove',
      label: this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.REMOVE_BUTTON_LABEL'),
      active: true,
      faIcon: 'far fa-trash'
    },
  ];

  constructor(injector: Injector, private addressService: AddressService,  public dialog: MatDialog) {
    super(injector);
    this.employeeId = this.activatedRoute.snapshot.parent.params;
  }


  public onFlyMenuAction(event: any, model: AddressModel): void {
    switch (event) {
      case 'remove':
        this.onRemove(model);
        break;

      case 'edit':
        this.onAddEditAddress(model);
        break;
    }
  }

  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_ADDRESS.PAGE_TITLE');
    super.onComponentReady();
    this.getModels();
  }

  public getFlyMenuAdOns(): FlyMenuItemInterface[] {
    return [...this.flyMenuAddOns];
  }

  public getModels(): void{
    this.addressSubscription = this.addressService.getAddress(this.employeeId.id).subscribe(model => {
      this.models = model;
    });
  }

  public onAddEditAddress(address?: AddressModel): void{
    this.openDrawerView(this.employeeId.id, address);
  }

  public openDrawerView(id: string, address?: AddressModel): void {
    const modalDialog =  this.dialog.open(AddEditAddressComponent,
      {panelClass: ['modal-drawer-view', 'modal-xs'], data: {model: address, employeeId: id}});
    modalDialog.afterClosed().subscribe(reload => {
      if (reload) {
        this.getModels();
      }
    });
  }

  public onRemove(model: AddressModel): void {
    this.dialogRef = this.openConfirmationDialogBox({
      title: this.translateService.instant('MOD_ADDRESS.DELETE_TITLE'),
      message: this.translateService.instant('MOD_ADDRESS.DELETE_MESSAGE')
    });
    this.dialogRef.afterClosed().subscribe(confirmation => {
      if (confirmation) {
        this.addressDeleteSubscription = this.addressService.deleteAddress(this.employeeId.id, model).subscribe(response => {
          this.dialogRef.close(response);
          this.getModels();
        });
      }
    });
  }

  public ngOnDestroy(): void{
    if (this.addressSubscription != null){
      this.addressSubscription.unsubscribe();
    }
    if (this.addressDeleteSubscription != null){
      this.addressDeleteSubscription.unsubscribe();
    }
  }
}
