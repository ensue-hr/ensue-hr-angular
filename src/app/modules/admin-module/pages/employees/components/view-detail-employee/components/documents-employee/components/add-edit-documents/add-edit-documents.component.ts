import {Component, Inject, Injector, OnDestroy, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../../../../../AbstractBaseComponent';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EmployeeDocumentsModel} from '@common/models';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Status} from '@common/enums/status.enums';
import {Subscription} from 'rxjs';
import {NicoFileUploadEvent} from '@system/modules/nico-file-upload';
import {EmployeeDocumentsService} from '@common/services/employee-documents.service';
import {FileUploadService} from '@common/services';
import {FileUploadModel} from '@common/models/file-upload.model';
import {NicoUtils} from '@system/utilities';

@Component({
  selector: 'app-add-edit-documents',
  templateUrl: './add-edit-documents.component.html',
  styleUrls: ['./add-edit-documents.component.scss']
})
export class AddEditDocumentsComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Page Title
   */
  public pageTitle = '';

  /**
   * Form
   */
  public documentForm: FormGroup;

  public documentFormId: 'document-form';

  /**
   * File Upload
   */
  public fileUploadEvent: NicoFileUploadEvent;

  public fileModel: FileUploadModel;

  public fileUploadFlag = true;

  public documentHint = '';

  public fileUploadErrorMessage = '';

  /**
   * Subscription
   */
  public documentSubscription: Subscription;

  public fileUploadSubscription: Subscription;

  /**
   * Constructor
   */
  constructor(
    protected injector: Injector,
    @Inject(MAT_DIALOG_DATA) public modal: EmployeeDocumentsModel,
    public dialogRef: MatDialogRef<AddEditDocumentsComponent>,
    public employeeService: EmployeeDocumentsService,
    public fileUploadService: FileUploadService) {
    super(injector);
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.modal.content ?
      this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_DOCUMENTS.EDIT_DOCUMENT_PAGE_TITLE') :
      this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_DOCUMENTS.ADD_DOCUMENT_PAGE_TITLE');
    super.onComponentReady();
    this.createForm();
  }

  /**
   * Create Form
   */
  public createForm(): void {
    this.documentForm = new FormGroup({
      title: new FormControl('', [Validators.required]),
      file: new FormControl(''),
      url: new FormControl('', [Validators.required]),
      type: new FormControl(''),
      thumbnail: new FormControl(''),
      status: new FormControl(Status.Published, [Validators.required]),
    });

    this.documentHint = this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_DOCUMENTS.SELECT_FILE_HINT');
    if (this.modal.content) {
      this.documentForm.patchValue(this.modal.content);
      this.switchState = this.modal.content.status === Status.Published;
      this.documentHint = this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_DOCUMENTS.FILE_EXISTS_SELECT_FILE_HINT');
    } else {
      this.documentForm.get('file').setValidators([
        Validators.required
      ]);
    }
  }

  /**
   * Save Document
   */
  public saveEmployeeDocument(): void {
    this.documentSubscription = this.employeeService.saveDocument(this.documentForm.getRawValue(), this.modal.empId, this.modal.content)
      .subscribe(response => {
        this.fileModel = null;
        this.dialogRef.close(response);
        NicoUtils.isNullOrUndefined(this.modal.content) ?
          this.showSuccessSnackBar(this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_DOCUMENTS.ADD_SUCCESS_MESSAGE_LABEL')) :
          this.showSuccessSnackBar(this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_DOCUMENTS.EDIT_SUCCESS_MESSAGE_LABEL'));
      }, error => {
        this.onFormSubmissionError(error, this.documentFormId);
      });
  }

  /**
   * Upload File
   */
  public uploadFile(): void {
    const fileData = new FormData();
    fileData.append('file', this.documentForm.controls.file.value);
    this.fileUploadSubscription = this.fileUploadService.post(fileData).subscribe(filePath => {
      // assign value to url, thumbnail and type
      this.fileUploadErrorMessage = '';
      this.fileModel = null;
      this.documentForm.get('url').setValue(filePath.path);
      this.documentForm.get('thumbnail').setValue(filePath.thumb_path);
      this.documentForm.get('type').setValue(filePath.type);
    }, error => {
      // error
      if (error.error) {
        this.fileUploadErrorMessage = error.error.file;
      } else {
        this.fileUploadFlag = false;
      }
      this.documentForm.get('url').setValue('');
      this.documentForm.get('thumbnail').setValue('');
    });
  }

  /**
   * On Destroy
   */
  public ngOnDestroy(): void {
    if (this.documentSubscription != null) {
      this.documentSubscription.unsubscribe();
    }
    if (this.fileUploadSubscription != null) {
      this.fileUploadSubscription.unsubscribe();
    }
    this.pageTitleService.setTitle(this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_DOCUMENTS.PAGE_TITLE'));
  }

}
