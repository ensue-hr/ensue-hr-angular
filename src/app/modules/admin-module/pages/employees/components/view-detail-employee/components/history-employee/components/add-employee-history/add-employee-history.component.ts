import {Component, Inject, Injector, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AbstractBaseComponent} from '../../../../../../../../../../AbstractBaseComponent';
import {EmployeeHistoryService, FileUploadService} from '@common/services';
import {Subscription} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {NicoFileUploadEvent} from '@system/modules/nico-file-upload';
import {FileUploadModel} from '@common/models/file-upload.model';

@Component({
  selector: 'app-add-employee-history',
  templateUrl: './add-employee-history.component.html',
  styleUrls: ['./add-employee-history.component.scss']
})
export class AddEmployeeHistoryComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Form
   */
  public historyForm: FormGroup;

  public historyFormId: 'history-form';

  /**
   * Page Title
   */
  public pageTitle = '';

  /**
   * Subscription
   */
  public historySubscription: Subscription;

  public fileUploadSubscription: Subscription;

  /**
   * File Upload
   */
  public fileUploadEvent: NicoFileUploadEvent;

  public fileModel: FileUploadModel;

  public fileUploadFlag = true;

  public documentHint = '';

  public fileUploadErrorMessage = '';

  /**
   * Constructor
   */
  constructor(protected injector: Injector,
              public employeeService: EmployeeHistoryService,
              @Inject(MAT_DIALOG_DATA) public modal,
              public dialogRef: MatDialogRef<AddEmployeeHistoryComponent>,
              public fileUploadService: FileUploadService) {
    super(injector);
  }

  public onComponentReady(): void {
    this.pageTitle = this.modal.activatedEmployee ?
      this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_HISTORIES.ACTIVATE_EMPLOYEE_LABEL') :
      this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_HISTORIES.DEACTIVATE_EMPLOYEE_LABEL');
    super.onComponentReady();
    this.createForm();
  }

  /**
   * Create Form
   */
  public createForm(): void {
    this.historyForm = new FormGroup({
      comment: new FormControl('', [Validators.required]),
      file: new FormControl(''),
      url: new FormControl(''),
      type: new FormControl(''),
      thumbnail: new FormControl(''),
    });

    this.documentHint = this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_DOCUMENTS.SELECT_FILE_HINT');
  }

  /**
   * Save Document for History
   */
  public saveHistoryDocument(): void {
    this.historySubscription = this.employeeService.toggleEmployeeStatus(this.historyForm.getRawValue(),
      this.modal.empId, this.modal.activatedEmployee)
      .subscribe(response => {
        this.fileModel = null;
        this.dialogRef.close(response);
        this.showSuccessSnackBar(this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_HISTORIES.ADD_SUCCESS_MESSAGE_LABEL'));
      }, error => {
        this.onFormSubmissionError(error, this.historyFormId);
        if (error.error) {
          this.fileUploadErrorMessage = error.error.file;
        } else {
          this.fileUploadFlag = false;
        }
      });
  }

  /**
   * Upload File
   */
  public uploadFile(): void {
    const fileData = new FormData();
    fileData.append('file', this.historyForm.controls.file.value);
    this.fileUploadSubscription = this.fileUploadService.post(fileData).subscribe(filePath => {
      // assign value to url, thumbnail and type
      this.fileUploadErrorMessage = '';
      this.fileModel = null;
      this.historyForm.get('url').setValue(filePath.path);
      this.historyForm.get('thumbnail').setValue(filePath.thumb_path);
      this.historyForm.get('type').setValue(filePath.type);
    }, error => {
      // error
      if (error.error) {
        this.fileUploadErrorMessage = error.error.file;
      } else {
        this.fileUploadFlag = false;
      }
      this.historyForm.get('url').setValue('');
      this.historyForm.get('thumbnail').setValue('');
    });
  }

  /**
   * On Destroy
   */
  public ngOnDestroy(): void {
    if (this.historySubscription != null) {
      this.historySubscription.unsubscribe();
    }
    if (this.fileUploadSubscription != null) {
      this.fileUploadSubscription.unsubscribe();
    }
    this.pageTitleService.setTitle(this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_DOCUMENTS.PAGE_TITLE'));
  }

}
