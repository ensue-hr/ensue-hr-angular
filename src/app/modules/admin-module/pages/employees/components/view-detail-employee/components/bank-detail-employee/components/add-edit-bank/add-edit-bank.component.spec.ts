import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';

import { AddEditBankComponent } from './add-edit-bank.component';
import {MockEmployeesService} from '@common/testing-resources/mock-services/MockEmployeesService';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {ActivatedRoute} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MockMatDialog} from '@common/testing-resources/mock-services';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {EmployeeContactsModel} from '@common/models';
import {Status} from '@common/enums/status.enums';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {EmployeeBanksService} from '@common/services';

describe('AddEditBankComponent', () => {
  let component: AddEditBankComponent;
  let fixture: ComponentFixture<AddEditBankComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditBankComponent ],
      providers: [
        {provide: EmployeeBanksService, useClass: MockEmployeesService},
        ...MOCK_PROVIDERS,
        {
          provide: ActivatedRoute,
          useValue:
            {
              snapshot: {
                queryParams: {
                  page: 1, sort_order: 'asc', per_page: 16
                },
                parent: {
                  params: {
                    id: 1
                  }
                }
              }
            }
        },
        { provide: MatDialogRef, useClass: MockMatDialog },
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ],
      imports: [
        TranslateModule,
        RouterTestingModule,
        ReactiveFormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set add page title in case of add', fakeAsync (async () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_BANKS.ADD_BANK_PAGE_TITLE');
    flush();
  }));

  it ('should set edit page title in case of edit', () => {
    component.modal = {content: {number: '1111111111', type: 2, status: 2}, empId: 1} as unknown as EmployeeContactsModel;
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_BANKS.EDIT_BANK_PAGE_TITLE');
  });

  it('should initialize form', () => {
    spyOn(component, 'createForm');
    component.ngOnInit();
    expect(component.createForm).toHaveBeenCalled();
  });

  it('should check initial form values in case of add', () => {
    const contactForm = component.bankForm;
    const contactFormValues = {
      name: '',
      branch: '',
      account_number: '',
      status: Status.Published
    };
    expect(contactForm.value).toEqual(contactFormValues);
  });

  it('should set form values in case of edit', () => {
    component.modal = {
      content: {
        name: 'Himalayan Bank Ltd.',
        branch: 'Battisputali',
        account_number: '001002003',
        status: Status.Published
      },
      empId: 1
    } as unknown as EmployeeContactsModel;
    component.onComponentReady();
    const contactForm = component.bankForm;
    const contactFormValues = {
      name: 'Himalayan Bank Ltd.',
      branch: 'Battisputali',
      account_number: '001002003',
      status: Status.Published
    };
    expect(contactForm.value).toEqual(contactFormValues);
  });

  it('should call showSuccessSnackBar in case of success', fakeAsync(async () => {
    spyOn(component, 'showSuccessSnackBar');
    component.onComponentReady();
    component.bankForm.setValue({
      name: 'Himalayan Bank Ltd.',
      branch: 'Battisputali',
      account_number: '001002003',
      status: Status.Published
    });
    await component.onFormSubmit();
    await fixture.whenStable();
    expect(component.showSuccessSnackBar).toHaveBeenCalled();
    flush();
  }));

  it('should disable submit button until conditions are met', () => {
    component.createForm();
    const button = fixture.debugElement.nativeElement.querySelectorAll('button')[1].getAttribute('disabled');
    expect(button).toMatch('');
  });

  it('should activate button after conditions are met', () => {
    component.onComponentReady();
    component.bankForm.patchValue({number: '1111111111', type: 1, status: 1});
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelectorAll('button')[1].getAttribute('disabled');
    expect(button).toBeDefined();
  });
});
