import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {EmployeeModel} from '@common/models/employee.model';
import {Subscription} from 'rxjs';
import {MaritalStatus} from '@common/enums/maritalStatus.enums';
import {EmployeesService} from '@common/services/employees.service';
import {MatDialog} from '@angular/material/dialog';
import {AddEditEmployeeComponent} from '../../../add-edit-employee/add-edit-employee.component';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {Gender} from '@common/enums/gender.enums';

@Component({
  selector: 'app-basic-info-employee',
  templateUrl: './basic-info-employee.component.html',
  styleUrls: ['./basic-info-employee.component.scss']
})
export class BasicInfoEmployeeComponent extends AbstractBaseComponent implements OnInit, OnDestroy {
  public model: EmployeeModel;
  public viewDetailEmployeeSubscription: Subscription;
  public employeeSubscription: Subscription;
  public isTenureInformation: boolean;
  public isPersonalInformation: boolean;
  public maritalStatus = MaritalStatus;
  public genderStatus = Gender;
  public pageTitle = '';

  constructor(injector: Injector, private employeeService: EmployeesService, public dialog: MatDialog) {
    super(injector);
  }

  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.PAGE_TITLE');
    super.onComponentReady();
    this.getEmployeeModel();
    this.isPersonalInformation = true;
    this.isTenureInformation = true;
  }

  public getModel(): void {
    this.viewDetailEmployeeSubscription = this.employeeService.getDetail(this.activatedRoute.snapshot.parent.params).subscribe(model => {
      this.model = model;
      this.employeeService.setUserDetailChange(this.model);
    });
  }

  public getEmployeeModel(): void {
    this.employeeSubscription = this.activatedRoute.parent?.data.subscribe((model: any) => {
      this.model = model.employeeResolver;
    });
  }

  public onEdit(): void {
    const modalDialog = this.dialog.open(AddEditEmployeeComponent, {
      panelClass: ['modal-drawer-view', 'modal-xs'],
      data: this.model
    });
    modalDialog.afterClosed().subscribe(reload => {
      if (reload) {
        this.getModel();
        this.employeeService.setUserDetailChange(this.model);
      }
    });
  }

  public getGenderLabel(gender: number): any {
    if (gender === this.genderStatus.Male) {
      return 'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.MALE_LABEL';
    }
    if (gender === this.genderStatus.Others) {
      return 'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.OTHER_LABEL';
    }
    if (gender === this.genderStatus.Female) {
      return 'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.FEMALE_LABEL';
    }
  }

  public ngOnDestroy(): void {
    if (this.viewDetailEmployeeSubscription != null) {
      this.viewDetailEmployeeSubscription.unsubscribe();
    }
    if (this.employeeSubscription != null) {
      this.employeeSubscription.unsubscribe();
    }
  }

}
