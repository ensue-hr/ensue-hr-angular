import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import { EmployeeAddressComponent } from './employee-address.component';
import {NicoHttpClient} from '@system/http-client';
import {
  MockMatDialog,
  MockMatSnackBar, MockMissingTranslationHandler,
  MockNicoHttpClient,
  MockNicoPageTitle,
  MockNicoSessionService,
  MockNicoStorageService
} from '@common/testing-resources/mock-services';
import {FormBuilder, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {MockFormGroup} from '@common/testing-resources/mock-services/MockFormGroup';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService, NicoStorageService} from '@system/services';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ActivatedRoute} from '@angular/router';
import {
  DEFAULT_LANGUAGE,
  MissingTranslationHandler,
  TranslateCompiler,
  TranslateFakeCompiler,
  TranslateFakeLoader,
  TranslateLoader, TranslateModule, TranslateParser,
  TranslateService,
  TranslateStore, USE_DEFAULT_LANG, USE_EXTEND, USE_STORE
} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {CommonModule} from '@angular/common';
import {AddressModel} from '@common/models/address.model';
import {PaginatedNicoCollection} from '@system/utilities';
import {AddressService} from '@common/services/address.service';
import {MockAddressService} from '@common/testing-resources/mock-services/MockAddressService';
import {By} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {of} from 'rxjs';
import {BreadcrumbService} from 'xng-breadcrumb';

describe('EmployeeAddressComponent', () => {
  let component: EmployeeAddressComponent;
  let fixture: ComponentFixture<EmployeeAddressComponent>;
  let service: AddressService;
  const mockData = {
    id : 1,
    country: 'Nepal',
    state: 3,
    city: 'ktm',
    street: 'bijeshwori',
    zip_code: 44600,
    type: 2,
    created_at: '',
    updated_at: ''
  } as unknown as AddressModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeAddressComponent ],
      providers: [
        {provide: AddressService, useClass: MockAddressService},
        {provide: NicoHttpClient, useClass: MockNicoHttpClient},
        {provide: FormGroup, useClass: MockFormGroup},
        {provide: NicoStorageService, useClass: MockNicoStorageService},
        {provide: NicoPageTitle, useClass: MockNicoPageTitle},
        {provide: NicoSessionService, useClass: MockNicoSessionService},
        {provide: MatSnackBar, useClass: MockMatSnackBar},
        {provide: MatDialog, useClass: MockMatDialog},
        {provide: ActivatedRoute, useValue:  { snapshot: {parent: {params: {id: '1'}}}}},
        FormBuilder,
        AjaxSpinnerService,
        TranslateService,
        TranslateStore,
        {provide: TranslateLoader, useClass: TranslateFakeLoader},
        {provide: TranslateCompiler, useClass: TranslateFakeCompiler},
        TranslateParser,
        {provide: MissingTranslationHandler, useClass: MockMissingTranslationHandler},
        {provide: USE_DEFAULT_LANG, useValue: undefined},
        {provide: USE_STORE, useValue: undefined},
        {provide: USE_EXTEND, useValue: undefined},
        {provide: DEFAULT_LANGUAGE, useValue: undefined},
        {provide: BreadcrumbService, useValue: undefined}
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        ReactiveFormsModule,
        CommonModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]

    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeAddressComponent);
    service = TestBed.inject(AddressService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call getModel on init', () => {
      spyOn(component, 'getModels');
      component.ngOnInit();
      expect(component.getModels).toHaveBeenCalled();
  });

  it('should change page title', () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_ADDRESS.PAGE_TITLE');
  });

  it('should disable add button if addresses are filled', () => {
    component.models = new PaginatedNicoCollection<AddressModel>();
    component.models.push(mockData);
    component.models.push(mockData);
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('.new-btn')).nativeElement.disabled).toBe(true);
  });

  it('should open modal on click new btn', () => {
    spyOn(component, 'onAddEditAddress');
    component.models = new PaginatedNicoCollection<AddressModel>();
    component.models.push(mockData);
    component.models.push(mockData);
    fixture.detectChanges();
    const newBtn: DebugElement =
      fixture.debugElement.query(By.css('.new-btn'));
    newBtn.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.onAddEditAddress).toHaveBeenCalledTimes(1);
  });

  it('should display no data label if there is no data', () => {
    component.models = new PaginatedNicoCollection<AddressModel>();
    fixture.detectChanges();
    const appEmptyState: DebugElement =
      fixture.debugElement.query(By.css('app-empty-state'));
    expect(appEmptyState).toBeTruthy();
  });

  it('should reload in case of edit/add', fakeAsync (() => {
    spyOn(component.dialog, 'open')
      .and
      .returnValue({afterClosed: () => of(true)} as unknown as MatDialogRef<any>);
    spyOn(component, 'getModels');
    fixture.detectChanges();
    component.openDrawerView('1', mockData);
    fixture.detectChanges();
    fixture.whenStable();
    expect(component.getModels).toHaveBeenCalled();
  }));
});
