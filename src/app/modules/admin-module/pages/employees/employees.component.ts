import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {EmployeesService} from '@common/services/employees.service';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';
import {Subscription} from 'rxjs';
import {EmployeeModel} from '@common/models/employee.model';
import {PaginatedNicoCollection} from '@system/utilities';
import {NicoStorageService} from '@system/services';
import {AddEditEmployeeComponent} from './components/add-edit-employee/add-edit-employee.component';
import {MatDialog} from '@angular/material/dialog';
import {EmployeeStatus} from '@common/enums/status.enums';
import {AppService} from '@common/services';


@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
})
export class EmployeesComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  private employeesSubscription: Subscription;
  public models: PaginatedNicoCollection<EmployeeModel>;
  public model: EmployeeModel;
  public isTableView: boolean;
  public keyword: any;
  public pageTitle = '';
  public status = EmployeeStatus;

  /**
   * Filter dropdown values
   */
  public statusDropdownValues = [
    {
      status: this.translateService.instant('MOD_EMPLOYEES.STATUS_DROPDOWN_VALUES.ALL'),
      value: 'all'
    },
    {
      status: this.translateService.instant('MOD_EMPLOYEES.STATUS_DROPDOWN_VALUES.ACTIVE'),
      value: EmployeeStatus.Active
    },
    {
      status: this.translateService.instant('MOD_EMPLOYEES.STATUS_DROPDOWN_VALUES.INACTIVE'),
      value: EmployeeStatus.Inactive
    }
  ];

  public sortByDropdownValues = [

    {
      label: this.translateService.instant('MOD_EMPLOYEES.SORT_BY_DROPDOWN_VALUES.NAME'),
      value: 'name'
    },
    {
      label: this.translateService.instant('MOD_EMPLOYEES.SORT_BY_DROPDOWN_VALUES.POSITION'),
      value: 'position'
    },
    {
      label: this.translateService.instant('MOD_EMPLOYEES.SORT_BY_DROPDOWN_VALUES.JOINED_AT'),
      value: 'joined_at'
    },
    {
      label: this.translateService.instant('MOD_EMPLOYEES.SORT_BY_DROPDOWN_VALUES.STATUS'),
      value: 'status'
    },
    {
      label: this.translateService.instant('MOD_EMPLOYEES.SORT_BY_DROPDOWN_VALUES.CREATED_AT'),
      value: 'created_at'
    },
  ];

  constructor(
    injector: Injector,
    private employeeService: EmployeesService,
    public dialog: MatDialog,
    protected storageService: NicoStorageService,
    public appService: AppService) {
    super(injector);
  }

  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_EMPLOYEES.PAGE_TITLE');
    super.onComponentReady();
    this.getModels();
    this.storageService.getItem('view') ? this.isTableView = this.storageService.getItem('view') === 'true' : this.isTableView = false;
  }

  public onAddEditEmployee(): void {
    const modalDialog = this.dialog.open(AddEditEmployeeComponent, {panelClass: ['modal-drawer-view', 'modal-xs'], disableClose: true});
    modalDialog.afterClosed().subscribe(reload => {
      if (reload) {
        this.getModels();
      }
    });
  }

  public getModels(): void {
    this.employeesSubscription = this.employeeService.get(this.getFilterHttpQueryParams()).subscribe((models) => {
      this.models = models;
    });
  }

  /**
   * View Toggle change
   */
  public onViewToggle($event): void {
    this.isTableView = $event;
  }

  public ngOnDestroy(): void {
    if (!this.employeesSubscription) {
      this.employeesSubscription.unsubscribe();
    }
  }

  public onSelectTableRow(model: EmployeeModel): void {
    this.router.navigate([model.id], {relativeTo: this.activatedRoute}).then(resolve => {
    });
  }
}
