import { NgModule} from '@angular/core';
import {HrCommonModule} from '@common/hr-common.module';
import { NicoComponentsModule } from '@system/components';
import { RouterModule } from '@angular/router';
import {EmployeesComponent} from './employees.component';
import {EMPLOYEES_ROUTES} from './employees.routes';
import {EmployeesService} from '@common/services/employees.service';
import { AddEditEmployeeComponent } from './components/add-edit-employee/add-edit-employee.component';
import { ViewDetailEmployeeComponent } from './components/view-detail-employee/view-detail-employee.component';
import {TranslateModule} from '@ngx-translate/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import { BasicInfoEmployeeComponent } from './components/view-detail-employee/components/basic-info-employee/basic-info-employee.component';
import {SocialSecurityComponent} from './components/view-detail-employee/components/social-security/social-security.component';
import {AddEditSocialSecurityComponent} from './components/view-detail-employee/components/social-security/components/add-edit-social-security/add-edit-social-security.component';
import {EmployeeAddressComponent} from './components/view-detail-employee/components/employee-address/employee-address.component';
import {AddEditAddressComponent} from './components/view-detail-employee/components/employee-address/components/add-edit-address/add-edit-address.component';
import { ContactEmployeeComponent } from './components/view-detail-employee/components/contact-employee/contact-employee.component';
import { AddEditContactComponent } from './components/view-detail-employee/components/contact-employee/components/add-edit-contact/add-edit-contact.component';
import { BankDetailEmployeeComponent } from './components/view-detail-employee/components/bank-detail-employee/bank-detail-employee.component';
import { AddEditBankComponent } from './components/view-detail-employee/components/bank-detail-employee/components/add-edit-bank/add-edit-bank.component';
import { DocumentsEmployeeComponent } from './components/view-detail-employee/components/documents-employee/documents-employee.component';
import { AddEditDocumentsComponent } from './components/view-detail-employee/components/documents-employee/components/add-edit-documents/add-edit-documents.component';
import {NicoFileUploadModule} from '@system/modules/nico-file-upload';
import {AllocatedLeaveEmployeeComponent} from './components/view-detail-employee/components/allocated-leave-employee/allocated-leave-employee.component';
import {AddEditAllocatedLeaveComponent} from './components/view-detail-employee/components/allocated-leave-employee/components/add-edit-allocated-leave/add-edit-allocated-leave.component';
import { HistoryEmployeeComponent } from './components/view-detail-employee/components/history-employee/history-employee.component';
import {CommonModule} from '@angular/common';
import { AddEmployeeHistoryComponent } from './components/view-detail-employee/components/history-employee/components/add-employee-history/add-employee-history.component';

@NgModule({
  declarations: [
    EmployeesComponent,
    AddEditEmployeeComponent,
    ViewDetailEmployeeComponent,
    BasicInfoEmployeeComponent,
    SocialSecurityComponent,
    AddEditSocialSecurityComponent,
    EmployeeAddressComponent,
    AddEditAddressComponent,
    ContactEmployeeComponent,
    AddEditContactComponent,
    BankDetailEmployeeComponent,
    AddEditBankComponent,
    DocumentsEmployeeComponent,
    AddEditDocumentsComponent,
    AllocatedLeaveEmployeeComponent,
    AddEditAllocatedLeaveComponent,
    HistoryEmployeeComponent,
    AddEmployeeHistoryComponent
  ],
    imports: [
        HrCommonModule,
        NicoComponentsModule,
        RouterModule.forChild(EMPLOYEES_ROUTES),
        TranslateModule.forChild({
            extend: true
        }),
        MatInputModule,
        NicoFileUploadModule,
        CommonModule,
    ],
  providers: [
    EmployeesService,
    {provide: MatDialogRef , useValue: {} },
    {provide: MAT_DIALOG_DATA, useValue: {} }
  ],
})

export class EmployeesModule { }
