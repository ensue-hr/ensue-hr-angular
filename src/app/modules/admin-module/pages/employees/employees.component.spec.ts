import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import { EmployeesComponent } from './employees.component';
import {NicoHttpClient} from '@system/http-client';
import {
  MockMatDialog,
  MockMatSnackBar, MockMissingTranslationHandler,
  MockNicoHttpClient,
  MockNicoPageTitle,
  MockNicoSessionService,
  MockNicoStorageService
} from '@common/testing-resources/mock-services';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MockFormGroup} from '@common/testing-resources/mock-services/MockFormGroup';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService, NicoStorageService} from '@system/services';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {
  DEFAULT_LANGUAGE,
  MissingTranslationHandler,
  TranslateCompiler,
  TranslateFakeCompiler,
  TranslateFakeLoader,
  TranslateLoader, TranslateModule, TranslateParser,
  TranslateService,
  TranslateStore, USE_DEFAULT_LANG, USE_EXTEND, USE_STORE
} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {EmployeeModel} from '@common/models/employee.model';
import {EmployeesService} from '@common/services/employees.service';
import {MockEmployeesService} from '@common/testing-resources/mock-services/MockEmployeesService';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {of} from 'rxjs';
import {BreadcrumbService} from 'xng-breadcrumb';


describe('EmployeesComponent', () => {
  let component: EmployeesComponent;
  let fixture: ComponentFixture<EmployeesComponent>;
  let pageTitleService: NicoPageTitle;
  const mockData = {
    avatar: null,
    code: 'EN200701',
    created_at: '2021-07-20T08:19:36.000000Z',
    department: 'Human Resources',
    department_id: 1,
    email: 'manish.shakya@ensue.us',
    id: 1,
    joined_at: '2007-01-01',
    name: 'Manish Ratna Shakya',
    position: 'Junior Frontend Developer',
    status: 1
  } as unknown as EmployeeModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeesComponent ],
      providers: [
        {provide: EmployeesService, useClass: MockEmployeesService},
        {provide: NicoHttpClient, useClass: MockNicoHttpClient},
        {provide: FormGroup, useClass: MockFormGroup},
        {provide: NicoStorageService, useClass: MockNicoStorageService},
        {provide: NicoPageTitle, useClass: MockNicoPageTitle},
        {provide: NicoSessionService, useClass: MockNicoSessionService},
        {provide: MatSnackBar, useClass: MockMatSnackBar},
        {provide: MatDialog, useClass: MockMatDialog},
        {provide: Router, useValue: jasmine.createSpyObj('Router', ['navigate'])},
        {provide: ActivatedRoute, useValue:  { snapshot: {queryParams: {page: 1}}}},
        FormBuilder,
        AjaxSpinnerService,
        TranslateService,
        TranslateStore,
        {provide: TranslateLoader, useClass: TranslateFakeLoader},
        {provide: TranslateCompiler, useClass: TranslateFakeCompiler},
        TranslateParser,
        {provide: MissingTranslationHandler, useClass: MockMissingTranslationHandler},
        {provide: USE_DEFAULT_LANG, useValue: undefined},
        {provide: USE_STORE, useValue: undefined},
        {provide: USE_EXTEND, useValue: undefined},
        {provide: DEFAULT_LANGUAGE, useValue: undefined},
        {provide: BreadcrumbService, useValue: undefined}
      ],
      imports: [
        RouterTestingModule,
        TranslateModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesComponent);
    component = fixture.componentInstance;
    pageTitleService = TestBed.inject(NicoPageTitle);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get models on init', () => {
    spyOn(component,  'getModels');
    component.ngOnInit();
    expect(component.getModels).toHaveBeenCalled();
  });

  it('should change the view on toggle change view', () => {
    const view = component.isTableView;
    component.onViewToggle(null);
    expect(component.isTableView).not.toEqual(view);
  });

  it('should redirect to view detail page after click on table row or view detail button ', () => {
     spyOn(component, 'onSelectTableRow');
     component.onSelectTableRow(mockData);
     expect(component.onSelectTableRow).toHaveBeenCalled();
  });

  it('should change page title on init ', () => {
    component.ngOnInit();
    expect(component.pageTitle).toEqual('MOD_EMPLOYEES.PAGE_TITLE');
  });
  it('should change html title on init ', () => {
    const title: DebugElement =
      fixture.debugElement.query(By.css('.page-header-title'));
    expect(title.nativeElement.outerText).toEqual('MOD_EMPLOYEES.PAGE_TITLE');
    });


  it('should open Add Modal on click New btn', () => {
    spyOn(component, 'onAddEditEmployee');
    const submitButton: DebugElement =
      fixture.debugElement.query(By.css('.new-btn'));
    fixture.detectChanges();
    submitButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.onAddEditEmployee).toHaveBeenCalledTimes(1);
  });

  it('should filter data on search', () => {
    spyOn(component, 'onSearchTextChange');
    const searchNav = fixture.debugElement.query(By.css('app-search-nav'));
    searchNav.triggerEventHandler('searchChange', 'bachelors');
    fixture.detectChanges();
    expect(component.onSearchTextChange).toHaveBeenCalled();
  });

  it('should filter data on on Sort By Change', () => {
    spyOn(component, 'onSortByChange');
    const searchNav = fixture.debugElement.query(By.css('app-search-nav'));
    searchNav.triggerEventHandler('sortByChange', 'status');
    fixture.detectChanges();
    expect(component.onSortByChange).toHaveBeenCalled();

  });
  it('should filter data on Sort Order Change ', () => {
    spyOn(component, 'onSortOrderChange');
    const searchNav = fixture.debugElement.query(By.css('app-search-nav'));
    searchNav.triggerEventHandler('sortOrderChange', 'asc');
    fixture.detectChanges();
    expect(component.onSortOrderChange).toHaveBeenCalled();
  });

  it('should reload in case of edit/add', fakeAsync (() => {
    spyOn(component.dialog, 'open')
      .and
      .returnValue({afterClosed: () => of(true)} as unknown as MatDialogRef<any>);
    spyOn(component, 'getModels');
    fixture.detectChanges();
    component.onAddEditEmployee();
    fixture.detectChanges();
    fixture.whenStable();
    expect(component.getModels).toHaveBeenCalled();
  }));
});
