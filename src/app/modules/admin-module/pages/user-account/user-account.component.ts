import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';
import {Subscription} from 'rxjs';
import {UserModel} from '@common/models';
import {ChangePasswordModalComponent} from './components/change-password-modal/change-password-modal.component';
import {UserService} from '@common/services';

@Component({
  selector: 'app-view-details',
  templateUrl: './user-account.component.html',
})
export class UserAccountComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  public userInfoSubscription: Subscription;

  public userProfile: UserModel;

  /**
   * Page title
   */
  public pageTitle = '';

  /**
   * Constructor
   */
  constructor(protected injector: Injector, private userInfo: UserService) {
    super(injector);
  }

  public componentsList = [
    {
      name: this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.NAVIGATION_TAB_LABELS.BASIC_INFORMATION_LABEL'),
      fragment: 'basic-information',
      icon: 'far fa-info-circle'
    },
    {
      name: this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.NAVIGATION_TAB_LABELS.CONTACTS_LABEL'),
      fragment: 'user-contacts',
      icon: 'far fa-phone-alt'
    },
    {
      name: this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.NAVIGATION_TAB_LABELS.BANKS_LABEL'),
      fragment: 'user-banks',
      icon: 'far fa-piggy-bank'
    },
    {
      name: this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.NAVIGATION_TAB_LABELS.ADDRESS_LABEL'),
      fragment: 'user-addresses',
      icon: 'far fa-home'
    },
    {
      name: this.translateService.instant('MOD_SOCIAL_SECURITY.HTML_TITLE'),
      fragment: 'user-social-security',
      icon: 'far fa-user-lock'
    },
    {
      name: this.translateService.instant('MOD_ALLOCATED_LEAVE.PAGE_TITLE'),
      fragment: 'user-allocated-leaves',
      icon: 'far fa-calendar-minus'
    },
  ];

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_ACCOUNT.HTML_TITLE');
    super.onComponentReady();
    this.getUserInfo();
  }

  /**
   * Get the user's info
   */
  public getUserInfo(): void {
    this.userInfoSubscription = this.userInfo.get().subscribe(userInfo => {
      this.userProfile = userInfo;
      });
  }

  /**
   * Open the change password modal
   */
  public openChangePasswordModal(): void {
    this.modalService.open(ChangePasswordModalComponent);
  }

  /**
   * On destroy
   */
  public ngOnDestroy(): void {
    if (this.userInfoSubscription != null) {
      this.userInfoSubscription.unsubscribe();
    }
  }

}
