import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService, NicoStorageService} from '@system/services';
import {
  MockAjaxSpinnerService, MockMatDialog, MockMatSnackBar, MockMissingTranslationHandler,
  MockNicoHttpClient,
  MockNicoPageTitle,
  MockNicoSessionService, MockNicoStorageService, MockUserInfoService
} from '@common/testing-resources/mock-services';
import {NicoHttpClient} from '@system/http-client';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {AppService, UserService} from '@common/services';
import {RouterTestingModule} from '@angular/router/testing';
import {UserAccountComponent} from './user-account.component';
import {
  DEFAULT_LANGUAGE,
  MissingTranslationHandler,
  TranslateCompiler, TranslateFakeCompiler,
  TranslateFakeLoader,
  TranslateLoader, TranslateModule, TranslateParser,
  TranslateService,
  TranslateStore, USE_DEFAULT_LANG, USE_EXTEND, USE_STORE
} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('UserAccountComponent', () => {
  let component: UserAccountComponent;
  let fixture: ComponentFixture<UserAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserAccountComponent ],
      providers: [
        {provide: NicoPageTitle, useClass: MockNicoPageTitle},
        {provide: NicoHttpClient, useClass: MockNicoHttpClient},
        {provide: AjaxSpinnerService, useClass: MockAjaxSpinnerService},
        {provide: NicoSessionService, useClass: MockNicoSessionService},
        {provide: MatSnackBar, useClass: MockMatSnackBar},
        {provide: MatDialog, useClass: MockMatDialog},
        {provide: NicoStorageService, useClass: MockNicoStorageService},
        AppService,
        {provide: UserService, useClass: MockUserInfoService},
        TranslateService,
        TranslateStore,
        {provide: TranslateLoader, useClass: TranslateFakeLoader},
        {provide: TranslateCompiler, useClass: TranslateFakeCompiler},
        TranslateParser,
        {provide: MissingTranslationHandler, useClass: MockMissingTranslationHandler},
        {provide: USE_DEFAULT_LANG, useValue: undefined},
        {provide: USE_STORE, useValue: undefined},
        {provide: USE_EXTEND, useValue: undefined},
        {provide: DEFAULT_LANGUAGE, useValue: undefined},
      ],
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create view details component', () => {
    expect(component).toBeTruthy();
  });

  it('should get user info on init', () => {
    spyOn(component, 'getUserInfo');
    component.ngOnInit();
    expect(component.getUserInfo).toHaveBeenCalled();
  });

  it('should set userProfile', fakeAsync(async () => {
    component.getUserInfo();
    expect(component.userProfile).not.toBeNull();
  }));
});
