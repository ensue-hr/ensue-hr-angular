import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {NicoCollection} from '@system/utilities';
import {Subscription} from 'rxjs';
import {Status} from '@common/enums/status.enums';
import {UserModel} from '@common/models';
import {UserDetailService} from '@common/services';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';
import {AllocatedLeaveModel} from '@common/models/allocated-leaves.model';

@Component({
  selector: 'app-user-allocated-leaves',
  templateUrl: './user-allocated-leaves.component.html',
  styleUrls: ['./user-allocated-leaves.component.scss']
})
export class UserAllocatedLeavesComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  public model: NicoCollection<AllocatedLeaveModel>;

  /**
   * Subscriptions
   */
  public userInfoSubscription: Subscription;

  /**
   * Enum
   */
  public status = Status;

  /**
   * Page title
   */
  public pageTitle = '';

  /**
   * User Info
   */
  public user: UserModel;

  /**
   * Constructor
   */
  constructor(
    protected injector: Injector,
    private userDetailService: UserDetailService
  ) {
    super(injector);
  }

  /**
   * Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_ALLOCATED_LEAVE.PAGE_TITLE');
    super.onComponentReady();
    this.getModels();
  }

  /**
   * Get all contacts
   */
  public getModels(): void {
    this.userInfoSubscription = this.userDetailService.get().subscribe(userInfo => {
      this.user = userInfo;
      this.model = this.user.allocated_leaves;
    });
  }

  /**
   * On Destroy
   */
  public ngOnDestroy(): void {
    if (this.userInfoSubscription != null) {
      this.userInfoSubscription.unsubscribe();
    }
  }

}
