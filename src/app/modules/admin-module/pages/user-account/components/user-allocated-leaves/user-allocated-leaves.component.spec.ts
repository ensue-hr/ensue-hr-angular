import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAllocatedLeavesComponent } from './user-allocated-leaves.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';

describe('UserAllocatedLeavesComponent', () => {
  let component: UserAllocatedLeavesComponent;
  let fixture: ComponentFixture<UserAllocatedLeavesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserAllocatedLeavesComponent ],
      providers: [
        ...MOCK_PROVIDERS
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAllocatedLeavesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
