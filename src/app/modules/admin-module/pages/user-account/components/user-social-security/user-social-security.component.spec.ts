import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSocialSecurityComponent } from './user-social-security.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {UserDetailService} from '@common/services';
import {MockUserDetailInfoService} from '@common/testing-resources/mock-services';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {PaginatedNicoCollection} from '@system/utilities';
import {SocialSecurityModel} from '@common/models/social-security.model';
import {By} from '@angular/platform-browser';

describe('UserSocialSecurityComponent', () => {
  let component: UserSocialSecurityComponent;
  let fixture: ComponentFixture<UserSocialSecurityComponent>;
  const mockData = {
    created_at: '2021-08-18T18:32:21.000000Z',
    end_date: null,
    id: 41,
    number: '9860106659',
    start_date: '2006-06-28',
    status: 2,
    title: 'Bachelors of Information and Management',
    updated_at: '2021-08-18T18:35:06.000000Z',
  } as unknown as SocialSecurityModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserSocialSecurityComponent ],
      providers: [
        ...MOCK_PROVIDERS,
        {provide: UserDetailService, useClass: MockUserDetailInfoService}
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        ReactiveFormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSocialSecurityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get model on init', () => {
    spyOn(component, 'getModels');
    component.ngOnInit();
    expect(component.getModels).toHaveBeenCalled();
  });

  it('should change html title to social security', () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_SOCIAL_SECURITY.HTML_TITLE');
  });

  it('should change page title to social security', () => {
    component.model = new PaginatedNicoCollection<SocialSecurityModel>();
    component.model.push(mockData);
    fixture.detectChanges();
    const htmlTitle = fixture.debugElement.query(By.css('.card-title'));
    expect(htmlTitle.nativeElement.outerText).toBe('MOD_SOCIAL_SECURITY.HTML_TITLE');
  });
});
