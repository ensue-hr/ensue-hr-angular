import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';
import {NicoCollection} from '@system/utilities';
import {UserModel} from '@common/models';
import {Subscription} from 'rxjs';
import {Status} from '@common/enums/status.enums';
import {UserDetailService} from '@common/services';
import {SocialSecurityModel} from '@common/models/social-security.model';

@Component({
  selector: 'app-user-social-security',
  templateUrl: './user-social-security.component.html',
})
export class UserSocialSecurityComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Models
   */
  public model: NicoCollection<SocialSecurityModel>;

  /**
   * Subscriptions
   */
  public userInfoSubscription: Subscription;

  /**
   * Enum
   */
  public status = Status;

  /**
   * Page title
   */
  public pageTitle = '';

  /**
   * User Info
   */
  public user: UserModel;

  /**
   * Constructor
   */
  constructor(
    protected injector: Injector,
    private userDetailService: UserDetailService
  ) {
    super(injector);
  }

  /**
   * Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_SOCIAL_SECURITY.HTML_TITLE');
    super.onComponentReady();
    this.getModels();
  }

  /**
   * Get all contacts
   */
  public getModels(): void {
    this.userInfoSubscription = this.userDetailService.get().subscribe(userInfo => {
      this.user = userInfo;
      this.model = this.user.social_securities;
    });
  }

  /**
   * On Destroy
   */
  public ngOnDestroy(): void {
    if (this.userInfoSubscription != null) {
      this.userInfoSubscription.unsubscribe();
    }
  }

}
