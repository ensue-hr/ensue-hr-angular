import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';

import { UserContactsComponent } from './user-contacts.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';
import {UserDetailService} from '@common/services/user-detail.service';
import {MockUserDetailInfoService, MockUserInfoService} from '@common/testing-resources/mock-services';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('UserContactsComponent', () => {
  let component: UserContactsComponent;
  let fixture: ComponentFixture<UserContactsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserContactsComponent ],
      providers: [
        ...MOCK_PROVIDERS,
        {provide: UserDetailService, useClass: MockUserDetailInfoService}
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        ReactiveFormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserContactsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create user contacts component', () => {
    expect(component).toBeTruthy();
  });

  it('should set page title', fakeAsync (async () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_CONTACTS.PAGE_TITLE');
    flush();
  }));

  it('should retrieve data', fakeAsync (async () => {
    spyOn(component, 'getModels').and.callThrough();
    await component.onComponentReady();
    await fixture.whenStable();
    expect(component.getModels).toHaveBeenCalled();
  }));

  it('should show error in case of an error in retrieving data', fakeAsync (async () => {
    await component.onComponentReady();
    await fixture.whenStable();
    expect(component.contactModel).not.toBeDefined();
  }));
});
