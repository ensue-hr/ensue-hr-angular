import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {NicoCollection} from '@system/utilities';
import {EmployeeContactsModel, UserModel} from '@common/models';
import {Subscription} from 'rxjs';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';
import {UserDetailService} from '@common/services/user-detail.service';
import {Status} from '@common/enums/status.enums';

@Component({
  selector: 'app-user-contacts',
  templateUrl: './user-contacts.component.html',
  styleUrls: ['./user-contacts.component.scss']
})
export class UserContactsComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Models
   */
  public contactModel: NicoCollection<EmployeeContactsModel>;

  /**
   * Subscriptions
   */
  public userInfoSubscription: Subscription;

  /**
   * Enum
   */
  public status = Status;

  /**
   * Page title
   */
  public pageTitle = '';

  /**
   * User Info
   */
  public user: UserModel;

  /**
   * Constructor
   */
  constructor(
    protected injector: Injector,
    private userDetailService: UserDetailService
    ) {
    super(injector);
  }

  /**
   * Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_CONTACTS.PAGE_TITLE');
    super.onComponentReady();
    this.getModels();
  }

  /**
   * Get all contacts
   */
  public getModels(): void {
    this.userInfoSubscription = this.userDetailService.get().subscribe(userInfo => {
      this.user = userInfo;
      this.contactModel = this.user.contacts;
    });
  }

  /**
   * On Destroy
   */
  public ngOnDestroy(): void {
    if (this.userInfoSubscription != null) {
      this.userInfoSubscription.unsubscribe();
    }
  }

}
