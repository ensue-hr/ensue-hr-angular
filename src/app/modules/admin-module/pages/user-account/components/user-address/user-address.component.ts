import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {NicoCollection} from '@system/utilities';
import { UserModel} from '@common/models';
import {Subscription} from 'rxjs';
import {Status} from '@common/enums/status.enums';
import {UserDetailService} from '@common/services';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';
import {AddressModel} from '@common/models/address.model';
import {AddressType} from '@common/enums/addressType.enum';

@Component({

  selector: 'app-user-address',
  templateUrl: './user-address.component.html',
  styleUrls: ['./user-address.component.scss']
})
export class UserAddressComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Models
   */
  public addressModel: NicoCollection<AddressModel>;

  /**
   * Subscriptions
   */
  public userInfoSubscription: Subscription;

  /**
   * Enum
   */
  public status = Status;

  /**
   * Page title
   */
  public pageTitle = '';

  /**
   * User Info
   */
  public user: UserModel;

  /**
   * Constructor
   */
  public addressType = AddressType;
  constructor(
    protected injector: Injector,
    private userDetailService: UserDetailService
  ) {
    super(injector);
  }

  /**
   * Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.EMPLOYEE_ADDRESS.PAGE_TITLE');
    super.onComponentReady();
    this.getModels();
  }

  /**
   * Get all contacts
   */
  public getModels(): void {
    this.userInfoSubscription = this.userDetailService.get().subscribe(userInfo => {
      this.user = userInfo;
      this.addressModel = this.user.addresses;
    });
  }

  /**
   * On Destroy
   */
  public ngOnDestroy(): void {
    if (this.userInfoSubscription != null) {
      this.userInfoSubscription.unsubscribe();
    }
  }


}
