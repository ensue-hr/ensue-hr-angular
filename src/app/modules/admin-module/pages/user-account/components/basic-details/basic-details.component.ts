import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {UserDetailService} from '@common/services/user-detail.service';
import {UserModel} from '@common/models';
import {Gender} from '@common/enums/gender.enums';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';
import {Subscription} from 'rxjs';
import {MaritalStatus} from '@common/enums/maritalStatus.enums';

@Component({
  selector: 'app-basic-details',
  templateUrl: './basic-details.component.html',
  styleUrls: ['./basic-details.component.scss']
})
export class BasicDetailsComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  public model: UserModel;
  public maritalStatus = MaritalStatus;
  public pageTitle = '';
  public accountSubscription: Subscription;
  public isPersonalInformation: boolean;
  public isTenureInformation: boolean;
  public genderStatus = Gender;

  constructor(injector: Injector, private userDetailService: UserDetailService) {
    super(injector);
    this.isPersonalInformation = true;
    this.isTenureInformation = true;
  }

  onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_ACCOUNT.HTML_TITLE');
    super.onComponentReady();
    this.getModel();
  }

  public getModel(): void {
    this.userDetailService.get().subscribe(model => {
      this.model = model;
    });
  }

  public getGenderLabel(gender: number): any{
    if (gender === this.genderStatus.Male){
      return 'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.MALE_LABEL';
    }
    if (gender === this.genderStatus.Others){
      return 'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.OTHER_LABEL';
    }
    if (gender === this.genderStatus.Female){
      return 'MOD_EMPLOYEES.MOD_VIEW_DETAIL_EMPLOYEE.FEMALE_LABEL';
    }
  }

  public ngOnDestroy(): void {
    if (this.accountSubscription != null){
      this.accountSubscription.unsubscribe();
    }
  }
}
