import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicDetailsComponent } from './basic-details.component';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule
} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';

describe('BasicDetailsComponent', () => {
  let component: BasicDetailsComponent;
  let fixture: ComponentFixture<BasicDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BasicDetailsComponent ],
      providers: [
        ...MOCK_PROVIDERS
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        ReactiveFormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]

    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call get model on init', () => {
    spyOn(component, 'getModel');
    component.ngOnInit();
    expect(component.getModel).toHaveBeenCalled();
  });

  it('should change page title', () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_ACCOUNT.HTML_TITLE');
  });
});
