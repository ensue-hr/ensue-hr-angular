import {Routes} from '@angular/router';
import {UserAccountComponent} from './user-account.component';
import {BasicDetailsComponent} from './components/basic-details/basic-details.component';
import {UserContactsComponent} from './components/user-contacts/user-contacts.component';
import {UserBanksComponent} from './components/user-banks/user-banks.component';
import {UserSocialSecurityComponent} from './components/user-social-security/user-social-security.component';
import {UserAddressComponent} from './components/user-address/user-address.component';
import {UserAllocatedLeavesComponent} from './components/user-allocated-leaves/user-allocated-leaves.component';
export const USER_ACCOUNT_ROUTES: Routes = [
  {path: '', component: UserAccountComponent,
    children: [
      {path: 'basic-information', component: BasicDetailsComponent},
      {path: 'user-contacts', component: UserContactsComponent},
      {path: 'user-banks', component: UserBanksComponent},
      {path: 'user-social-security', component: UserSocialSecurityComponent},
      {path: 'user-addresses', component: UserAddressComponent},
      {path: 'user-allocated-leaves', component: UserAllocatedLeavesComponent},
      {path: '', redirectTo: 'basic-information', pathMatch: 'full'}
      ]
  }

];
