import { ComponentFixture, TestBed } from '@angular/core/testing';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {ActivatedRoute, Router} from '@angular/router';
import {EmployeeDocumentsService} from '@common/services/employee-documents.service';
import {MockEmployeesService} from '@common/testing-resources/mock-services/MockEmployeesService';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MockMatDialog} from '@common/testing-resources/mock-services';
import {MockMatDialogRef} from '@common/testing-resources/mock-services/MockMatDialogRef';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {NgxMatDatetimePickerModule, NgxMatNativeDateModule} from '@angular-material-components/datetime-picker';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {PaginatedNicoCollection} from '@system/utilities';
import {LeaveModel} from '@common/models/leave.model';
import {By} from '@angular/platform-browser';
import {LeaveService} from '@common/services/leave.service';
import {NicoStorageService} from '@system/services';
import {UtcToLocalPipe} from '@common/pipes/utc-to-local.pipe';
import {of} from 'rxjs';
import {PersonalLeaveComponent} from './personal-leave.component';

describe('PersonalLeaveComponent', () => {
  let component: PersonalLeaveComponent;
  let fixture: ComponentFixture<PersonalLeaveComponent>;
  let service: LeaveService;
  let storageService: NicoStorageService;
  const mockData = {
    avatar: undefined,
    code: undefined,
    created_at: '2021-09-17T06:54:35.000000Z',
    days: 3,
    department: undefined,
    description: 'mero khutta bacyo',
    email: undefined,
    employee: {id: 1, name: 'Manish Ratna Shakya', email: 'manish.shakya@ensue.us', avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7w-VUkvVobuC_P3TImDJalLsfOGGijkNRmA&usqp=CAU', code: 'EN200701'},
    end_at: '2021-11-23 18:14:59',
    id: 9,
    is_emergency: false,
    name: undefined,
    requested: {id: 20, name: 'Angel Ratna Roman', email: 'angel@gmail.com', avatar: null, code: 'EN202106'},
    start_at: '2021-11-20 18:15:00',
    status: 1,
    title: 'khutta bhachyo',
    type: 'sick',
    updated_at: '2021-09-17T06:54:35.000000Z',
  } as unknown as LeaveModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalLeaveComponent, UtcToLocalPipe],
      providers: [
        ...MOCK_PROVIDERS,
        {
          provide: ActivatedRoute,
          useValue:
            {
              snapshot: {
                queryParams: {
                  page: 1, sort_order: 'asc', per_page: 16
                },
                parent: {
                  params: {
                    id: 1
                  }
                }
              }
            }
        },
        {provide: Router, useValue: {
            url: 'manage/leaves/personal-leave',
            events: of('/'),
            navigate: jasmine.createSpy('navigate')
          }},
        {provide: EmployeeDocumentsService, useClass: MockEmployeesService},
        {provide: MatDialog, useClass: MockMatDialog},
        {provide: MatDialogRef, useValue: MockMatDialogRef},
        {provide: MAT_DIALOG_DATA, useValue: {model: {id: 1, requested: {id: 1}}}},
      ],
      imports: [
        TranslateModule,
        RouterTestingModule,
        ReactiveFormsModule,
        NgxMatDatetimePickerModule,
        NgxMatNativeDateModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    service = TestBed.inject(LeaveService);
    storageService = TestBed.inject(NicoStorageService);
    storageService.setItem('settings',  JSON.stringify({leave_type: ['a', 'b', 'c']}));
    fixture = TestBed.createComponent(PersonalLeaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set page title to employee leave', () => {
    component.ngOnInit();
    expect(component.pageTitle).toEqual('MOD_LEAVE.PERSONAL_VIEW.PAGE_TITLE');
  });

  it('should show view modal on click view details', () => {
    spyOn(component, 'onViewLeave');
    component.models = new PaginatedNicoCollection<LeaveModel>();
    component.models.push(mockData);
    fixture.detectChanges();
    const submitButton: DebugElement =
      fixture.debugElement.query(By.css('.card-footer'));
    submitButton.triggerEventHandler('click', null);
    expect(component.onViewLeave).toHaveBeenCalled();
  });
});
