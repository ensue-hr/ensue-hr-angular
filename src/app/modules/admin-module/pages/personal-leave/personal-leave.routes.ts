import {Routes} from '@angular/router';
import {PersonalLeaveComponent} from './personal-leave.component';

export const PERSONAL_LEAVE_ROUTES: Routes = [
  {path: '', component: PersonalLeaveComponent},

];
