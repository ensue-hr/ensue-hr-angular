import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CancellationLeaveComponent } from './cancellation-leave.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {ActivatedRoute} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MockMatDialog} from '@common/testing-resources/mock-services';
import {MockMatDialogRef} from '@common/testing-resources/mock-services/MockMatDialogRef';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {ReactiveFormsModule, Validators} from '@angular/forms';
import {NgxMatDatetimePickerModule, NgxMatNativeDateModule} from '@angular-material-components/datetime-picker';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {LeaveService} from '@common/services/leave.service';
import {MockLeaveService} from '@common/testing-resources/mock-services/MockLeaveService';
import {LeaveModel} from '@common/models/leave.model';

describe('CancellationLeaveComponent', () => {
  let component: CancellationLeaveComponent;
  let fixture: ComponentFixture<CancellationLeaveComponent>;
  let service: LeaveService;
  const mockData = {
    avatar: undefined,
    code: undefined,
    created_at: '2021-09-17T06:54:35.000000Z',
    days: 3,
    department: undefined,
    description: 'mero khutta bacyo',
    email: undefined,
    employee: {id: 1, name: 'Manish Ratna Shakya', email: 'manish.shakya@ensue.us', avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7w-VUkvVobuC_P3TImDJalLsfOGGijkNRmA&usqp=CAU', code: 'EN200701'},
    end_at: '2021-11-23 18:14:59',
    id: '9',
    is_emergency: false,
    name: undefined,
    requested: {id: 20, name: 'Angel Ratna Roman', email: 'angel@gmail.com', avatar: null, code: 'EN202106'},
    start_at: '2021-11-20 18:15:00',
    status: 1,
    title: 'khutta bhachyo',
    type: 'sick',
    updated_at: '2021-09-17T06:54:35.000000Z',
  } as unknown as LeaveModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CancellationLeaveComponent ],
      providers: [
        ...MOCK_PROVIDERS,
        {
          provide: ActivatedRoute,
          useValue:
            {
              snapshot: {
                queryParams: {
                  page: 1, sort_order: 'asc', per_page: 16
                },
                parent: {
                  params: {
                    id: 1
                  }
                }
              }
            }
        },
        {provide: LeaveService, useClass: MockLeaveService},
        {provide: MatDialog, useClass: MockMatDialog},
        {provide: MatDialogRef, useClass: MockMatDialogRef},
        {provide: MAT_DIALOG_DATA, useValue: {leave: mockData}},
      ],
      imports: [
        TranslateModule,
        RouterTestingModule,
        ReactiveFormsModule,
        NgxMatDatetimePickerModule,
        NgxMatNativeDateModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancellationLeaveComponent);
    service = TestBed.inject(LeaveService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have page title to cancellation leave', () => {
    component.ngOnInit();
    expect(component.pageTitle).toBe('MOD_LEAVE.MOD_CANCELLATION.PAGE_TITLE');
  });

  it('should be empty form on init', () => {
    component.ngOnInit();
    component.createForm();
    const dummy = {
      title: null,
      description: null,
      start_at: null,
    };
    expect(component.cancellationForm.value).toEqual(dummy);
  });

  it('should submit data on submit form ', () => {
    spyOn(component, 'onFormSubmit');
    const submitButton: DebugElement =
      fixture.debugElement.query(By.css('button[type=submit]'));
    fixture.detectChanges();
    submitButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.onFormSubmit).toHaveBeenCalledTimes(1);
  });

  it('should post data on save ', () => {
      service.onCancellationLeave(mockData.id, mockData).subscribe(response => {
        expect(response).toEqual(mockData);
      });
  });

  it('should have modal title to cancellation leave', () => {
    const title: DebugElement =
      fixture.debugElement.query(By.css('.modal-title'));
    expect(title.nativeElement.outerText).toEqual('MOD_LEAVE.MOD_CANCELLATION.PAGE_TITLE');
  });

  it('should close drawer on click cross btn', () => {
    spyOn(component.dialogRef,  'close');
    const closeBtn: DebugElement =
      fixture.debugElement.query(By.css('.icon-btn-circular'));
    closeBtn.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(closeBtn.attributes['mat-dialog-close']).toBe('');
  });


});
