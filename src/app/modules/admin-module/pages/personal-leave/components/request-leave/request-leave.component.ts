import {AfterViewInit, Component, Inject, Injector, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NicoStorageService} from '@system/services';
import {EmployeeModel} from '@common/models';
import {EmployeesService} from '@common/services';
import {Subscription} from 'rxjs';
import {EmployeeStatus} from '@common/enums/status.enums';
import {AdvancedSelectConfigInterface} from '@system/components';
import {environment} from '../../../../../../../environments/environment';
import {LeaveService} from '@common/services/leave.service';
import * as moment from 'moment';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DateUtility} from '@common/utilities/date-utility';

@Component({
  selector: 'app-request-leave',
  templateUrl: './request-leave.component.html',
})
export class RequestLeaveComponent extends AbstractBaseComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('picker') picker: any;
  @ViewChild('dropDownItemTemplate') dropDownItemTemplate: TemplateRef<any>;

  public requestLeaveSubscription: Subscription;
  public advanceSelectQueryParams = `&status=${EmployeeStatus.Active}`;
  public config: AdvancedSelectConfigInterface = { };

  public employeeInfoSubscription: Subscription;
  public setEndDateSubscription: Subscription;
  public setDateTimePickerSubscription: Subscription;
  public notifySubscription: Subscription;
  public employeesModel: EmployeeModel;
  public date: moment.Moment;
  public showSpinners = true;
  public showSeconds = true;
  public stepHour = 1;
  public stepMinute = 1;
  public stepSecond = 1;

  public rangeDate: any;
  public leaveCanRequestBeforeDays: string;
  public requestForm: FormGroup;
  public requestFormID: string;
  public leaveType = [];
  public env: any;
  public min = new Date();
  public max: Date;
  public today = DateUtility.getMomentDate();
  public url = '';
  public defaultTime = [0, 0, 0];
  public defaultTimeEnd = [23, 59, 59 ];
  public notify = false;
  public pageTitle = '';
  public oldTitle: string;
  public endMin: any;
  public switchEmergency: boolean;
  public emergency = [
    {value: false, label: this.translateService.instant('MOD_LEAVE.MOD_LEAVE_REQUEST.NORMAL_LABEL')},
    {value: true, label: this.translateService.instant('MOD_LEAVE.MOD_LEAVE_REQUEST.EMERGENCY_LABEL')}
  ];
  constructor(injector: Injector,
              private nicoStorageService: NicoStorageService,
              private employeeService: EmployeesService,
              private requestLeaveService: LeaveService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<RequestLeaveComponent>,
              private formBuilder: FormBuilder) {
    super(injector);
    this.requestFormID = 'request-form';
    this.env = environment;
    this.oldTitle = this.pageTitleService.getTitle().split('-')[1];
    this.switchEmergency = false;
  }

  public onComponentReady(): void {
    if (this.data.model){
      this.pageTitle = this.translateService.instant('MOD_LEAVE.MOD_UPDATE_REQUEST.PAGE_TITLE');
    }
    else {
      this.pageTitle = this.translateService.instant('MOD_LEAVE.MOD_LEAVE_REQUEST.PAGE_TITLE');
    }
    super.onComponentReady();
    this.createForm();
    this.getEmployeeModel();
    const leaveSetting = JSON.parse(this.nicoStorageService.getItem('settings'));
    const leaveTypes = leaveSetting.leaves_type;
    leaveTypes?.push(this.translateService.instant('MOD_LEAVE.UNPAID_LABEL'));
    leaveTypes?.push(this.translateService.instant('MOD_LEAVE.CANCELLATION_TYPE_LABEL'));
    this.leaveType = leaveTypes;
    this.leaveCanRequestBeforeDays = leaveSetting.leaves_can_request_before_days;
    this.rangeDate = DateUtility.addDate(this.today, this.leaveCanRequestBeforeDays, 'days');
    this.url = this.env.api + 'employees/managers';
    this.notifyUser();
    this.setEndDatePick();
    this.setDateTimePicker();
  }

  public createForm(): void {
    this.requestForm = this.formBuilder.group({
      title: [null, [Validators.required]],
      type: ['', [Validators.required]],
      description: [null, [Validators.required]],
      start_at: [null, [Validators.required]],
      end_at: [null, [Validators.required]],
      is_emergency: [false],
      requested_to: [null, Validators.required],
    });
    this.requestForm.controls.end_at.disable();

    if (this.data.model){
        this.requestForm.patchValue(this.data.model);
        this.requestForm.get('start_at').setValue(new Date(DateUtility.utcToLocalFormat(this.data.model.start_at)));
        this.requestForm.get('end_at').setValue(new Date(DateUtility.utcToLocalFormat(this.data.model.end_at)));
        this.requestForm.controls.end_at.enable();
        this.requestForm.get('requested_to').setValue(this.data?.model?.requested?.id);
    }
  }

  public onFormSubmit(): void{
    this.requestLeaveSubscription = this.requestLeaveService
      .saveRequestLeave(this.requestForm.value, this.data.model).subscribe(response => {
        this.dialogRef.close(true);

        if (response.length > 1){
        this.showInfoSnackBar(this.translateService.instant('MOD_LEAVE.MOD_LEAVE_REQUEST.EXTENDED_LEAVE_REQUEST'));
        }
        else {
            this.data.model ?
              this.showSuccessSnackBar(this.translateService.instant('MOD_LEAVE.MOD_LEAVE_REQUEST.REQUEST_LEAVE_UPDATE_LABEL')) :
              this.showSuccessSnackBar(this.translateService.instant('MOD_LEAVE.MOD_LEAVE_REQUEST.REQUEST_LEAVE_SUCCESS_LABEL'));
        }
    }, error => {
      this.onFormSubmissionError(error, this.requestFormID);
    });
  }

  public getEmployeeModel(): void {
    this.employeeInfoSubscription = this.employeeService.get().subscribe(models => {
      this.employeesModel = models;
    });
  }

  public ngAfterViewInit(): void {
    this.config = {
      itemTemplate: this.dropDownItemTemplate,
      selectInfoLabel: this.translateService.instant('MOD_LEAVE.MOD_LEAVE_REQUEST.REQUEST_TO_LABEL'),
      equalityCheckProperty: 'id',
      disableEmptyField: true
    };
  }
  public onRequestTo(employee: EmployeeModel): void {
      this.requestForm.get('requested_to').setValue(employee.id);
  }

  public notifyUser(): void {
    this.notifySubscription = this.requestForm.controls.start_at.valueChanges.subscribe( value => {
      this.notify = DateUtility.getDateBetween(this.today, this.rangeDate, value);
    });
  }

  public setDateTimePicker(): void {
    this.setDateTimePickerSubscription = this.requestForm.controls.is_emergency.valueChanges.subscribe( value => {
      this.requestForm.get('start_at').reset();
      this.requestForm.get('end_at').reset();
      if (value === 'true') {
        this.switchEmergency = true;
        this.min.setDate(this.min.getDate() - 14 );
        this.max = new Date();
        this.max.setDate(this.max.getDate() + 1);
      }
      else{
        this.switchEmergency = false;
        this.min = new Date();
        this.min.setDate(this.min.getDate());
        this.max = null;
      }
    });
  }
  public setEndDatePick(): void{
    this.setEndDateSubscription = this.requestForm.controls.start_at.valueChanges.subscribe( value => {
      if (value) {
        this.requestForm.controls.end_at.enable();
        this.endMin = value;
      }
    });
  }
  public ngOnDestroy(): void {
    this.pageTitleService.setTitle(this.oldTitle);
    if (this.employeeInfoSubscription != null) {
      this.employeeInfoSubscription.unsubscribe();
    }
    if (this.requestLeaveSubscription != null) {
      this.requestLeaveSubscription.unsubscribe();
    }
    if (this.setDateTimePickerSubscription != null) {
      this.setDateTimePickerSubscription.unsubscribe();
    }
    if (this.setEndDateSubscription != null) {
      this.setEndDateSubscription.unsubscribe();
    }
    if (this.notifySubscription != null) {
      this.notifySubscription.unsubscribe();
    }
  }

}
