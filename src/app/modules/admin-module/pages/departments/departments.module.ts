import { NgModule} from '@angular/core';
import {HrCommonModule} from '@common/hr-common.module';

import { DepartmentsComponent } from './departments.component';

import { DEPARTMENT_ROUTES } from './department-routes.module';

import { DepartmentService } from '@common/services/department.service';
import { NicoComponentsModule } from '@system/components';
import { RouterModule } from '@angular/router';
import { AddEditDepartmentComponent } from './components/add-edit-department/add-edit-department.component';
import { ViewDetailDepartmentComponent } from './components/view-detail-department/view-detail-department.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";

@NgModule({
  declarations: [
      DepartmentsComponent,
      AddEditDepartmentComponent,
      ViewDetailDepartmentComponent,

  ],
    imports: [
        HrCommonModule,
        NicoComponentsModule,
        RouterModule.forChild(DEPARTMENT_ROUTES),
        MatProgressSpinnerModule,

    ],
  providers: [
    DepartmentService,
  ],
})

export class DepartmentsModule { }
