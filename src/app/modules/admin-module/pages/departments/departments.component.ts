import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {DepartmentModel} from '@common/models/department.model';
import {DepartmentService} from '@common/services/department.service';
import {FlyMenuItemInterface} from '@system/components';
import {AbstractBaseComponent} from 'app/AbstractBaseComponent';
import {MatDialog} from '@angular/material/dialog';
import {AddEditDepartmentComponent} from './components/add-edit-department/add-edit-department.component';
import {ViewDetailDepartmentComponent} from './components/view-detail-department/view-detail-department.component';
import {PaginatedNicoCollection} from '@system/utilities/paginated-nico-collection';
import {Subscription} from 'rxjs';
import {Status} from '@common/enums/status.enums';
import {NicoStorageService} from '@system/services';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.scss']
})
export class DepartmentsComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  public departmentInfoSubscription: Subscription;
  public statusChangeSubscription: Subscription;
  public models: PaginatedNicoCollection<DepartmentModel>;
  public dialogRef: any;

  public pageTitle = '';
  /**
   * To determine whether the view is table or card
   */
  public isTableView: boolean;

  public status = Status;
  public statusDropdownValues = [
    {
      status: this.translateService.instant('MOD_DEPARTMENT.STATUS_DROPDOWN_VALUES.ALL'),
      value: 'all'
    },
    {
      status: this.translateService.instant('MOD_DEPARTMENT.STATUS_DROPDOWN_VALUES.UNPUBLISHED'),
      value: 1
    },
    {
      status: this.translateService.instant('MOD_DEPARTMENT.STATUS_DROPDOWN_VALUES.PUBLISHED'),
      value: 2
    }
  ];

  public sortByDropdownValues = [
    {
      label: this.translateService.instant('MOD_DEPARTMENT.SORT_BY_DROPDOWN_VALUES.TITLE'),
      value: 'title'
    },
    {
      label: this.translateService.instant('MOD_DEPARTMENT.SORT_BY_DROPDOWN_VALUES.STATUS'),
      value: 'status'
    },
    {
      label: this.translateService.instant('MOD_EMPLOYEES.SORT_BY_DROPDOWN_VALUES.CREATED_AT'),
      value: 'created_at'
    },
  ];




  public flyMenuAddOns: FlyMenuItemInterface[] = [
    {
      name: 'edit',
      label: this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.EDIT_BUTTON_LABEL'),
      active: true,
      faIcon: 'far fa-pen'
    },
    {
      name: 'remove',
      label: this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.REMOVE_BUTTON_LABEL'),
      active: true,
      faIcon: 'far fa-trash'
    },
    {
      name: 'statusChange',
      label: this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.UNPUBLISH_LABEL'),
      active: true,
      faIcon: 'far fa-publish'
    }
  ];

  public constructor(
    protected injector: Injector,
    public departmentService: DepartmentService,
    public dialog: MatDialog,
    protected storageService: NicoStorageService) {
    super(injector);
  }

  public onFlyMenuAction(event: any, model: DepartmentModel): void {
    switch (event) {
      case 'remove':
        this.onRemove(model);
        break;

      case 'edit':
        this.dialogView(AddEditDepartmentComponent, model);
        break;

      case 'viewDetail':
        this.dialogView(ViewDetailDepartmentComponent, model);
        break;

      case 'statusChange':
        this.onSwitchValueChange(model.id);
    }
  }

  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_DEPARTMENT.PAGE_TITLE');
    super.onComponentReady();
    this.getModels();
    this.storageService.getItem('view') ?
      this.isTableView = this.storageService.getItem('view') === 'true' : this.isTableView = false;
  }

  public getFlyMenuAdOns(model: DepartmentModel): FlyMenuItemInterface[] {
    const menu = [...this.flyMenuAddOns];
    menu[menu.length - 1].label = model.status === Status.Unpublished ?
      this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.PUBLISH_LABEL')
      : this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.UNPUBLISH_LABEL');
    menu[menu.length - 1].faIcon = model.status === Status.Unpublished ?
      'far fa-eye'
      : 'far fa-eye-slash';
    return menu;
  }

  public getModels(): void {
    this.departmentInfoSubscription = this.departmentService.get(this.getFilterHttpQueryParams()).subscribe((models) => {
      this.models = models;
    });
  }

  public onAddDepartment(): void {
    const modalDialog = this.dialog.open(AddEditDepartmentComponent, {panelClass: ['modal-drawer-view', 'modal-xs'], disableClose: true});
    modalDialog.afterClosed().subscribe(reload => {
      this.pageTitle = this.translateService.instant('MOD_DEPARTMENT.PAGE_TITLE');
      if (reload) {
        this.getModels();
      }
    });
  }

  public onSwitchValueChange(id): void {
    this.statusChangeSubscription = this.departmentService.changeStatus({id})
      .subscribe((state) => {
        this.getModels();
      });
  }

  public dialogView(component: any, model?: DepartmentModel): void {
    this.breadcrumbService.set('@departmentName', ' ');
    const modalDialog = this.dialog.open(component, {data: {model}, panelClass: ['modal-drawer-view', 'modal-xs'], disableClose: true});
    modalDialog.afterClosed().subscribe(reload => {
        if (reload) {
          this.getModels();
        }
    });
  }

  public ngOnDestroy(): void {
    if (this.departmentInfoSubscription != null) {
      this.departmentInfoSubscription.unsubscribe();
    }
  }

  public onRemove(model: DepartmentModel): void {
    const {id} = model;
    this.dialogRef = this.openConfirmationDialogBox({
      title: this.translateService.instant('MOD_DEPARTMENT.DELETE_DEPARTMENT_LABEL'),
      message: this.translateService.instant('MOD_DEPARTMENT.DELETE_DEPARTMENT_MESSAGE'),
    });
    this.dialogRef.afterClosed().subscribe(confirmation => {
      if (confirmation) {
        this.departmentService.deleteDepartment(id).subscribe(response => {
          this.dialogRef.close(true);
          this.getModels();
        });
      }
    });
  }

  /**
   * View Toggle change
   */
  public onViewToggle($event): void {
    this.isTableView = $event;
  }
}
