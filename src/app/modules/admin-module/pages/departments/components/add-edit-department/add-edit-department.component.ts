import {Inject, OnDestroy} from '@angular/core';
import {Component, Injector, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DepartmentService} from '@common/services/department.service';
import {AbstractBaseComponent} from 'app/AbstractBaseComponent';
import {Subscription} from 'rxjs';
import {Status} from '@common/enums/status.enums';
import {NicoUtils} from '@system/utilities';

@Component({
  selector: 'app-edit-department',
  templateUrl: './add-edit-department.component.html',
  styleUrls: ['./add-edit-department.component.scss']
})

export class AddEditDepartmentComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  public editDepartmentSubscription: Subscription;
  public switchState: boolean;
  public departmentForm: FormGroup;
  public pageTitle = '';
  public oldTitle: string;
  public editDepartmentFormID: string;

  constructor(protected injector: Injector,
              private departmentService: DepartmentService,
              public dialogRef: MatDialogRef<AddEditDepartmentComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    super(injector);
    this.switchState = false;
  }

  public onComponentReady(): void {
    this.data ?
      this.pageTitle = this.translateService.instant('MOD_DEPARTMENT.MOD_EDIT_DEPARTMENT.PAGE_TITLE') :
      this.pageTitle = this.translateService.instant('MOD_DEPARTMENT.MOD_ADD_DEPARTMENT.PAGE_TITLE');
    super.onComponentReady();
    this.createFormGroup();
    this.oldTitle = this.translateService.instant('MOD_DEPARTMENT.PAGE_TITLE');
    this.editDepartmentFormID = 'add-edit-department';
  }

  public onSwitchValueChange(): void {
    this.switchState = !this.switchState;
    if (this.departmentForm.value.status === Status.Published) {
      this.departmentForm.get('status').setValue(Status.Unpublished);
    } else {
      this.departmentForm.get('status').setValue(Status.Published);
    }
  }

  public onSubmitForm(): void {
    this.editDepartmentSubscription = this.departmentService
      .saveDepartment(this.departmentForm.getRawValue(), this.data && {id: this.data.model?.id})
      .subscribe(response => {
        this.dialogRef.close(true);
        NicoUtils.isNullOrUndefined(this.data) ?
          this.showSuccessSnackBar(this.translateService.instant('MOD_DEPARTMENT.MOD_ADD_DEPARTMENT.ADD_DEPARTMENT_SUCCESS_LABEL')) :
          this.showSuccessSnackBar(this.translateService.instant('MOD_DEPARTMENT.MOD_EDIT_DEPARTMENT.EDIT_DEPARTMENT_SUCCESS_LABEL'));
      }, error => {
        this.onFormSubmissionError(error, this.editDepartmentFormID);
      });
  }

  public createFormGroup(): void {
    this.departmentForm = new FormGroup({
      title: new FormControl(null, [Validators.required]),
      description: new FormControl(null, [Validators.required]),
      status: new FormControl(null)
    });

    if (this.data) {
      this.departmentForm.patchValue(this.data?.model);
      this.switchState = this.data.model?.status === Status.Published;
    } else {
      this.departmentForm.get('status').setValue(Status.Unpublished);
    }
  }

  ngOnDestroy(): void {
    this.pageTitleService.setTitle(this.oldTitle);
    if (this.editDepartmentSubscription != null) {
      this.editDepartmentSubscription.unsubscribe();
    }
  }
}
