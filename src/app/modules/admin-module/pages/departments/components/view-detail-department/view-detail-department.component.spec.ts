import {ComponentFixture, TestBed} from '@angular/core/testing';

import { ViewDetailDepartmentComponent } from './view-detail-department.component';
import {DepartmentService} from '@common/services/department.service';
import {MockDepartmentService} from '@common/testing-resources/mock-services/MockDepartmentService';
import {NicoHttpClient} from '@system/http-client';
import {
  MockMatDialog,
  MockMatSnackBar, MockMissingTranslationHandler,
  MockNicoHttpClient,
  MockNicoPageTitle,
  MockNicoSessionService,
  MockNicoStorageService
} from '@common/testing-resources/mock-services';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MockFormGroup} from '@common/testing-resources/mock-services/MockFormGroup';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService, NicoStorageService} from '@system/services';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog, MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {ActivatedRoute} from '@angular/router';
import {
  DEFAULT_LANGUAGE,
  MissingTranslationHandler,
  TranslateCompiler,
  TranslateFakeCompiler,
  TranslateFakeLoader,
  TranslateLoader, TranslateModule, TranslateParser,
  TranslateService,
  TranslateStore, USE_DEFAULT_LANG, USE_EXTEND, USE_STORE
} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {DepartmentModel} from '@common/models';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {BreadcrumbService} from 'xng-breadcrumb';
import {MockBreadcrumService} from '@common/testing-resources/mock-services/MockBreadcrumService';

describe('ViewDetailDepartmentComponent', () => {
  let component: ViewDetailDepartmentComponent;
  let fixture: ComponentFixture<ViewDetailDepartmentComponent>;
  let service: DepartmentService;
  let pageTitleService: NicoPageTitle;
  const mockData = {
    created_at: '2021-07-15T16:25:17.000000Z',
    description: 'Finance Department',
    employees_count: 0,
    hod: null,
    id: 4,
    status: 1,
    title: 'Finance department',
    updated_at: '2021-07-16T07:39:38.000000Z',
  } as unknown as DepartmentModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ViewDetailDepartmentComponent],
      providers: [
        {provide: DepartmentService, useClass: MockDepartmentService},
        {provide: NicoHttpClient, useClass: MockNicoHttpClient},
        {provide: FormGroup, useClass: MockFormGroup},
        {provide: NicoStorageService, useClass: MockNicoStorageService},
        {provide: NicoPageTitle, useClass: MockNicoPageTitle},
        {provide: NicoSessionService, useClass: MockNicoSessionService},
        {provide: MatSnackBar, useClass: MockMatSnackBar},
        {provide: MatDialog, useValue: MockMatDialog},
        {provide: ActivatedRoute, useValue:  { snapshot: {params: {id: 1}}}},
        {provide: MatDialogRef, useValue: {}},
        {provide: MAT_DIALOG_DATA, useValue: {model: mockData}},
        {provide: BreadcrumbService, useClass: MockBreadcrumService},
        FormBuilder,
        AjaxSpinnerService,
        TranslateService,
        TranslateStore,
        {provide: TranslateLoader, useClass: TranslateFakeLoader},
        {provide: TranslateCompiler, useClass: TranslateFakeCompiler},
        TranslateParser,
        {provide: MissingTranslationHandler, useClass: MockMissingTranslationHandler},
        {provide: USE_DEFAULT_LANG, useValue: undefined},
        {provide: USE_STORE, useValue: undefined},
        {provide: USE_EXTEND, useValue: undefined},
        {provide: DEFAULT_LANGUAGE, useValue: undefined},
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        MatDialogModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    service = TestBed.inject(DepartmentService);
    pageTitleService = TestBed.inject(NicoPageTitle);
    fixture = TestBed.createComponent(ViewDetailDepartmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change page title to change', () => {
    component.ngOnInit();
    expect(component.pageTitle).toBe('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.PAGE_TITLE');
  });

  it('should show no. of employees as 5 Employees', () => {
    component.model = {
      id: 1,
      title: 'HR',
      description: 'This is a test',
      status: 2,
      employees_count: 5,
      hod: '',
      employees: [{id: 1, name: 'Jane Doe'}]
    } as unknown as DepartmentModel;
    fixture.detectChanges();
    const empCount = fixture.debugElement.nativeElement.querySelector('span');
    expect(empCount.innerHTML).toEqual(' 5 MOD_DEPARTMENT.EMPLOYEE_COUNT_PLURAL ');
  });

  it('should show no. of employees as 1 Employee', () => {
    component.model = {
      id: 1,
      title: 'HR',
      description: 'This is a test',
      status: 2,
      employees_count: 1,
      hod: '',
      employees: [{id: 1, name: 'Jane Doe'}]
    } as unknown as DepartmentModel;
    fixture.detectChanges();
    const empCount = fixture.debugElement.nativeElement.querySelector('span');
    expect(empCount.innerHTML).toEqual(' 1 MOD_DEPARTMENT.EMPLOYEE_COUNT_SINGULAR ');
  });

  it('should show no. of employees as No Employees', () => {
    component.model = {
      id: 1,
      title: 'HR',
      description: 'This is a test',
      status: 2,
      employees_count: 0,
      hod: '',
      employees: [{id: 1, name: 'Jane Doe'}]
    } as unknown as DepartmentModel;
    fixture.detectChanges();
    const empCount = fixture.debugElement.nativeElement.querySelector('span');
    expect(empCount.innerHTML).toEqual(' MOD_DEPARTMENT.EMPLOYEE_COUNT_ZERO ');
  });
});
