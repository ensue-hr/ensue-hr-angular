import {AfterViewInit, Component, Injector, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {DepartmentModel} from '@common/models';
import {EmployeeModel} from '@common/models/employee.model';
import {Hod} from '@common/enums/hod.enums';
import {DepartmentService} from '@common/services/department.service';
import {MatDialog} from '@angular/material/dialog';
import {EmployeesService} from '@common/services/employees.service';
import {AddEditDepartmentComponent} from '../../../add-edit-department/add-edit-department.component';
import {EmployeeStatus, Status} from '@common/enums/status.enums';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {AdvancedSelectConfigInterface} from '@system/components';
import {environment} from '../../../../../../../../../environments/environment';
import {PaginatedNicoCollection} from '@system/utilities';

@Component({
  selector: 'app-basic-information',
  templateUrl: './basic-info-department.component.html',
  styleUrls: ['./basic-info-department.component.scss']
})
export class BasicInfoDepartmentComponent extends AbstractBaseComponent implements OnInit, OnDestroy, AfterViewInit {
  /**
   * Subscriptions
   */
  public departmentInfoSubscription: Subscription;
  public employeeInfoSubscription: Subscription;
  public departmentId: string;

  public publishStatus = Status;

  public testConfig = {
    enableSearchInput: true,
    enableToggle: false,
    enableSortOrder: false,
    enableSortBy: false,
    enableStatus: false,
  };

  /**
   * An observable to watch search text change
   */
  protected searchTextChange: Subject<string> = new Subject<string>();
  public searchTextChangeSubscription: Subscription;

  @ViewChild('dropDownItemTemplate') dropDownItemTemplate: TemplateRef<any>;
  @ViewChild('dropDownEmptyTemplate') dropDownEmptyTemplate: TemplateRef<any>;
  public viewDetailDepartmentFormID = 'view-detail-department';


  public advanceSelectQueryParams = `&status=${EmployeeStatus.Active}`;
  public config: AdvancedSelectConfigInterface = { };
  public url = '';
  public env: any;

  public model: DepartmentModel;
  public employeesModel: PaginatedNicoCollection<EmployeeModel>;

  public switchState = false;

  public status: number;

  public oldTitle = this.translateService.instant('MOD_DEPARTMENT.PAGE_TITLE');
  public isAssignHOD: boolean;
  public HOD_ID = Hod.Not_assigned;

  constructor(protected injector: Injector,
              private departmentService: DepartmentService,
              public dialog: MatDialog,
              private employeeService: EmployeesService,
             ) {
    super(injector);
    this.env = environment;
  }

  onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_DEPARTMENT.MOD_VIEW_DEPARTMENT.PAGE_TITLE');
    super.onComponentReady();
    this.getDepartmentId();
    this.getDepartmentModel();
    this.getEmployeeModel();
    this.subscribeSearchTextChange();
    this.isAssignHOD = false;
    setTimeout(() => {
      this.config.equalityCheckProperty = 'id';
    }, 100);
    this.url = this.env.api + 'employees';


  }


  public onDepartmentEditClick(model: DepartmentModel): void {
    const modalDialog = this.dialog.open(AddEditDepartmentComponent, {
      data: {model},
      panelClass: ['modal-drawer-view', 'modal-xs'],
      disableClose: true
    });
    modalDialog.afterClosed().subscribe(reload => {
      if (reload) {
        this.getModel();
      }
    });
  }

  public onChangeHod(employee: EmployeeModel): void{
    this.departmentService.changeHOD(this.model.id, {employee_id : employee ? employee.id : null}).subscribe((response) => {
    });
  }

  public getDepartmentId(): void {
    this.departmentId = this.activatedRoute.parent?.snapshot.params.id;
  }

  public getDepartmentModel(): void {
    this.activatedRoute.parent?.data.subscribe((model: any) => {
      this.model = model.departmentResolver;
      this.breadcrumbService.set('@departmentName', this.model?.title);
      if (this.model.status === Status.Published) {
        this.switchState = true;
        this.status = Status.Published;
      } else {
        this.switchState = false;
        this.status = Status.Unpublished;
      }

      if (this.model.hod) {
        this.HOD_ID = model.hod?.id;
      }
    });
  }

  public getModel(): void {
    this.departmentInfoSubscription = this.departmentService.getDetail({id: this.departmentId})
      .subscribe((model) => {
        this.model = model;
        this.departmentService.setUserDetailChange(this.model);
        if (this.model.status === Status.Published) {
          this.switchState = true;
          this.status = Status.Published;
        } else {
          this.switchState = false;
          this.status = Status.Unpublished;
        }

        if (this.model.hod) {
          this.HOD_ID = model.hod?.id;
        }
      });
  }

  public getEmployeeModel(): void {
    this.filterQueryParams.department_id = this.departmentId;
    this.setFilterHttpQueryParams();
    this.employeeInfoSubscription = this.employeeService.get(this.getFilterHttpQueryParams()).subscribe(models => {
      this.employeesModel = models;
    });
  }

  ngOnDestroy(): void {
    this.pageTitleService.setTitle(this.oldTitle);
    if (this.departmentInfoSubscription != null) {
      this.departmentInfoSubscription.unsubscribe();
    }
    if (this.employeeInfoSubscription != null) {
      this.employeeInfoSubscription.unsubscribe();
    }
  }

  public goBack(): void {
    this.location.back();
  }

  public onClose(): void {
    this.isAssignHOD = !this.isAssignHOD;
  }

  /**
   * Event listener for search text change in search-nav component
   */
  public onSearchTextChange(evt: string): void {
    this.searchTextChange.next(evt);
  }

  /**
   * Make a subscription to search Text change.
   * We should never run function that take longer time to execute directly when a model/text changes.
   * We need to have a debounce time and see if the text is distinct
   */
  protected subscribeSearchTextChange(): void {
    this.searchTextChangeSubscription = this.searchTextChange.pipe(debounceTime(500), distinctUntilChanged())
      .subscribe((value: string) => {
        this.filterQueryParams.keyword = value;
        this.filterQueryParams.page = 1;
        this.setFilterHttpQueryParams();
        this.getEmployeeModel();
      });
  }

  ngAfterViewInit(): void {
    this.config = {
      itemTemplate: this.dropDownItemTemplate,
      emptySelectionTemplate: this.dropDownEmptyTemplate,
      selectInfoLabel: 'Select Employee',
      equalityCheckProperty: 'id'
    };
  }
}
