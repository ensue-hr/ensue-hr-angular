import {Routes} from '@angular/router';
import {ViewDetailDepartmentComponent} from './view-detail-department.component';
import {BasicInfoDepartmentComponent} from './components/basic-info-department/basic-info-department.component';

export const VIEW_DETAIL_DEPARTMENT_ROUTES: Routes = [
  {
    path: '',
    component: ViewDetailDepartmentComponent,

    children: [
      {
        path: '',
        redirectTo: 'basic-info-department',
        pathMatch: 'full'
      },
      {
        path: 'basic-info-department',
        component: BasicInfoDepartmentComponent,
        data: {breadcrumb: {alias: 'departmentName'}},

      }
    ]
  }
  ];
