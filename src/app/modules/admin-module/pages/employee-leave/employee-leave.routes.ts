import {Routes} from '@angular/router';
import {EmployeeLeaveComponent} from './employee-leave.component';

export const EMPLOYEE_LEAVE_ROUTES: Routes = [
  {path: '', component: EmployeeLeaveComponent},

];
