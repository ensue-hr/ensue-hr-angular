import {Component, Inject, Injector, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {LeaveModel} from '@common/models/leave.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LeaveStatus} from '@common/enums/status.enums';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {LeaveService} from '@common/services/leave.service';
import {AppRouteConstants} from '@common/constants/appRouteConstants';
import {DeclineModalComponent} from '@common/components/decline-modal/decline-modal.component';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';
// @ts-ignore
import {StatusInterface} from '@common/interface/status.interface';

@Component({
  selector: 'app-employee-leave-detail-view',
  templateUrl: './employee-leave-detail-view.component.html',
  styleUrls: ['./employee-leave-detail-view.component.scss']
})
export class EmployeeLeaveDetailViewComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  public leaveApproveSubscription: Subscription;
  public modelLeaveSubscription: Subscription;
  public viewReason: boolean;
  public model: LeaveModel;
  public declineForm: FormGroup;
  public leaveForm: FormGroup;
  public declineFormID: string;
  public leaveForEmployee: boolean;
  public leaveStatus = LeaveStatus;
  public oldTitle: string;
  public pageTitle = '';

  constructor(injector: Injector,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<EmployeeLeaveDetailViewComponent>,
              public confirmationRefs: MatDialogRef<EmployeeLeaveDetailViewComponent>,
              public leaveService: LeaveService,
              public formBuilder: FormBuilder,
              public dialog: MatDialog
  ) {
    super(injector);
    this.viewReason = false;
    this.data.leaveFor === AppRouteConstants.EMPLOYEE_LEAVE ?
      this.leaveForEmployee = true :
      this.leaveForEmployee = false;
    this.declineFormID = 'declineFormID';
    this.oldTitle = this.pageTitleService.getTitle().split('-')[1];

  }

  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_LEAVE.LEAVE_DETAIL_LABEL');
    super.onComponentReady();
    this.onCreateForm();
    this.getModel();
  }

  public getModel(): void {
    this.modelLeaveSubscription = this.leaveService.getLeavesDetail(this.data.leave.id).subscribe(model => {
      this.model = model;
      this.setFormData(this.model);
    });
  }

  public setFormData(model: LeaveModel): void {
    this.leaveForm.patchValue(model);
  }
  public onCreateForm(): void {
    this.declineForm = this.formBuilder.group({
      reason: ['', Validators.required]
    });
    this.leaveForm = this.formBuilder.group({
      title: [null],
      description: [null],
      start_at: [null],
      end_at: [null],
    });
  }

  public onDecline(): void {
    const modalDialog = this.dialog.open(DeclineModalComponent, {panelClass: 'modal-xs',
      data: {model: this.model, message: this.translateService.instant('MOD_LEAVE.LEAVE_DECLINE_MESSAGE')}});
    modalDialog.afterClosed().subscribe(reload => {
      if (reload) {
        this.dialogRef.close(true);
      }
    });
  }

  public onApprove(): void {
    this.confirmationRefs = this.openConfirmationDialogBox({
      title: this.translateService.instant('MOD_LEAVE.APPROVE_LEAVE_LABEL'),
      message: this.translateService.instant('MOD_LEAVE.APPROVE_CONFIRMATION_MESSAGE')
    });
    this.confirmationRefs.afterClosed().subscribe(confirmation => {
      if (confirmation) {
        this.confirmationRefs.close(true);
        this.leaveApproveSubscription = this.leaveService.onApproveLeave(this.model).subscribe(response => {
          this.dialogRef.close(true);
          this.showSuccessSnackBar(this.translateService.instant('MOD_LEAVE.APPROVE_MESSAGE'));
        });
      }
    });
  }


// @ts-ignore
  public getPillData(status): StatusInterface {
    switch (status) {
      case LeaveStatus.Approved:
        return {
          label: this.translateService.instant('MOD_LEAVE.APPROVED_LABEL'),
          color: 'success'
        };

      case LeaveStatus.Pending:
        return {
          label: this.translateService.instant('MOD_LEAVE.PENDING_LABEL'),
          color: 'info'
        };

      case LeaveStatus.Cancelled:
        return {
          label: this.translateService.instant('MOD_LEAVE.CANCELLED_LABEL'),
          color: 'danger'
        };

      case LeaveStatus.Declined:
        return {
          label: this.translateService.instant('MOD_LEAVE.DECLINED_LABEL'),
          color: 'danger'
        };

      default:
        return {
          label: '',
          color: null
        };
    }
  }

  ngOnDestroy(): void {
    this.pageTitleService.setTitle(this.oldTitle);
    if (this.leaveApproveSubscription != null){
      this.leaveApproveSubscription.unsubscribe();
    }
    if (this.modelLeaveSubscription != null){
      this.modelLeaveSubscription.unsubscribe();
    }
  }

}
