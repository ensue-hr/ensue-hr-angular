import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeRequestLeaveComponent } from './employee-request-leave.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {ActivatedRoute} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MockMatDialog} from '@common/testing-resources/mock-services';
import {MockMatDialogRef} from '@common/testing-resources/mock-services/MockMatDialogRef';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {NgxMatDatetimePickerModule, NgxMatNativeDateModule} from '@angular-material-components/datetime-picker';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {LeaveModel} from '@common/models/leave.model';
import {LeaveService} from '@common/services/leave.service';
import {MockLeaveService} from '@common/testing-resources/mock-services/MockLeaveService';
import {NicoStorageService} from '@system/services';
import {By} from '@angular/platform-browser';

describe('EmployeeRequestLeaveComponent', () => {
  let component: EmployeeRequestLeaveComponent;
  let fixture: ComponentFixture<EmployeeRequestLeaveComponent>;
  let service: LeaveService;
  let storageService: NicoStorageService;
  const mockData = {
    avatar: undefined,
    code: undefined,
    created_at: '2021-09-17T06:54:35.000000Z',
    days: 3,
    department: undefined,
    description: 'mero khutta bacyo',
    email: undefined,
    employee: {id: 1, name: 'Manish Ratna Shakya', email: 'manish.shakya@ensue.us', avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7w-VUkvVobuC_P3TImDJalLsfOGGijkNRmA&usqp=CAU', code: 'EN200701'},
    end_at: '2021-11-23 18:14:59',
    id: '9',
    is_emergency: false,
    name: undefined,
    requested: {id: 20, name: 'Angel Ratna Roman', email: 'angel@gmail.com', avatar: null, code: 'EN202106'},
    start_at: '2021-11-20 18:15:00',
    status: 1,
    title: 'khutta bhachyo',
    type: 'sick',
    updated_at: '2021-09-17T06:54:35.000000Z',
  } as unknown as LeaveModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeRequestLeaveComponent ],
      providers: [
        ...MOCK_PROVIDERS,
        {
          provide: ActivatedRoute,
          useValue:
            {
              snapshot: {
                queryParams: {
                  page: 1, sort_order: 'asc', per_page: 16
                },
                parent: {
                  params: {
                    id: 1
                  }
                }
              }
            }
        },
        {provide: LeaveService, useClass: MockLeaveService},
        {provide: MatDialog, useClass: MockMatDialog},
        {provide: MatDialogRef, useValue: MockMatDialogRef},
        {provide: MAT_DIALOG_DATA, useValue: {model: {id: 1, requested: {id: 1}}}},
      ],
      imports: [
        TranslateModule,
        RouterTestingModule,
        ReactiveFormsModule,
        NgxMatDatetimePickerModule,
        NgxMatNativeDateModule,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ]

  })
    .compileComponents();
  });

  beforeEach(() => {
    service = TestBed.inject(LeaveService);
    storageService = TestBed.inject(NicoStorageService);
    storageService.setItem('settings',  JSON.stringify({leave_type: ['a', 'b', 'c']}));
    fixture = TestBed.createComponent(EmployeeRequestLeaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set page title to request for employee', () => {
    component.ngOnInit();
    expect(component.pageTitle).toEqual('MOD_LEAVE.MOD_LEAVE_REQUEST_FOR.PAGE_TITLE');
  });

  it('should set modal title to request for employee', () => {
    const title: DebugElement =
      fixture.debugElement.query(By.css('.modal-title'));
    expect(title.nativeElement.outerText).toEqual('MOD_LEAVE.MOD_LEAVE_REQUEST_FOR.PAGE_TITLE');
  });

  it('should call submit form on save', () => {
    spyOn(component, 'onFormSubmit');
    const submitButton: DebugElement =
      fixture.debugElement.query(By.css('button[type=submit]'));
    submitButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.onFormSubmit).toHaveBeenCalled();
  });

  it('should post data on save', () => {
    const dummy = {
      start_at: '2021-11-20 18:15:00',
      status: 1,
      title: 'khutta bhachyo',
      type: 'sick',
      end_at: '2021-11-23 18:14:59',
    } as unknown as LeaveModel;
    service.saveRequestLeave(dummy, 'nthg').subscribe(response => {
      expect(response).toEqual(mockData);
    });
  });


  it('should disable end at if start at is not filled', () => {
    component.data = {};
    component.setEndDatePick();
    expect(component.requestForm.controls.end_at.status).toBe('DISABLED');
  });

  it('should be greater value of end at of start at', () => {
    component.data = {};
    component.createForm();
    component.requestForm.get('start_at').setValue(new Date('2021/9/18 00:00:00'));
    component.endMin = component.requestForm.get('start_at').value;
    fixture.detectChanges();
    component.setEndDatePick();
    component.requestForm.get('start_at').setValue(new Date('2021/9/17 00:00:00'));
    fixture.detectChanges();
    expect(component.requestForm.get('end_at').status).toBe('INVALID');
  });
});
