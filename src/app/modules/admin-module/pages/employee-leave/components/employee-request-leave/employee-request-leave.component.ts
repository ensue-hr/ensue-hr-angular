import {AfterViewInit, Component, Inject, Injector, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {EmployeeStatus} from '@common/enums/status.enums';
import {AdvancedSelectConfigInterface} from '@system/components';
import {EmployeeModel} from '@common/models';
import * as moment from 'moment';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DateUtility} from '@common/utilities/date-utility';
import {NicoStorageService} from '@system/services';
import {EmployeesService} from '@common/services';
import {LeaveService} from '@common/services/leave.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {environment} from '../../../../../../../environments/environment';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';

@Component({
  selector: 'app-employee-request-leave',
  templateUrl: './employee-request-leave.component.html',
  styleUrls: ['./employee-request-leave.component.scss']
})
export class EmployeeRequestLeaveComponent extends AbstractBaseComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('picker') picker: any;
  @ViewChild('dropDownItemTemplate') dropDownItemTemplate: TemplateRef<any>;

  public requestLeaveSubscription: Subscription;

  public advanceSelectQueryParams = `&status=${EmployeeStatus.Active}`;
  public config: AdvancedSelectConfigInterface = { };

  public employeeInfoSubscription: Subscription;
  public notifySubscription: Subscription;
  public employeesModel: EmployeeModel;
  public date: moment.Moment;
  public showSpinners = true;
  public showSeconds = true;
  public stepHour = 1;
  public stepMinute = 1;
  public stepSecond = 1;

  public rangeDate: any;
  public leaveCanRequestBeforeDays: string;
  public requestForm: FormGroup;
  public requestFormID: string;
  public leaveType: any;
  public env: any;
  public min = new Date();
  public max: Date;
  public today = DateUtility.getMomentDate();
  public url = '';
  public defaultTime = [0, 0, 0];
  public defaultTimeEnd = [23, 59, 59 ];
  public notify = false;
  public pageTitle = '';
  public oldTitle = '';
  public endMin: any;
  public switchEmergency: boolean;
  public emergency = [
    {value: false, label: this.translateService.instant('MOD_LEAVE.MOD_LEAVE_REQUEST.NORMAL_LABEL')},
    {value: true, label: this.translateService.instant('MOD_LEAVE.MOD_LEAVE_REQUEST.EMERGENCY_LABEL')}
  ];
  constructor(injector: Injector,
              private nicoStorageService: NicoStorageService,
              private employeeService: EmployeesService,
              private requestLeaveService: LeaveService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<EmployeeRequestLeaveComponent>,
              private formBuilder: FormBuilder) {
    super(injector);
    this.requestFormID = 'request-form';
    this.env = environment;
    this.oldTitle = this.pageTitleService.getTitle().split('-')[1];
    this.switchEmergency = false;
  }

  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_LEAVE.MOD_LEAVE_REQUEST_FOR.PAGE_TITLE');
    super.onComponentReady();
    this.createForm();
    this.getEmployeeModel();
    const leaveSetting = JSON.parse(this.nicoStorageService.getItem('settings'));
    const leaveTypes = leaveSetting.leaves_type;
    leaveTypes?.push(this.translateService.instant('MOD_LEAVE.UNPAID_LABEL'));
    this.leaveType = leaveTypes;
    this.leaveCanRequestBeforeDays = leaveSetting.leaves_can_request_before_days;
    this.rangeDate = DateUtility.addDate(this.today, this.leaveCanRequestBeforeDays, 'days');
    this.url = this.env.api + 'employees';
    this.notifyUser();
    this.setEndDatePick();
    this.min.setDate(this.min.getDate() - 14 );
    this.max = new Date();
    this.max.setDate(this.max.getDate() + 1);
  }

  public createForm(): void {
    this.requestForm = this.formBuilder.group({
      title: [null, [Validators.required]],
      type: ['', [Validators.required]],
      description: [null, [Validators.required]],
      start_at: [null, [Validators.required]],
      end_at: [null, [Validators.required]],
      employee_id: [null, Validators.required]
    });
    this.requestForm.controls.end_at.disable();
  }

  public onFormSubmit(): void{
    this.requestLeaveSubscription = this.requestLeaveService
      .saveEmployeeRequestLeave(this.requestForm.value).subscribe(response => {
        this.dialogRef.close(true);
        this.showSuccessSnackBar(this.translateService.instant('MOD_LEAVE.MOD_LEAVE_REQUEST.REQUEST_EMPLOYEE_LEAVE_SUCCESS_LABEL'));
      }, error => {
        this.onFormSubmissionError(error, this.requestFormID);
      });
  }

  public getEmployeeModel(): void {
    this.employeeInfoSubscription = this.employeeService.get().subscribe(models => {
      this.employeesModel = models;
    });
  }

  public ngAfterViewInit(): void {
    this.config = {
      itemTemplate: this.dropDownItemTemplate,
      selectInfoLabel: this.translateService.instant('MOD_LEAVE.EMPLOYEE_LABEL'),
      equalityCheckProperty: 'id',
      disableEmptyField: true
    };
  }
  public onRequestTo(employee: EmployeeModel): void {
    this.requestForm.get('employee_id').setValue(employee.id);
  }

  public notifyUser(): void {
    this.notifySubscription = this.requestForm.controls.start_at.valueChanges.subscribe( value => {
      this.notify = DateUtility.getDateBetween(this.today, this.rangeDate, value);
    });
  }

  public setEndDatePick(): void{
    this.requestForm.controls.start_at.valueChanges.subscribe( value => {
      if (value) {
        this.requestForm.controls.end_at.enable();
        this.endMin = value;
      }
    });
  }
  public ngOnDestroy(): void {
    this.pageTitleService.setTitle(this.oldTitle);
    if (this.employeeInfoSubscription != null) {
      this.employeeInfoSubscription.unsubscribe();
    }
    if (this.requestLeaveSubscription != null) {
      this.requestLeaveSubscription.unsubscribe();
    }
  }

}
