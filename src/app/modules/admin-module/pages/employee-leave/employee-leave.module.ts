import {RouterModule} from '@angular/router';
import {NicoFileUploadModule} from '@system/modules/nico-file-upload';
import {NgModule} from '@angular/core';
import {HrCommonModule} from '@common/hr-common.module';
import {TranslateModule} from '@ngx-translate/core';
import {MatInputModule} from '@angular/material/input';
import {NicoComponentsModule} from '@system/components';
import {EmployeeLeaveComponent} from './employee-leave.component';
import {EmployeeRequestLeaveComponent} from './components/employee-request-leave/employee-request-leave.component';
import {EmployeeLeaveDetailViewComponent} from './components/employee-leave-detail-view/employee-leave-detail-view.component';
import {EMPLOYEE_LEAVE_ROUTES} from './employee-leave.routes';
import {MatTooltipModule} from "@angular/material/tooltip";


@NgModule({
  declarations: [
      EmployeeLeaveComponent,
      EmployeeRequestLeaveComponent,
      EmployeeLeaveDetailViewComponent
  ],
    imports: [
        HrCommonModule,
        NicoComponentsModule,
        RouterModule.forChild(EMPLOYEE_LEAVE_ROUTES),
        TranslateModule.forChild({
            extend: true
        }),
        MatInputModule,
        NicoFileUploadModule,
        MatTooltipModule,
    ],
  providers: [
  ],
})
export class EmployeeLeaveModule { }
