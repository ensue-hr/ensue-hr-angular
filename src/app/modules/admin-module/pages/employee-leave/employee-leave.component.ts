import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {LeaveStatus} from '@common/enums/status.enums';
import {PaginatedNicoCollection} from '@system/utilities';
import {LeaveModel} from '@common/models/leave.model';
import {MatDialog} from '@angular/material/dialog';
import {LeaveService} from '@common/services/leave.service';
import { NicoStorageService} from '@system/services';
import {EmployeeLeaveDetailViewComponent} from './components/employee-leave-detail-view/employee-leave-detail-view.component';
import {EmployeeRequestLeaveComponent} from './components/employee-request-leave/employee-request-leave.component';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';
// @ts-ignore
import {StatusInterface} from '@common/interface/status.interface';

@Component({
  selector: 'app-employee-view',
  templateUrl: './employee-leave.component.html',
  styleUrls: ['./employee-leave.component.scss']
})
export class EmployeeLeaveComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  private leaveSubscription: Subscription;
  public leaveStatus = LeaveStatus;
  public models: PaginatedNicoCollection<LeaveModel>;
  public startAt = '';
  public endAt = '';
  public pageTitle = '';
  public isTableView: boolean;
  public userDetail: any;
  public leaveFor: string;
  public statusDropdownValues = [
    {
      status: this.translateService.instant('MOD_EMPLOYEES.STATUS_DROPDOWN_VALUES.ALL'),
      value: 'all'
    },
    {
      status: this.translateService.instant('MOD_LEAVE.PENDING_LABEL'),
      value: LeaveStatus.Pending
    },
    {
      status: this.translateService.instant('MOD_LEAVE.CANCELLED_LABEL'),
      value: LeaveStatus.Cancelled
    },
    {
      status: this.translateService.instant('MOD_LEAVE.APPROVED_LABEL'),
      value: LeaveStatus.Approved
    },
    {
      status: this.translateService.instant('MOD_LEAVE.DECLINED_LABEL'),
      value: LeaveStatus.Declined
    }
  ];

  public sortByDropdownValues = [

    {
      label: this.translateService.instant('MOD_LEAVE.MOD_LEAVE_REQUEST.TITLE_LABEL'),
      value: 'title'
    },
    {
      label: this.translateService.instant('MOD_LEAVE.MOD_LEAVE_REQUEST.START_AT_LABEL'),
      value: 'start_at'
    },
    {
      label: this.translateService.instant('MOD_LEAVE.MOD_LEAVE_REQUEST.END_AT_LABEL'),
      value: 'end_at'
    },
    {
      label: this.translateService.instant('MOD_EMPLOYEES.SORT_BY_DROPDOWN_VALUES.STATUS'),
      value: 'status'
    },
    {
      label: this.translateService.instant('MOD_EMPLOYEES.SORT_BY_DROPDOWN_VALUES.CREATED_AT'),
      value: 'created_at'
    },
  ];
  constructor(injector: Injector,
              public dialog: MatDialog,
              public leaveService: LeaveService,
              public storageService: NicoStorageService
  ) {
    super(injector);

  }

  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_LEAVE.PAGE_TITLE');
    super.onComponentReady();
    this.userDetail = JSON.parse(this.storageService.getItem('user_details'));
    this.filterQueryParams.status = LeaveStatus.Pending;
    this.setFilterHttpQueryParams();
    if (this.userDetail){
      this.getModels();
    }
    this.storageService.getItem('view') ? this.isTableView = this.storageService.getItem('view') === 'true' : this.isTableView = false;
  }


  public getModels(): void {
    if (this.userDetail?.id) {
      this.filterQueryParams.requested_to = this.userDetail?.id;
      this.setFilterHttpQueryParams();
    }
    this.leaveSubscription = this.leaveService.getLeaves(this.getFilterHttpQueryParams()).subscribe(models => {
      this.models = models;
    });
  }


  public onViewToggle($event): void {
    this.isTableView = $event;
  }

  public onSelectTableRow(model: LeaveModel): void {
    this.onViewLeave(model);
  }

  public onViewLeave(leave: LeaveModel): void{
    const modalDialog = this.dialog
      .open(EmployeeLeaveDetailViewComponent, {
        panelClass: ['modal-drawer-view', 'modal-xs'],
        disableClose: true,
        data: {leaveFor: this.leaveFor , leave}
      });
    modalDialog.afterClosed().subscribe(reload => {
      if (reload) {
        this.getModels();
      }
    });
  }

  public ngOnDestroy(): void {
    if (this.leaveSubscription != null){
      this.leaveSubscription.unsubscribe();
    }
  }

  // @ts-ignore
  public getPillData(status): StatusInterface {
    switch (status) {
        case LeaveStatus.Approved:
          return {
            label: this.translateService.instant('MOD_LEAVE.APPROVED_LABEL'),
            color: 'success'
          };

        case LeaveStatus.Pending:
          return {
            label: this.translateService.instant('MOD_LEAVE.PENDING_LABEL'),
            color: 'info'
          };

        case LeaveStatus.Cancelled:
          return {
            label: this.translateService.instant('MOD_LEAVE.CANCELLED_LABEL'),
            color: 'danger'
          };

        case LeaveStatus.Declined:
          return {
            label: this.translateService.instant('MOD_LEAVE.DECLINED_LABEL'),
            color: 'danger'
          };

      default:
        return {
          label: '',
          color: null
        };
    }
  }

  public requestLeave(): void {
    const modalDialog = this.dialog
      .open(EmployeeRequestLeaveComponent, {
        panelClass: ['modal-drawer-view', 'modal-xs'],
        disableClose: true,
        // data: this.
      });
    modalDialog.afterClosed().subscribe(reload => {
      if (reload) {
        this.getModels();
      }
    });
  }
}
