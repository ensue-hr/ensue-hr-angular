import { NgModule } from '@angular/core';
import {HrCommonModule} from '@common/hr-common.module';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ADMIN_ATTENDANCE_ROUTES} from './admin-attendance.routes';
import { AdminAttendanceDashboardComponent } from './components/admin-attendance-dashboard/admin-attendance-dashboard.component';
import { AdminAttendanceDashboardChartComponent } from './components/admin-attendance-dashboard/components/admin-attendance-dashboard-chart/admin-attendance-dashboard-chart.component';
import { AdminAttendanceDashboardListComponent } from './components/admin-attendance-dashboard/components/admin-attendance-dashboard-list/admin-attendance-dashboard-list.component';
import { AdminAttendanceReportComponent } from './components/admin-attendance-report/admin-attendance-report.component';
import { AdminAttendanceReportSummaryComponent } from './components/admin-attendance-report/components/admin-attendance-report-summary/admin-attendance-report-summary.component';
import { AdminAttendanceReportLineChartComponent } from './components/admin-attendance-report/components/admin-attendance-report-line-chart/admin-attendance-report-line-chart.component';
import { AdminEmployeeAttendanceRecordComponent } from './components/admin-attendance-report/components/admin-employee-attendance-record/admin-employee-attendance-record.component';
import { AdminAttendanceReportBarChartComponent } from './components/admin-attendance-report/components/admin-attendance-report-bar-chart/admin-attendance-report-bar-chart.component';
import { AdminAttendanceHistoryComponent } from './components/admin-attendance-history/admin-attendance-history.component';
import { AttendanceTableViewComponent } from './components/admin-attendance-history/components/attendance-table-view/attendance-table-view.component';
import {NicoComponentsModule} from "@system/components";

@NgModule({
  declarations: [
    AdminAttendanceDashboardComponent,
    AdminAttendanceDashboardChartComponent,
    AdminAttendanceDashboardListComponent,
    AdminAttendanceReportComponent,
    AdminAttendanceReportSummaryComponent,
    AdminAttendanceReportLineChartComponent,
    AdminEmployeeAttendanceRecordComponent,
    AdminAttendanceReportBarChartComponent,
    AdminAttendanceHistoryComponent,
    AttendanceTableViewComponent
  ],
    imports: [
        HrCommonModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(ADMIN_ATTENDANCE_ROUTES),
        TranslateModule.forChild({
            extend: true
        }),
        MatTooltipModule,
        NicoComponentsModule,
    ],
  providers: [],
})

export class AdminAttendanceModule { }
