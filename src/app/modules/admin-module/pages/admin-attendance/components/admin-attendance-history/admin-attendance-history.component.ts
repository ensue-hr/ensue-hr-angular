import {AfterViewInit, Component, Injector, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';
import {PaginatedNicoCollection} from '@system/utilities';
import {AttendanceModel, EmployeeModel, UserModel} from '@common/models';
import {AttendanceService, EmployeesService} from '@common/services';
import {MatDialog} from '@angular/material/dialog';
import {Subscription} from 'rxjs';
import {FormControl, FormGroup} from '@angular/forms';
import {DateUtility} from '@common/utilities/date-utility';
import {AdvancedSelectConfigInterface} from '@system/components';
import {environment} from '../../../../../../../environments/environment';
import {SearchNavEnum} from '@common/enums/searchNav.enum';

@Component({
  selector: 'app-admin-attendance-history',
  templateUrl: './admin-attendance-history.component.html',
  styleUrls: ['./admin-attendance-history.component.scss']
})
export class AdminAttendanceHistoryComponent extends AbstractBaseComponent implements OnInit, OnDestroy, AfterViewInit {

  /**
   * Page title
   */
  public pageTitle: string;

  /**
   * Models
   */
  public attendanceModel: PaginatedNicoCollection<AttendanceModel>;

  public employeeModel: UserModel;

  /**
   * Nico advanced select
   */
  public config: AdvancedSelectConfigInterface = { };

  @ViewChild('dropDownItemTemplate') dropDownItemTemplate: TemplateRef<any>;

  @ViewChild('dropDownEmptyTemplate') dropDownEmptyTemplate: TemplateRef<any>;

  /**
   * User model
   */
  public userModel: UserModel;

  public urlForAdvancedSelect = '';

  public env: any;

  /**
   * Subscription
   */
  public attendanceSubscription: Subscription;

  public employeeSubscription: Subscription;

  /**
   * Form group
   */
  public rangeForm: FormGroup;

  /**
   * Date picker
   */
  public startDate: any;

  public endDate: any;

  public maxDate: any;

  /**
   * Status dropdown values
   */
  public checkLeaveDropdownValues = [
    {
      status: this.translateService.instant('MOD_ADMIN_ATTENDANCE_HISTORY.ALL_LABEL'),
      value: SearchNavEnum.All
    },
    {
      status: this.translateService.instant('MOD_ADMIN_ATTENDANCE_HISTORY.ON_LEAVE_LABEL'),
      value: SearchNavEnum.OnLeave
    }
  ];

  /**
   * Sort by dropdown values
   */
  public sortByDropdownValues = [
    {
      label: this.translateService.instant('MOD_ADMIN_ATTENDANCE_HISTORY.ATTEND_DATE_LABEL'),
      value: SearchNavEnum.AttendDate
    },
    {
      label: this.translateService.instant('MOD_ADMIN_ATTENDANCE_HISTORY.CHECK_IN_LABEL'),
      value: SearchNavEnum.CheckIn
    }
  ];

  /**
   * Constructor
   */
  constructor(protected injector: Injector,
              public attendanceService: AttendanceService,
              public employeeService: EmployeesService,
              public dialog: MatDialog) {
    super(injector);
    this.maxDate = DateUtility.substractDate(1);
    this.startDate = DateUtility.getMomentDate().startOf('month').format('YYYY-MM-DD');
    this.endDate = DateUtility.getMomentDate().format('YYYY-MM-DD');
    this.pageTitle = this.translateService.instant('MOD_ADMIN_ATTENDANCE_HISTORY.PAGE_TITLE');
    this.env = environment;
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    super.onComponentReady();
    this.getModels();
    this.createForm();
    this.getEmployeeModel();
    this.urlForAdvancedSelect = this.env.api + 'employees';
  }

  /**
   * Create form
   */
  public createForm(): void {
    this.rangeForm = new FormGroup({
      startDate: new FormControl(this.startDate),
      endDate: new FormControl(this.endDate)
    });
  }

  /**
   * Get attendance data
   */
  public getModels(employeeId?: string): void {
    if (this.startDate && this.endDate && this.filterQueryParams) {
      this.filterQueryParams.start_date = this.startDate;
      this.filterQueryParams.end_date = this.endDate;
      this.filterQueryParams.employee_id = employeeId ? employeeId : '';

      this.setFilterHttpQueryParams();
      this.attendanceSubscription = this.attendanceService.get(this.filterHttpQueryParams).subscribe(calendarModel => {
        this.attendanceModel = calendarModel;
      });
    }
  }

  /**
   * Get employee model
   */
  public getEmployeeModel(): void {
    this.setFilterHttpQueryParams();
    this.employeeSubscription = this.employeeService.get(this.getFilterHttpQueryParams()).subscribe(models => {
      this.employeeModel = models;
    });
  }

  /**
   * On leave
   */
  public onStatusChange($event): void {
    this.publishStatusChange.next($event);
  }

  /**
   * Fetch models on date change
   */
  public onEndDateChange(): void {
    this.startDate = DateUtility.setDateFormat(this.rangeForm.get('startDate').value, 'YYYY-MM-DD');
    this.endDate = DateUtility.setDateFormat(this.rangeForm.get('endDate').value, 'YYYY-MM-DD');
    this.getModels();
  }

  /**
   * After view init
   */
  ngAfterViewInit(): void {
    this.config = {
      itemTemplate: this.dropDownItemTemplate,
      emptySelectionTemplate: this.dropDownEmptyTemplate,
      selectInfoLabel: this.translateService.instant('MOD_ADMIN_ATTENDANCE_HISTORY.EMPLOYEE_SELECT_LABEL'),
      equalityCheckProperty: 'id'
    };
  }

  /**
   * Upon clicking a table row
   */
  public onEmployeeSelect(employee: EmployeeModel): void {
    this.getModels(employee?.id);
  }

  /**
   * On destroy
   */
  public ngOnDestroy(): void {
    if (this.attendanceSubscription != null) {
      this.attendanceSubscription.unsubscribe();
    }
    if (this.employeeSubscription != null) {
      this.employeeSubscription.unsubscribe();
    }
  }

  /**
   * TODO: For view toggle between table view and calendar view
   */
  // public onViewToggle($event): void {
  // this.isTableView = $event;
  // }


}
