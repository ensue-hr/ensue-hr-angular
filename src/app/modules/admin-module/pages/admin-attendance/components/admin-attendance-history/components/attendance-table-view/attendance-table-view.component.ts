import {Component, Injector, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {AttendanceModel, UserModel} from '@common/models';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {DateUtility} from '@common/utilities/date-utility';
import {NicoUtils} from '@system/utilities';
import {TimesheetModalComponent} from '../../../../../attendance/components/staff-dashboard/components/calendar/components/timesheet-modal/timesheet-modal.component';
import {MatDialog} from '@angular/material/dialog';
import {AttendanceType} from '@common/enums/attendanceType.enum';

@Component({
  selector: 'app-attendance-table-view',
  templateUrl: './attendance-table-view.component.html',
  styleUrls: ['./attendance-table-view.component.scss']
})
export class AttendanceTableViewComponent extends AbstractBaseComponent implements OnInit, OnChanges, OnDestroy {

  @Input() attendanceModel: AttendanceModel[];

  @Input() employeeModel: UserModel;

  /**
   * Constructor
   */
  constructor(protected injector: Injector, public dialog: MatDialog) {
    super(injector);
    this.pageTitle = this.translateService.instant('MOD_ADMIN_ATTENDANCE_HISTORY.PAGE_TITLE');
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    super.onComponentReady();
  }

  public ngOnChanges(changes: SimpleChanges): void {
  }

  /**
   * Convert to date format hh:mm
   */
  public convertDateToHoursAndMinutes(time?: string, type?: string): any {
    const convertedTime = DateUtility.setDateFormat(time, 'H:mm');
    const workStartTime = JSON.parse(this.userSettingsItems)?.employee_work_time?.split('-')[0];
    const workEndTime = JSON.parse(this.userSettingsItems)?.employee_work_time?.split('-')[1];
    if (!NicoUtils.isNullOrUndefined(time)) {
      switch (type) {
        case AttendanceType.CheckIn: {
          if (new Date(0, 0, 0, Number(convertedTime.split(':')[0]), Number(convertedTime.split(':')[1])) > new Date(0, 0, 0, workStartTime.split(':')[0], workStartTime.split(':')[1])) {
            return {label: DateUtility.utcToLocalFormat(time, 'H:mm'), color: 'warning-color'};
          } else {
            return {label: DateUtility.utcToLocalFormat(time, 'H:mm'), color: 'primary-color'};
          }
        }
        case AttendanceType.CheckOut: {
          if (new Date(0, 0, 0, Number(convertedTime.split(':')[0]), Number(convertedTime.split(':')[1])) < new Date(0, 0, 0, workEndTime.split(':')[0], workEndTime.split(':')[1])) {
            return {label: DateUtility.utcToLocalFormat(time, 'H:mm'), color: 'warning-color'};
          } else {
            return {label: DateUtility.utcToLocalFormat(time, 'H:mm'), color: 'primary-color'};
          }
        }
      }
    }
    return {label: '-', color: 'display-none'};
  }

  /**
   * Convert to date format YYYY-MM-DD
   */
  public convertDateTimeToDate(time: string): string {
    return DateUtility.setDateFormat(time, 'YYYY-MM-DD');
  }

  /**
   * On table row click
   */
  public onTableRowClick(model: AttendanceModel): void {
    if (this.attendanceModel?.length > 0) {
      this.dialog.open(TimesheetModalComponent, {
        data: {
          employeeId: model?.employee_id,
          selectedDate: model?.attend_date,
          workTime: model?.work_time,
          breakTime: model?.break_time,
          workTimeCount: ''
        },
        panelClass: ['modal-lg'],
        disableClose: true
      });
    }
  }

  /**
   * Assign color class to work time
   */
  public assignWorkTimeColor(time: number): string {
    const workTime = Number(JSON.parse(this.userSettingsItems).employee_working_hours) * 3600;
    if (Number(time) > workTime) {
      return '';
    }
    return 'warning-color';
  }

}
