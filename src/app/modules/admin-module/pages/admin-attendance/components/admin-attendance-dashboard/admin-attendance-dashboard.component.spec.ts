import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAttendanceDashboardComponent } from './admin-attendance-dashboard.component';
import {
  TranslateModule

} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';

describe('AdminAttendanceDashboardComponent', () => {
  let component: AdminAttendanceDashboardComponent;
  let fixture: ComponentFixture<AdminAttendanceDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAttendanceDashboardComponent ],
      providers: [
        ...MOCK_PROVIDERS,
      ],
      imports: [
        RouterTestingModule,
        TranslateModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAttendanceDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set page title to dashboard', () => {
    expect(component.pageTitle).toEqual('MOD_ATTENDANCE.MOD_ADMIN_ATTENDANCE.PAGE_TITLE');
  });
});
