import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../AbstractBaseComponent';

@Component({
  selector: 'app-admin-attendance-dashboard',
  templateUrl: './admin-attendance-dashboard.component.html',
  styleUrls: ['./admin-attendance-dashboard.component.scss']
})
export class AdminAttendanceDashboardComponent extends AbstractBaseComponent implements OnDestroy {

  public pageTitle = '';
  constructor(
    injector: Injector) {
    super(injector);
  }

  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_ATTENDANCE.MOD_ADMIN_ATTENDANCE.PAGE_TITLE');
    super.onComponentReady();
  }

  public ngOnDestroy(): void {
  }

}
