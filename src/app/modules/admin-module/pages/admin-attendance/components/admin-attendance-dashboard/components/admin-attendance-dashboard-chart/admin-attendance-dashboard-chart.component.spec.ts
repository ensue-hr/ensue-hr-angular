import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAttendanceDashboardChartComponent } from './admin-attendance-dashboard-chart.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {AttendanceReportModel} from '@common/models/attendance-report.model';

describe('AdminAttendanceDashboardChartComponent', () => {
  let component: AdminAttendanceDashboardChartComponent;
  let fixture: ComponentFixture<AdminAttendanceDashboardChartComponent>;
  const dummy = {
    absent: 10,
    employees: 10,
    late_login: 0,
    leaves: 0,
    on_time_login: 3,
    present: 5,
  } as unknown as AttendanceReportModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAttendanceDashboardChartComponent ],
      providers: [
        ...MOCK_PROVIDERS,
      ],
      imports: [
        RouterTestingModule,
        TranslateModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAttendanceDashboardChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get the model on init', () => {
    spyOn(component, 'getModels');
    component.ngOnInit();
    expect(component.getModels).toHaveBeenCalled();
  });

  it('should call get  chart data on init', () => {
      spyOn(component, 'getChartData');
      component.todaySummary = dummy ;
      fixture.detectChanges();
      component.ngOnInit();
      expect(component.getChartData).toHaveBeenCalledWith('late_login' || 'on_time_login');
  });

  it('should return calculated percent value', () => {
    component.todaySummary = dummy ;
    fixture.detectChanges();
    const returnValue = component.getChartData('on_time_login');
    expect(returnValue).toBe(60);
  });
});
