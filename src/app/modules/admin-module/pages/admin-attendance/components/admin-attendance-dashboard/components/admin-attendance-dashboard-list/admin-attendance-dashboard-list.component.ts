import { Component, Injector, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AttendanceReportService } from '@common/services/attendance-report.service';
import { AbstractBaseComponent } from '../../../../../../../../AbstractBaseComponent';
import { DateUtility } from '@common/utilities/date-utility';
import { PaginatedNicoCollection } from '@system/utilities';
import * as moment from 'moment';
import { map } from 'rxjs/operators';
import { AttendanceReportModel } from '@common/models/attendance-report.model';

@Component({
  selector: 'app-admin-attendance-dashboard-list',
  templateUrl: './admin-attendance-dashboard-list.component.html',
  styleUrls: ['./admin-attendance-dashboard-list.component.scss'],
})
export class AdminAttendanceDashboardListComponent
  extends AbstractBaseComponent
  implements OnDestroy
{
  public modelSubscription: Subscription;
  public todayAttendance: PaginatedNicoCollection<AttendanceReportModel>;
  public showTimeSpentHours: string;
  public noValue = 0;
  public searchNavConfig = {
    enableSearchInput: true,
    enableToggle: false,
    enableSortOrder: false,
    enableSortBy: true,
    enableStatus: false,
  };

  public sortByDropdownValues = [
    {
      label: this.translateService.instant(
        'MOD_EMPLOYEES.SORT_BY_DROPDOWN_VALUES.CREATED_AT'
      ),
      value: 'created_at',
    },
  ];

  constructor(
    injector: Injector,
    public attendanceService: AttendanceReportService
  ) {
    super(injector);
  }

  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant(
      'MOD_ATTENDANCE.MOD_ADMIN_ATTENDANCE.PAGE_TITLE'
    );
    super.onComponentReady();
    this.getModels();
  }

  public getModels(): void {
    this.modelSubscription = this.attendanceService
      .getAttendanceTodayAttendance(this.getFilterHttpQueryParams())
      .pipe(
        map((res) => {
          if (res && res.length > 0) {
            return this.getSortData(res.all());
          }
        })
      )
      .subscribe((attendance) => {
        this.todayAttendance = attendance;
      });
  }

  public ngOnDestroy(): void {
    if (this.modelSubscription !== null) {
      this.modelSubscription.unsubscribe();
    }
  }

  public workHourToPercent(
    seconds?: any,
    dateTime?: any,
    breakTime?: any,
    checkoutTime?: any
  ): any {
    if (seconds !== null && this.todayAttendance) {
      if (checkoutTime !== null) {
        return this.withCheckoutTime(seconds);
      } else {
        return this.withOutCheckoutTime(dateTime, breakTime);
      }
    } else {
      return this.noValue;
    }
  }

  public withOutCheckoutTime(checkInTime: number, breakTime?: number): number {
    if (checkInTime !== null) {
      const today = new Date();
      const currentDateTime = new Date(DateUtility.localToUTCFormat(today));
      const convertToUnixSecond = moment(currentDateTime).unix();
      const seconds = Math.abs(
        convertToUnixSecond - moment(checkInTime).unix() - breakTime
      );
      this.showTimeSpentHours = (seconds / 3600).toFixed(1);
      const hr = seconds / (8 * 3600);
      if (seconds / 3600 > 8) {
        return 100;
      }
      return hr * 100;
    } else {
      return this.noValue;
    }
  }

  public withCheckoutTime(seconds: number): string {
    const sec = seconds / (8 * 3600);
    this.showTimeSpentHours = (seconds / 3600).toFixed(1);
    if (seconds / 3600 > 8) {
      return '100';
    }
    const hr = sec * 100;
    return hr.toFixed(1);
  }

  public getSortData(data): any {
    const sortedData = [];
    data.filter((res) => {
      if (res.leave_in !== null) {
        sortedData.push(res);
      }
    });
    data.filter((res) => {
      if (res.check_in !== null) {
        sortedData.push(res);
      }
    });
    data.filter((res) => {
      if (res.check_in === null && res.leave_in === null) {
        sortedData.push(res);
      }
    });
    return sortedData;
  }

  public getEndPointValue(value: number): string {
    if (value) {
      return moment(value).format('YYYY-MM-DD HH:mm:ss');
    } else {
      return '';
    }
  }

  public getLocalDateTimeValue(value: string): string {
    return `${DateUtility.utcToLocalFormat(
      DateUtility.getUtcDateTime(value?.split('-')[0], 'HH:mm'),
      'HH:mm'
    )}
     - ${DateUtility.utcToLocalFormat(
       DateUtility.getUtcDateTime(value?.split('-')[1], 'HH:mm'),
       'HH:mm'
     )}`;
  }
}
