import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAttendanceReportSummaryComponent } from './admin-attendance-report-summary.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA, SimpleChanges} from '@angular/core';
import {AttendanceReportModel} from '@common/models/attendance-report.model';

describe('AdminAttendanceReportSummaryComponent', () => {
  let component: AdminAttendanceReportSummaryComponent;
  let fixture: ComponentFixture<AdminAttendanceReportSummaryComponent>;

  const mockData = {
    absent_days: 0,
    avg_break_time: '3823.6364',
    avg_check_in: '2021-10-17 07:00:35',
    avg_check_out: '2021-10-17 04:14:40',
    avg_office_time: '33804.5455',
    avg_work_time: '29980.9091',
    employee: {id: 1, name: 'Manish Ratna Shakya',
      email: 'manish.shakya@ensue.us',
    avatar: 'https://static.toiimg.com/thumb/resizemode-4,msid-76729750,imgsize-249247,width-720/76729750.jpg'},
    email: 'manish.shakya@ensue.us',
    id: 1,
    name: 'Manish Ratna Shakya',
    position: 'Junior frontend developer',
    employee_id: 1,
    leave_days: '0',
    present_days: '22',
    total_break_time: '84120',
    total_office_time: '743700',
    total_work_time: '659580',
    working_days: 21
  } as unknown as AttendanceReportModel;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAttendanceReportSummaryComponent ],
      providers: [
        ...MOCK_PROVIDERS,
      ],
      imports: [
        RouterTestingModule,
        TranslateModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAttendanceReportSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // afterEach(() => {
  //   spyOn(component, 'ngOnDestroy').and.callFake(() => { });
  //   fixture.destroy();
  // });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get model on init ', () => {
    spyOn(component, 'getModels');
    component.onComponentReady();
    expect(component.getModels).toHaveBeenCalled();
  });

  it('should call get model on change date values', () => {
    spyOn(component, 'getModels');
    component.ngOnChanges(mockData as unknown as SimpleChanges);
    expect(component.getModels).toHaveBeenCalled();
  });

  it('should set color as per data', () => {
    component.ngOnInit();
    const data = component.setLateCheckInColor(mockData.avg_check_in, 33300);
    expect(data).toBeTruthy();
  });
});
