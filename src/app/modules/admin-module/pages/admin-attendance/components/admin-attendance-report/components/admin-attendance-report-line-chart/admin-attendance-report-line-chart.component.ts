import {Component, Injector, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {AttendanceReportService} from '@common/services/attendance-report.service';
import {AttendanceReportModel} from '@common/models/attendance-report.model';
import {Subscription} from 'rxjs';
import * as moment from 'moment';
import {DateUtility} from '@common/utilities/date-utility';
import {ChartLegendInterface} from '@common/interface/chart-legend.interface';


@Component({
  selector: 'app-admin-attendance-report-line-chart',
  templateUrl: './admin-attendance-report-line-chart.component.html',
  styleUrls: ['./admin-attendance-report-line-chart.component.scss']
})
export class AdminAttendanceReportLineChartComponent extends AbstractBaseComponent implements OnInit, OnChanges, OnDestroy {

  @Input() startDate: string;
  @Input() endDate: string;
  @Input() startThreshold: number;
  @Input() endThreshold: number;


  public chartSubscription: Subscription;
  public models: AttendanceReportModel;
  public chartOption: any;
  public chartInterval: number;
  public yAxisStartPoint: number;
  public lateColor: string;
  public onTimeColor: string;

  constructor(injector: Injector, public attendanceReportService: AttendanceReportService) {
    super(injector);
    this.chartInterval = 3600;
    this.yAxisStartPoint = 25200;
    this.lateColor = '#ff5432';
    this.onTimeColor = '#855CF8';
  }

  public onComponentReady(): void {
    super.onComponentReady();
    this.getModels();
  }

  public getModels(): void {
    if (this.endDate && this.startDate && this.filterQueryParams) {
      this.filterQueryParams.start_date = this.startDate;
      this.filterQueryParams.end_date = this.endDate;
      this.setFilterHttpQueryParams();
      this.chartSubscription = this.attendanceReportService.getChartData(this.getFilterHttpQueryParams()).subscribe(models => {
        this.models = models;
        this.setChartValues(this.models);
      });
    }
  }


  public ngOnChanges(changes: SimpleChanges): void {
    this.endDate = changes.endDate?.currentValue;
    if (changes.startDate) {
      this.startDate = changes.startDate?.currentValue;
    }
    this.getModels();
  }

  public setChartValues(models: any): void {
    const xAxisData = [];
    const checkIn = [];
    const checkOut = [];
    for (const data of models?.data) {
      xAxisData.push(data.date);
      if (data.avg_check_in) {
        const checkInData = DateUtility.utcToLocalFormat(moment(new Date(Date.UTC(0, 0, 0,
          Number(data.avg_check_in.split(':')[0]), Number(data.avg_check_in.split(':')[1]),
          Number(data.avg_check_in.split(':')[2])))), 'HH:mm:ss: A');
        checkIn.push(moment(checkInData, 'HH:mm:ss: A').diff(moment().startOf('day'), 'seconds'));
      } else {
        checkIn.push(0);
      }
      if (data.avg_check_out) {
        const checkOutData = DateUtility.utcToLocalFormat(moment(new Date(Date.UTC(0, 0, 0,
          Number(data.avg_check_out.split(':')[0]), Number(data.avg_check_out.split(':')[1]),
          Number(data.avg_check_out.split(':')[2])))), 'HH:mm:ss: A');
        checkOut.push(moment(checkOutData, 'HH:mm:ss: A').diff(moment().startOf('day'), 'seconds'));
      } else {
        checkOut.push(0);
      }
    }
    this.chartOption = {
      grid: {
        left: 0,
        right: 0,
        containLabel: true,
      },
      lineStyle: {
        width: 0.5,
      },
      tooltip: {
        trigger: 'axis',
        formatter: (point: any) => {
          return ` ${this.translateService.instant('MOD_ATTENDANCE_REPORT.DATE_LABEL')}: ${point[0].axisValue}
              <br/>${point[0].marker} ${this.translateService.instant('MOD_ATTENDANCE_REPORT.AVG_CHECK_IN_LABEL')}:  ${point[0].value !== 0 ? moment.utc(point[0].value * 1000).format('HH:mm') : ' __:__'}
              <br/> ${point[1].marker} ${this.translateService.instant('MOD_ATTENDANCE_REPORT.AVG_CHECK_OUT_LABEL')}:  ${point[1].value !== 0 ? moment.utc(point[1].value * 1000).format('HH:mm') : ' __:__'}`;
        }
      },
      xAxis: {
        data: xAxisData,
        boundaryGap: true,
        axisLabel: {
          formatter: (value) => {
            return value.split('-')[2];
          }
        }
      },
      yAxis: {
        type: 'value',
        min: this.yAxisStartPoint,
        maxInterval: this.chartInterval,
        axisLabel: {
          formatter: (value) => {
            return moment.utc(value * 1000).format('HH:mm');
          }
        }
      },
      series: this.attendanceReportService.getSeriesData(checkIn, checkOut, this.startThreshold, this.endThreshold),
      animationDelayUpdate: (idx) => idx * 5,
    };
  }

  public setChartLegend(): ChartLegendInterface[] {
    return [
      {marker: this.onTimeColor, title: this.translateService.instant('MOD_ATTENDANCE_REPORT.ON_TIME_LABEL')},
      {marker: this.lateColor, title: this.translateService.instant('MOD_ATTENDANCE_REPORT.LATE_LABEL')},
    ];
  }

  public ngOnDestroy(): void {
    if (this.chartSubscription !== null) {
      this.chartSubscription.unsubscribe();
    }
  }
}
