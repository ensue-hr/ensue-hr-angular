import {Component, Injector, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import { AttendanceReportService } from '@common/services/attendance-report.service';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {Subscription} from 'rxjs';
import {AttendanceReportModel} from '@common/models/attendance-report.model';
import {ChartLegendInterface} from '@common/interface/chart-legend.interface';

@Component({
  selector: 'app-admin-attendance-report-bar-chart',
  templateUrl: './admin-attendance-report-bar-chart.component.html',
  styleUrls: ['./admin-attendance-report-bar-chart.component.scss']
})
export class AdminAttendanceReportBarChartComponent extends AbstractBaseComponent implements OnInit, OnChanges, OnDestroy {

  @Input() startDate: string;
  @Input() endDate: string;
  public chartOption: any;
  public model: AttendanceReportModel;
  public modelSubscription: Subscription;
  public presentColor: string;
  public weekendColor: string;
  public holidayColor: string;
  public hourSeconds: number;

  constructor(injector: Injector, public attendanceReportService: AttendanceReportService) {
    super(injector);
    this.hourSeconds = 3600;
    this.presentColor = '#32A321';
    this.weekendColor = '#9EABB7';
    this.holidayColor = '#E84118';
  }

  public onComponentReady(): void {
    super.onComponentReady();
    this.getModels();
  }

  public getModels(): void {
    if (this.startDate && this.endDate && this.filterQueryParams) {
      this.filterQueryParams.start_date = this.startDate;
      this.filterQueryParams.end_date = this.endDate;
      this.setFilterHttpQueryParams();
      this.modelSubscription = this.attendanceReportService.getChartData(this.getFilterHttpQueryParams()).subscribe(models => {
        this.model = models;
        this.setChartData();
      });
    }
  }

  public setChartData(): void {
    const xAxisData = [];
    for (const data of this.model?.data) {
      xAxisData.push(data.date);
    }
    this.chartOption = {
      grid: {
        left: 0,
        right: 0,
        containLabel: true
      },
      tooltip: {
        trigger: 'axis',
        formatter: (bar: any) => {
          const toolValue = `${this.translateService.instant('MOD_ATTENDANCE_REPORT.DATE_LABEL')}: ${bar[0].axisValue} <br/> ${bar[0].marker} ${bar[0].name}`;
          if (bar[0].name === 'Weekend' || bar[0].name === 'Holiday') {
            return toolValue;
          }
          return `${toolValue} : ${(bar[0].value / this.hourSeconds).toFixed(1)} ${this.translateService.instant('MOD_ATTENDANCE_REPORT.HOURS_LABEL')}`;
        }
      },
      xAxis: {
        data: xAxisData,
        axisPointer: {
          type: 'none'
        },
        boundaryGap: true,
        axisLabel: {
          formatter: (value) => {
            return value.split('-')[2];
          }
        }
      },
      yAxis: {
        type: 'value',
        min: 0,
        maxInterval: this.hourSeconds,
        axisLabel: {
          formatter: (value) => {
            return (value / this.hourSeconds).toFixed() + ' hour';
          }
        }
      },
      series: this.attendanceReportService.getSeriesBarData(this.model?.data),
      animationDelayUpdate: (idx) => idx * 5,
    };
  }

  public ngOnChanges(changes: SimpleChanges): void {
    this.endDate = changes.endDate?.currentValue;
    if (changes?.startDate) {
      this.startDate = changes.startDate?.currentValue;
    }
    this.getModels();
  }

  public setChartLegend(): ChartLegendInterface[] {
    return [
      {marker: this.presentColor, title: this.translateService.instant('MOD_ATTENDANCE_REPORT.PRESENT_LABEL')},
      {marker: this.weekendColor, title: this.translateService.instant('MOD_ATTENDANCE_REPORT.WEEKEND_LABEL')},
      {marker: this.holidayColor, title: this.translateService.instant('MOD_ATTENDANCE_REPORT.ABSENT_LABEL')},
    ];
  }

  public ngOnDestroy(): void {
    if (this.modelSubscription !== null) {
      this.modelSubscription.unsubscribe();
    }
  }

}
