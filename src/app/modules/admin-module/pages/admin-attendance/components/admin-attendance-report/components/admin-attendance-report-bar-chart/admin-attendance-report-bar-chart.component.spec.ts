import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAttendanceReportBarChartComponent } from './admin-attendance-report-bar-chart.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('AdminAttendanceReportBarChartComponent', () => {
  let component: AdminAttendanceReportBarChartComponent;
  let fixture: ComponentFixture<AdminAttendanceReportBarChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAttendanceReportBarChartComponent ],
      providers: [
        ...MOCK_PROVIDERS,
      ],
      imports: [
        RouterTestingModule,
        TranslateModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAttendanceReportBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call model on init', () => {
    spyOn(component, 'getModels');
    component.onComponentReady();
    expect(component.getModels).toHaveBeenCalledTimes(1);
  });

  it('should initialize start and end date on component init', () => {
    const start = component.startDate;
    const end = component.startDate;
    expect(start).not.toBeNull();
    expect(end).not.toBeNull();
  });

});
