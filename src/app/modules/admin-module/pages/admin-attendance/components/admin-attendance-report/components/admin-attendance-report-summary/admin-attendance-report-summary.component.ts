import {Component, Injector, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {DateUtility} from '@common/utilities/date-utility';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {AttendanceReportService} from '@common/services/attendance-report.service';
import {AttendanceReportModel} from '@common/models/attendance-report.model';
import {Subscription} from 'rxjs';
import {environment} from '../../../../../../../../../environments/environment';
import {NicoStorageService} from '@system/services';
import * as moment from 'moment';

@Component({
  selector: 'app-admin-attendance-report-summary',
  templateUrl: './admin-attendance-report-summary.component.html',
  styleUrls: ['./admin-attendance-report-summary.component.scss']
})
export class AdminAttendanceReportSummaryComponent extends AbstractBaseComponent implements OnInit, OnDestroy, OnChanges {

  @Input() startDate: string;
  @Input() endDate: string;
  @Input() startThreshold: number;
  @Input() endThreshold: number;
  public models: AttendanceReportModel;
  public reportSubscription: Subscription;
  public api: string;
  public exportLabel: string;
  public env = environment;
  public accessToken: string;
  constructor(injector: Injector,
              public attendanceReportService: AttendanceReportService,
              public nicoStorageService: NicoStorageService) {
    super(injector);
    this.api = this.env.api || '';
    this.getStorageData();
  }

  public getStorageData(): void {
    this.accessToken = this.nicoStorageService.getItem('access_token');
  }

  public onComponentReady(): void {
    super.onComponentReady();
    this.getModels();
    this.exportLabel = this.translateService.instant('MOD_ATTENDANCE_REPORT.EXPORT_LABEL');
  }


  public getModels(): void {
    if (this.startDate && this.endDate && this.filterQueryParams) {
      this.filterQueryParams.start_date = this.startDate;
      this.filterQueryParams.end_date = this.endDate;
      this.setFilterHttpQueryParams();
      this.reportSubscription = this.attendanceReportService.getReportSummary(this.getFilterHttpQueryParams()).subscribe(models => {
        this.models = models;
      });
    }
  }


  public getConvertedDate(utc: string): any {
    return DateUtility.utcToLocalFormat(moment(new Date(Date.UTC(0, 0, 0, Number(utc.split(':')[0]), Number(utc.split(':')[1])))), 'HH:mm')
  }

  public setLateCheckInColor(date, threshold?: number): any {
    const utc = DateUtility.utcToLocalFormat(date, 'HH:mm:ss');
    return threshold < DateUtility.timeToSeconds(utc);
  }
  public setLateCheckOutColor(date, threshold?: number): any {
    const utc = DateUtility.utcToLocalFormat(date, 'HH:mm:ss');
    return threshold > DateUtility.timeToSeconds(utc);
  }
  public getSecondsToHours(seconds: number): string {
     return (seconds / 3600).toFixed();
  }

  public ngOnChanges(changes: SimpleChanges): void {
    this.endDate = changes.endDate?.currentValue;
    if (changes.startDate){
      this.startDate = changes.startDate?.currentValue;
    }
    this.api = `${this.env.api}attendances/reports/summary/excel?start_date=${this.startDate}&end_date=${this.endDate}&access_token=${this.accessToken}`;
    this.getModels();
  }
  public ngOnDestroy(): void {
    if (this.reportSubscription !== null) {
      this.reportSubscription.unsubscribe();
    }
  }

}
