import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AdminAttendanceReportComponent } from './admin-attendance-report.component';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateModule} from '@ngx-translate/core';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {NicoStorageService} from '@system/services';
import {MockNicoStorageService} from '@common/testing-resources/mock-services';

describe('AdminAttendanceReportComponent', () => {
  let component: AdminAttendanceReportComponent;
  let fixture: ComponentFixture<AdminAttendanceReportComponent>;
  let storageService: NicoStorageService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAttendanceReportComponent ],
      providers: [
        {provide: NicoStorageService, useClass: MockNicoStorageService},
        ...MOCK_PROVIDERS,
      ],
      imports: [
        RouterTestingModule,
        TranslateModule,
        ReactiveFormsModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    storageService = TestBed.inject(NicoStorageService);
    storageService.setItem('settings',  JSON.stringify({employee_work_time: '9:00-15:00'}));
    fixture = TestBed.createComponent(AdminAttendanceReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create form on init', () => {
    spyOn(component, 'createForm');
    component.onComponentReady();
    expect(component.createForm).toHaveBeenCalled();
  });

  it('should have start date and end date on init', () => {
    component.ngOnInit();
    expect(component.startDate).not.toBeNull();
    expect(component.endDate).not.toBeNull();
  });

  it('should set page title on init', () => {
    component.ngOnInit();
    expect(component.pageTitle).toEqual('MOD_ATTENDANCE_REPORT.PAGE_TITLE');
  });

});
