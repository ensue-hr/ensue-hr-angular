import {Component, Injector, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {AbstractBaseComponent} from '../../../../../../../../AbstractBaseComponent';
import {AttendanceReportModel} from '@common/models/attendance-report.model';
import {Subscription} from 'rxjs';
import { AttendanceReportService } from '@common/services/attendance-report.service';
import {AttendStatusEnum} from '@common/enums/attendStatus.enum';
import {ChartLegendInterface} from '@common/interface/chart-legend.interface';

@Component({
  selector: 'app-admin-employee-attendance-record',
  templateUrl: './admin-employee-attendance-record.component.html',
  styleUrls: ['./admin-employee-attendance-record.component.scss']
})
export class AdminEmployeeAttendanceRecordComponent extends AbstractBaseComponent implements OnInit, OnChanges {

  @Input() startDate: string;
  @Input() endDate: string;

  public tableData: Array<any>;
  public models: any;
  public modelSubscription: Subscription;
  public presentColor: string;
  public absentColor: string;
  public weekendColor: string;
  public holidayColor: string;
  public halfLeaveColor: string;
  public leaveColor: string;

  constructor(injector: Injector, public attendanceReportService: AttendanceReportService) {
    super(injector);
    this.tableData = [];
    this.presentColor = '';
    this.weekendColor = '#9EABB7';
    this.absentColor = '#E84118';
    this.halfLeaveColor = '#FBC531';
    this.holidayColor = '#E84118';
    this.leaveColor = '#FBC531';
  }

  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_ATTENDANCE_REPORT.PAGE_TITLE');
    super.onComponentReady();
    this.getModels();

  }

  public getModels(): void {
    if (this.startDate && this.endDate && this.filterQueryParams) {
      this.filterQueryParams.start_date = this.startDate;
      this.filterQueryParams.end_date = this.endDate;
      this.setFilterHttpQueryParams();
      this.modelSubscription = this.attendanceReportService.
      getEmployeeAttendanceReport(this.getFilterHttpQueryParams()).subscribe((model: AttendanceReportModel) => {
        this.models = model.data;
        this.setTableDate();
      });
    }
  }


  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.endDate) {
      this.endDate = changes.endDate.currentValue;
    }
    if (changes.startDate) {
      this.startDate = changes.startDate.currentValue;
    }
    this.getModels();
  }

  public setTableDate(): void {
    this.tableData = [];
    this.tableData.push(' ');
    if (this.models) {
        for (const date of this.models[0]?.attendances){
          this.tableData.push(date.date);
        }
    }
  }

  public getAttendanceState(attendance: any): string {
    if (attendance.leave && attendance.present) {
      return AttendStatusEnum.HalfLeave;
    }
    if (attendance.holiday ) {
      return AttendStatusEnum.Holiday;
    }
    if (attendance.present ) {
      return AttendStatusEnum.Present;
    }
    if (attendance.leave) {
      return AttendStatusEnum.Leave;
    }
    if (attendance.weekend ) {
      return AttendStatusEnum.Weekend;
    }
    return AttendStatusEnum.Absent;
    }


    public setTableColor(attendance): string {
      switch (this.getAttendanceState(attendance)){
          case AttendStatusEnum.Holiday:
            return 'holidays';
          case AttendStatusEnum.Present:
            return 'present';
          case AttendStatusEnum.Leave:
            return 'leave';
          case AttendStatusEnum.Weekend:
            return 'weekend';
          case AttendStatusEnum.Absent:
            return 'absent';
          case AttendStatusEnum.HalfLeave:
            return 'half-leave';
          default:
            return '';
      }
    }

  public setChartLegend(): ChartLegendInterface[] {
    return [
      {marker: this.presentColor, value: AttendStatusEnum.Present , title: this.translateService.instant('MOD_ATTENDANCE_REPORT.PRESENT_LABEL')},
      {marker: this.absentColor, value: AttendStatusEnum.Absent , title: this.translateService.instant('MOD_ATTENDANCE_REPORT.ABSENT_LABEL')},
      {marker: this.leaveColor, value: AttendStatusEnum.Leave , title: this.translateService.instant('MOD_ATTENDANCE_REPORT.LEAVE_LABEL')},
      {marker: this.holidayColor, value: AttendStatusEnum.Holiday , title: this.translateService.instant('MOD_ATTENDANCE_REPORT.HOLIDAY_LABEL')},
      {marker: this.weekendColor, value: AttendStatusEnum.Weekend , title: this.translateService.instant('MOD_ATTENDANCE_REPORT.WEEKEND_LABEL')},
      {marker: this.halfLeaveColor, value: AttendStatusEnum.HalfLeave , title: this.translateService.instant('MOD_ATTENDANCE_REPORT.HALF_LEAVE_LABEL')},
    ];
  }
}
