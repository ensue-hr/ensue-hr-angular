import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';

import { ChangePasswordFirstloginComponent } from './change-password-firstlogin.component';
import {AjaxSpinnerService, NicoPageTitle, NicoSessionService, NicoStorageService} from '@system/services';
import {
  MockMatDialog,
  MockMatSnackBar, MockMissingTranslationHandler,
  MockNicoHttpClient,
  MockNicoPageTitle,
  MockNicoSessionService, MockNicoStorageService, MockUserInfoService
} from '@common/testing-resources/mock-services';
import {NicoHttpClient} from '@system/http-client';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {AppService, UserService} from '@common/services';
import {
  DEFAULT_LANGUAGE,
  MissingTranslationHandler,
  TranslateCompiler, TranslateFakeCompiler,
  TranslateFakeLoader,
  TranslateLoader, TranslateModule, TranslateParser,
  TranslateService,
  TranslateStore, USE_DEFAULT_LANG, USE_EXTEND, USE_STORE
} from '@ngx-translate/core';
import {PasswordEmailService} from '@common/services/password-email.service';
import {of, throwError} from 'rxjs';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('ChangePasswordFirstloginComponent', () => {
  let component: ChangePasswordFirstloginComponent;
  let fixture: ComponentFixture<ChangePasswordFirstloginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangePasswordFirstloginComponent ],
      providers: [
        ...MOCK_PROVIDERS,
        {
          provide: MatDialogRef, useValue: {
            close(params?: any): any {
              return;
            }
          }
        },
        {
          provide: PasswordEmailService, useValue: {
            post(params?: any): any {
              return of({});
            }
          }
        }
      ],
      imports: [TranslateModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePasswordFirstloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set page title', () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_ACCOUNT.MOD_CHANGE_PASSWORD.HTML_TITLE');
  });

  it('should initialize form', () => {
    spyOn(component, 'createForm');
    component.ngOnInit();
    expect(component.createForm).toHaveBeenCalled();
  });

  it('should not show success snack bar in case of error', fakeAsync(async () => {
    // @ts-ignore
    spyOn(component, 'showSuccessSnackBar').and.returnValue(throwError({error: {status: 417}}));
    component.saveChangedPassword();
    expect(component.showSuccessSnackBar).not.toHaveBeenCalled();
    flush();
  }));

  it('should not show error message in case of success', fakeAsync(async () => {
    spyOn(component, 'onFormSubmissionError').and.returnValue();
    component.saveChangedPassword();
    expect(component.onFormSubmissionError).not.toHaveBeenCalled();
  }));

});
