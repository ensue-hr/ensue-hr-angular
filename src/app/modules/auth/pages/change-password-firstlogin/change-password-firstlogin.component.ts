import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthUserChangePasswordService} from '@common/services';
import {Subscription} from 'rxjs';
import {HrValidators} from '@common/utilities';

@Component({
  selector: 'app-change-password-firstlogin',
  templateUrl: './change-password-firstlogin.component.html',
})
export class ChangePasswordFirstloginComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Page title
   */

  public pageTitle = '';

  /**
   * Form
   */
  public changePasswordFormGroup: FormGroup;

  public changePasswordFormId = 'change-pass-form';

  /**
   * Subscription
   */
  public changePasswordSubscription: Subscription;

  /**
   * Constructor
   */
  constructor(
    protected injector: Injector,
    public formBuilder: FormBuilder,
    public changePasswordService: AuthUserChangePasswordService,
  ) {
    super(injector);
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_ACCOUNT.MOD_CHANGE_PASSWORD.HTML_TITLE');
    super.onComponentReady();
    this.createForm();
  }

  /**
   * To create the login form
   */
  public createForm(): void {
    this.changePasswordFormGroup = this.formBuilder.group({
      old_password: new FormControl('', [Validators.required]),
      new_password: new FormControl('', [Validators.required, Validators.minLength(8), HrValidators.validatePassword]),
      new_password_confirmation: new FormControl('', [Validators.required]),
    });
    this.changePasswordFormGroup.get('new_password_confirmation').setValidators(
      [
        Validators.required,
        HrValidators.fieldsMatch(this.changePasswordFormGroup.get('new_password'))
      ]
    );
  }

  /**
   * On form submit
   */
  public saveChangedPassword(): void {
    this.changePasswordSubscription = this.changePasswordService.put(this.changePasswordFormGroup.getRawValue())
      .subscribe(() => {
        this.showSuccessSnackBar(this.translateService.instant('MOD_ACCOUNT.SUCCESS_MESSAGE_CHANGE_PASSWORD'));
        this.router.navigate(['auth/logout']);
      }, err => {
        this.onFormSubmissionError(err, this.changePasswordFormId);
      });
  }

  /**
   * On Destroy
   */
  public ngOnDestroy(): void {
    if (this.changePasswordSubscription != null) {
      this.changePasswordSubscription.unsubscribe();
    }
    this.pageTitleService.setTitle(this.translateService.instant('MOD_ACCOUNT.HTML_TITLE'));
  }

}
