import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';

import {LoginComponent} from './login.component';
import {
  TranslateModule,
} from '@ngx-translate/core';
import {throwError} from 'rxjs';
import {environment} from '../../../../../environments/environment';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {SettingService} from '@common/services/settings.service';
import {MockSettingService} from '@common/testing-resources/mock-services/MockSettingService';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      providers: [
        ...MOCK_PROVIDERS,
        {provide: SettingService, useClass: MockSettingService}

      ],
      imports: [TranslateModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should set page title', () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_AUTH.MOD_LOGIN.HTML_TITLE');
  });

  it('should create login component', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize form', () => {
    spyOn(component, 'createForm');
    component.ngOnInit();
    expect(component.createForm).toHaveBeenCalled();
  });

  it('should not set user to session and redirect in case of error', fakeAsync(async () => {
    // @ts-ignore
    fixture.detectChanges();
    // @ts-ignore
    spyOn(component, 'setUserToSessionAndRedirect').and.returnValue(throwError({status: 417}));
    component.onFormSubmit();
    expect(component.setUserToSessionAndRedirect).not.toHaveBeenCalled();
    flush();
  }));

  it('should check initial form values', () => {
    const loginFormGroup = component.loginFormGroup;
    const loginFormValues = {
      username: '',
      password: ''
    };
    expect(loginFormGroup.value).toEqual(loginFormValues);
  });

  it('should validate form fields', () => {
    component.createForm();
    const loginFormElement: HTMLInputElement = fixture.debugElement.nativeElement
      .querySelector('#login-form').querySelectorAll('input')[0];
    const userNameValueFormGroup = component.loginFormGroup.get('username');
    expect(loginFormElement.value).toEqual(userNameValueFormGroup.value);
    expect(userNameValueFormGroup.errors.required).toBeTruthy();
  });

  it('should have the link at the sso buttons set as in environment', () => {
    const env = component.env;
    component.onComponentReady();
    expect(env).toEqual(environment);
  });

  it('should have the link to forgot password set', () => {
    component.onComponentReady();
    const forgotPasswordLink = fixture.debugElement.nativeElement
      .querySelector('#login-form').querySelector('a').getAttribute('routerLink');
    expect(forgotPasswordLink).toEqual('/auth/forgot-password');
  });

  it('should call setUserToSessionAndRedirect in case of success', fakeAsync(async () => {
    spyOn(component, 'setUserToSessionAndRedirect');
    spyOn(component, 'setSettingInfo').and.returnValue();
    component.onComponentReady();
    component.loginFormGroup.patchValue({username: 'validUsername', password: 'validPassword'});
    await component.onFormSubmit();
    await fixture.whenStable();
    expect(component.setUserToSessionAndRedirect).toHaveBeenCalled();
    flush();
  }));


  it('should call onFormSubmissionError in case of success', fakeAsync(async () => {
    spyOn(component, 'onFormSubmissionError');
    component.onComponentReady();
    component.loginFormGroup.patchValue({username: 'invalidUsername', password: 'invalidPassword'});
    await component.onFormSubmit();
    await fixture.whenStable();
    expect(component.onFormSubmissionError).toHaveBeenCalled();
  }));
});
