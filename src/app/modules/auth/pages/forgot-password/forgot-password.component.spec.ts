import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing';

import {ForgotPasswordComponent} from './forgot-password.component';
import {
  TranslateModule
} from '@ngx-translate/core';
import {defer, of, throwError} from 'rxjs';
import {PasswordEmailService} from '@common/services/password-email.service';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {MockPasswordEmailService} from '@common/testing-resources/mock-services/MockPasswordEmailService';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('ForgotPasswordComponent', () => {
  let component: ForgotPasswordComponent;
  let fixture: ComponentFixture<ForgotPasswordComponent>;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        ...MOCK_PROVIDERS,
        {
          provide: PasswordEmailService, useClass: MockPasswordEmailService
        },
      ],
      imports: [TranslateModule],
      declarations: [ForgotPasswordComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgotPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should set page title', () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_AUTH.MOD_FORGOT_PASSWORD.HTML_TITLE');
  });

  it('should create forgot password component', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize form', () => {
    spyOn(component, 'createForm');
    component.onComponentReady();
    expect(component.createForm).toHaveBeenCalled();
  });

  it('should not show success snack bar in case of error', fakeAsync(async () => {
    // @ts-ignore
    spyOn(component, 'showSuccessSnackBar').and.returnValue(throwError({error: {status: 417}}));
    await component.sendResetPasswordLink();
    await fixture.whenStable();
    expect(component.showSuccessSnackBar).not.toHaveBeenCalled();
    flush();
  }));

  it('should show error message in case of error', fakeAsync(async () => {
    spyOn(component, 'showSuccessSnackBar').and.returnValue();
    await component.sendResetPasswordLink();
    await fixture.whenStable();
    expect(component.showSuccessSnackBar).not.toHaveBeenCalled();
  }));

  it('should show validation when form is empty', () => {
    component.createForm();
    const loginFormElement: HTMLInputElement = fixture.debugElement.nativeElement
      .querySelector('#forgot-password-form').querySelectorAll('input')[0];
    const userNameValueFormGroup = component.formGroup.get('email');
    expect(loginFormElement.value).toEqual(userNameValueFormGroup.value);
    expect(userNameValueFormGroup.errors.required).toBeTruthy();
  });

  it('should show success message in case of success and redirect to login', fakeAsync(async () => {
    spyOn(component, 'showSuccessSnackBar');
    component.onComponentReady();
    component.formGroup.patchValue({email: 'a@a.com'});
    await component.sendResetPasswordLink();
    await fixture.whenStable();
    expect(component.showSuccessSnackBar).toHaveBeenCalled();
    flush();
  }));

});
