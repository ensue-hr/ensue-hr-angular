import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ResetPasswordService} from '@common/services';
import {Subscription} from 'rxjs';
import {HrValidators} from '@common/utilities';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
})
export class ResetPasswordComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Form
   */
  public formGroup: FormGroup;

  public resetPasswordFormId = 'reset-pass-form';

  /**
   * Subscription
   */
  public resetPasswordSubscription: Subscription;

  /**
   * Page Title
   */
  public pageTitle = '';

  public isLoading = false;

  /**
   * Constructor
   */
  constructor(private formBuilder: FormBuilder, public resetPasswordService: ResetPasswordService, protected injector: Injector) {
    super(injector);
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_AUTH.MOD_RESET_PASSWORD.HTML_TITLE');
    super.onComponentReady();
    this.createForm();
    this.setTokenAndEmail();
  }

  /**
   * To create the login form
   */
  public createForm(): void {
    this.formGroup = this.formBuilder.group({
      password: new FormControl('', [Validators.required, Validators.minLength(8), HrValidators.validatePassword]),
      password_confirmation: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      token: new FormControl('', [Validators.required]),
    });
    this.formGroup.get('password_confirmation').setValidators(
      [
        Validators.required,
        HrValidators.fieldsMatch(this.formGroup.get('password'))
      ]
    );
  }

  /**
   * Set token and email
   */
  public setTokenAndEmail(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.formGroup.get('email').setValue(params.email);
      this.formGroup.get('token').setValue(params.token);
    });
  }

  /**
   * To send email reset link
   */
  public resetPassword(): void {
    if (!this.formGroup.valid) {
      Object.keys(this.formGroup.controls).forEach(field => {
        this.formGroup.get(field).markAsTouched({onlySelf: true});
      });
      return;
    }
    this.isLoading = true;
    this.resetPasswordSubscription = this.resetPasswordService.post(this.formGroup.getRawValue()).subscribe(() => {
      this.isLoading = false;
      this.router.navigate(['auth/reset-password-confirmation']);
    }, error => {
      this.isLoading = false;
      this.onFormSubmissionError(error, this.resetPasswordFormId);
    });
  }

  public ngOnDestroy(): void {
    if (this.resetPasswordSubscription != null) {
      this.resetPasswordSubscription.unsubscribe();
    }
  }
}
