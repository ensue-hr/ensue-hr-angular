import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';
import {Subscription} from 'rxjs';
import {LogoutService} from '@common/services';
import {UserModel} from '@common/models';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-logout',
  template: '',
})
export class LogoutComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Subscription
   */
  public logoutSubscription: Subscription;

  /**
   * Page Title
   */
  public pageTitle = '';

  /**
   * Constructor
   */
  constructor(injector: Injector, protected logoutService: LogoutService,
              public dialog: MatDialog) {
    super(injector);
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_AUTH.MOD_LOGOUT.HTML_TITLE');
    super.onComponentReady();
    this.logoutUserAndClearAuth();
  }

  /**
   * To log user out and clear user session
   */
  public logoutUserAndClearAuth(): void {
    this.logoutSubscription = this.logoutService.get().subscribe((resp: UserModel) => {
      this.session.clearAuth();
      this.router.navigate(['auth/login']);
    }, error => {
      this.session.clearAuth();
      this.router.navigate(['auth/login']);
    });
  }

  /**
   * On destroy
   */
  public ngOnDestroy(): void {
    if (this.logoutSubscription != null) {
      this.logoutSubscription.unsubscribe();
    }
  }
}
