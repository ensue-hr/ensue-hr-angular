import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';
import {Subscription} from 'rxjs';
import {UserDetailService, UserService} from '@common/services';
import {NicoStorageService} from '@system/services';
import {SettingService} from '@common/services/settings.service';

@Component({
  selector: 'app-callback',
  template: '',
})
export class CallbackComponent extends AbstractBaseComponent implements OnInit, OnDestroy {

  /**
   * Page Title
   */
  public pageTitle = '';

  /**
   * Subscription
   */
  public tokenSubscription: Subscription;

  public userSubscription: Subscription;

  public settingSubscription: Subscription;

  public errorMessageSubscription: Subscription;

  /**
   * Constructor
   */
  constructor(private injector: Injector, public user: UserService, public userDetailService: UserDetailService,
              public nicoStorageService: NicoStorageService, public settingService: SettingService) {
    super(injector);
  }

  /**
   * On Component Ready
   */
  public onComponentReady(): void {
    this.pageTitle = this.translateService.instant('MOD_AUTH.MOD_CALLBACK.HTML_TITLE');
    super.onComponentReady();
    this.extractToken();
  }

  /**
   * To extract token from url and place in localstorage
   */
  public extractToken(): void {
    const promise = new Promise (resolve => {
      this.tokenSubscription = this.activatedRoute.queryParams.subscribe(params => {
        resolve(this.session.setAuthToken(params.access_token));
      });
    });
    promise.then(() => {
      this.userSubscription = this.user.get().subscribe(user => {
        this.session.setUser(user);
        this.setSettingInfo();
        this.setMyDetails();
      }, error => {
        this.session.clearAuth();
        this.errorMessageSubscription = this.activatedRoute.queryParams.subscribe(params => {
          this.showErrorSnackBar(params.message);
        });
      });
    });
    promise.catch(error => {
      this.router.navigate(['auth/login']);
    });
  }

  public setSettingInfo(): void {
    this.settingSubscription = this.settingService.get().subscribe(response => {
      const settings = {};
      for (const setting of response) {
        settings[setting.key] = setting.value;
      }
      this.nicoStorageService.setItem('settings', JSON.stringify(settings));
      this.router.navigate(['manage/dashboard']);
    });
  }

  public setMyDetails(): void{
    this.userDetailService.get().subscribe(model => {
      this.nicoStorageService.setItem('user_details', JSON.stringify(model));
    });
  }
  /**
   * Destroy method
   */
  public ngOnDestroy(): void {

    if (this.tokenSubscription != null) {
      this.tokenSubscription.unsubscribe();
    }

    if (this.userSubscription != null) {
      this.userSubscription.unsubscribe();
    }
    if (this.settingSubscription != null) {
      this.settingSubscription.unsubscribe();
    }
  }
}
