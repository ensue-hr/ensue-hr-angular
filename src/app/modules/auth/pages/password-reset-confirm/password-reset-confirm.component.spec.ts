import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PasswordResetConfirmComponent } from './password-reset-confirm.component';
import {TranslateModule
} from '@ngx-translate/core';
import {MOCK_PROVIDERS} from '@common/testing-resources/mock-utils';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('PasswordResetConfirmComponent', () => {
  let component: PasswordResetConfirmComponent;
  let fixture: ComponentFixture<PasswordResetConfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        ...MOCK_PROVIDERS
      ],
      imports: [TranslateModule],
      declarations: [ PasswordResetConfirmComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordResetConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create Password Reset Component', () => {
    expect(component).toBeTruthy();
  });

  it('should set page title', () => {
    component.onComponentReady();
    expect(component.pageTitle).toEqual('MOD_AUTH.MOD_PASSWORD_RESET_CONFIRM.HTML_TITLE');
  });

  it('should have the link to login page set', () => {
    component.onComponentReady();
    const forgotPasswordLink = fixture.debugElement.nativeElement
      .querySelector('a').getAttribute('routerLink');
    expect(forgotPasswordLink).toEqual('/auth/login');
  });
});
