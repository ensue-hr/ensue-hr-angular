import {Component, Injector, OnInit} from '@angular/core';
import {filter} from 'rxjs/operators';
import {Scroll} from '@angular/router';
import {AbstractBaseComponent} from '../../AbstractBaseComponent';

@Component({
  selector: 'app-web-components',
  templateUrl: './web-components.component.html',
  styleUrls: ['./web-components.component.scss']
})
export class WebComponentsComponent extends AbstractBaseComponent implements OnInit {

  public lightTheme: boolean;

  public selectedElementId: string;

  public componentsList = [
    {name: 'Avatar', fragment: 'avatar'},
    {name: 'Badge & Pills', fragment: 'badge-pills'},
    {name: 'Buttons', fragment: 'buttons'},
    {name: 'Charts', fragment: 'charts'},
    {name: 'Circular Icons', fragment: 'circular-icon'},
    {name: 'Drawer', fragment: 'drawer'},
    {name: 'Dropdown', fragment: 'dropdown'},
    {name: 'Empty State', fragment: 'empty-state'},
    {name: 'Form', fragment: 'form'},
    {name: 'Flex Table', fragment: 'flex-table'},
    {name: 'Grid', fragment: 'grid'},
    {name: 'Input', fragment: 'input'},
    {name: 'Modal', fragment: 'modal'},
    {name: 'Notification', fragment: 'notification'},
    {name: 'Pagination', fragment: 'pagination'},
    {name: 'Skeleton', fragment: 'skeleton'},
    {name: 'Snackbar', fragment: 'snackbar'},
    {name: 'Switch', fragment: 'switch'},
    {name: 'Tab', fragment: 'tab'},
    {name: 'Typography', fragment: 'typography'},
  ];

  constructor(inject: Injector) {
    super(inject);
  }

  ngOnInit(): void {
    this.pageTitleService.setTitle('Web Styles - Components');
    this.lightTheme = this.appService.isLightMode();
  }

  /**
   * Toggle Light/Dark Theme
   */
  public onThemeToggle(): void {
    this.lightTheme = !this.lightTheme;
    this.appService.enableLightMode(this.lightTheme);
  }
}
