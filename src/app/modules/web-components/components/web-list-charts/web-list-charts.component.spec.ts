import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebListChartsComponent } from './web-list-charts.component';

describe('WebListChartsComponent', () => {
  let component: WebListChartsComponent;
  let fixture: ComponentFixture<WebListChartsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebListChartsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WebListChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
