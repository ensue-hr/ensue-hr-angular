import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {NicoUtils} from '@system/utilities';
import {EmptyStateComponent} from '@common/components/empty-state/empty-state.component';
import {WebListTestChildComponent} from './web-list-test-child.component';

@Component({
  selector: 'app-web-list-drawer',
  templateUrl: './web-list-drawer.component.html',
})
export class WebListDrawerComponent implements OnInit {

  constructor(private dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  public openModalDrawer(size: string, disableClose?: boolean): void {
    this.dialog.open(EmptyStateComponent, {
      panelClass: ['modal-drawer-view', size],
      disableClose: NicoUtils.isNullOrUndefined(disableClose) ? false : disableClose
    });
  }

  public openModalDrawerForm(): void {
    this.dialog.open(WebListTestChildComponent, {
      panelClass: ['modal-drawer-view', 'modal-xs'],
      disableClose: true
    });
  }
}
