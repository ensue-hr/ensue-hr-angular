import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-test-child',
  template: `
    <h4 class="title">
      {{data.title}}
    </h4>
    <div class="card-body">
      <form #tesForm="ngForm">
        <div class="form-group">
          <label for="username" class="form-label">Username</label>
          <input type="text" id="username" class="form-control" placeholder="Username" [(ngModel)]="model.name" name="name"/>
        </div>
        <div class="form-group">
          <label for="mail" class="form-label">Email</label>
          <input type="email" id="mail" class="form-control" placeholder="Email" [(ngModel)]="model.email" name="email"/>
        </div>
        <div class="form-group">
          <label for="userType" class="form-label">User Type</label>
          <select id="userType" class="form-control select-form-control" [(ngModel)]="model.type" name="type">
            <option value="superAdmin">Super Admin</option>
            <option value="admin">Admin</option>
            <option value="user">User</option>
            <option value="guest">Guest</option>
          </select>
        </div>
        <div class="display-flex flex-justify-content-end">
          <button class="btn btn-light" (click)="close()">Cancel</button>
          <button class="btn btn-primary ml-sm" (click)="close()">Save</button>
        </div>
      </form>
    </div>
  `,
})
export class WebListTestChildComponent implements OnInit {

  public model: any = {};

  constructor(public dialogRef: MatDialogRef<any>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit(): void {
  }

  close(): void {
    this.dialogRef.close(this.model);
  }
}
