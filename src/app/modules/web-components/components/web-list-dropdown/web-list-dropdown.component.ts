import {AfterViewInit, Component, Injector, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AdvancedSelectConfigInterface, FlyMenuItemInterface} from '@system/components';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';

@Component({
  selector: 'app-web-list-dropdown',
  templateUrl: './web-list-dropdown.component.html',
})
export class WebListDropdownComponent extends AbstractBaseComponent implements OnInit, AfterViewInit {

  @ViewChild('dropDownItemTemplate') dropDownItemTemplate: TemplateRef<any>;

  public options = [
    {name: 'A very long name here'},
    {name: 'Name'},
    {name: 'Test Name'}
  ];

  public config: AdvancedSelectConfigInterface = {};

  public flyMenuAddOns: FlyMenuItemInterface[] = [
    {name: 3, label: 'Edit', active: true, faIcon: 'far fa-pen'},
    {name: 3, label: 'Delete', active: true, faIcon: 'far fa-trash'},
    {name: 3, label: 'Settings', active: true, faIcon: 'far fa-cog'}
  ];

  constructor(inject: Injector) {
    super(inject);
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.config = {
      itemTemplate: this.dropDownItemTemplate,
      selectInfoLabel: 'Select One',
    };
  }

}
