import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-web-list-switch',
  templateUrl: './web-list-switch.component.html',
})
export class WebListSwitchComponent implements OnInit {

  // Switch Component
  public isActive = false;
  public isDisabled = false;
  public labelPlacement: 'suffix' | 'prefix' = 'suffix';
  public switchLabel: string;

  constructor() {
  }

  ngOnInit(): void {
  }

  public onSwitchValueChange($event): void {
    this.isActive = $event;
  }

}
