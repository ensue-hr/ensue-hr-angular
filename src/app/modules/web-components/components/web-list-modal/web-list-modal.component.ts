import {Component, Injector, OnInit} from '@angular/core';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';
import {EmptyStateComponent} from '@common/components/empty-state/empty-state.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-web-list-modal',
  templateUrl: './web-list-modal.component.html',
})
export class WebListModalComponent extends AbstractBaseComponent implements OnInit {

  constructor(inject: Injector, private dialog: MatDialog) {
    super(inject);
  }

  ngOnInit(): void {
  }

  public openModal(sizeClass: string): void {
    this.dialog.open(EmptyStateComponent, {panelClass: sizeClass});
  }
}
