import {Component, Injector, OnInit} from '@angular/core';
import {SnackBarComponent} from '@common/components/snack-bar/snack-bar.component';
import {AbstractBaseComponent} from '../../../../AbstractBaseComponent';

@Component({
  selector: 'app-web-list-snackbar',
  templateUrl: './web-list-snackbar.component.html',
})
export class WebListSnackbarComponent extends AbstractBaseComponent implements OnInit {

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
  }

  public openSnackBar(message: string, type?: string, title?: string): void {
    this.snackbar.openFromComponent(SnackBarComponent, {
      verticalPosition: 'top',
      horizontalPosition: 'end',
      data: {type, message, title}
    });
  }

}
