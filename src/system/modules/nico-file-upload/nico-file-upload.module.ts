import {NgModule} from '@angular/core';
import {NicoFileUploadComponent} from './component/nico-file-upload.component';
import {CommonModule} from '@angular/common';

@NgModule(
  {
    declarations: [NicoFileUploadComponent],
    exports: [
      NicoFileUploadComponent
    ],
    imports: [
      CommonModule
    ],
    providers: []
  }
)
export class NicoFileUploadModule {

}
