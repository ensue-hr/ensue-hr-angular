import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef
} from '@angular/core';
import {ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR} from '@angular/forms';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {NicoUtils} from '../../utilities';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {NicoHttpClient} from '@system/http-client';
import {HttpParams} from '@angular/common/http';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'nico-advanced-select',
  template: `
    <div class="nico-advanced-select" [class.open]="dropdownExpanded">
      <div class="advance-select-block" (click)="toggleDropdown()">
        <span *ngIf="!selectedItem && !selectedTemplate">{{config.selectInfoLabel}}</span>
        <span *ngIf="selectedItem || selectedTemplate">
          <ng-container *ngTemplateOutlet="selectedTemplate;context:{item:selectedItem}"></ng-container>
        </span>
        <div class="chevron-down">
          <svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
            <path fill-rule="evenodd"
                  d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
          </svg>
        </div>
      </div>
      <ul class="advanced-select-option-container {{config?.dropdownPlacement}}" *ngIf="dropdownExpanded"
          [@nicoAdvSelectAnimation]>
        <li class="advanced-select-option-item search-option" *ngIf="config.showSearchInput">
          <input type="text" (keyup)="onModelChange($event)"
                 class="form-control select-input" [id]='searchInputId'
                 [placeholder]="config.searchPlaceholderText || 'Search'"></li>
        <li *ngIf="busyFetchingItems" class="advanced-select-option-item busy-spinner">
          <span class="fa fa-spinner fa-spinner-custom-animation"></span>
        </li>
        <li *ngIf="!busyFetchingItems && items?.length>0" class="advanced-select-option-item">
          <ul class="dropdown-option-list">
            <ng-container *ngIf="!config.disableEmptyField">
              <li *ngIf="!config.emptySelectionTemplate" (click)="clearItem()"
                  class="dropdown-option-item">{{config.emptySelectionText}}</li>
              <li *ngIf="config.emptySelectionTemplate" (click)="clearItem()" class="dropdown-option-item">
                <ng-container *ngTemplateOutlet="config.emptySelectionTemplate"></ng-container>
              </li>
            </ng-container>
            <li *ngFor="let item of items;let i=index" (click)="selectItem(i)" class="dropdown-option-item"
                [class.selected]="isItemSelected(item)">
              <ng-container *ngTemplateOutlet="config.itemTemplate;context:{item:item}"></ng-container>
            </li>
          </ul>
        </li>
        <li *ngIf="!busyFetchingItems && items?.length==0"
            class="advanced-select-option-item no-result-option">{{config.emptyResultText}}</li>
        <li *ngIf="config.actionButtonEnabled" class="advanced-select-option-item action-option">
          <button class="btn btn-primary" (click)="onActionButtonClick($event)">{{config.actionButtonLabel}}</button>
        </li>
      </ul>
    </div>
  `,
  styleUrls: ['nico-advanced-select.scss'],
  animations: [
    trigger('nicoAdvSelectAnimation', [
      state('void', style({opacity: 0, transform: 'scale(0.2)'})),
      state('*', style({opacity: 1, transform: 'scale(1)'})),
      transition('* <=> void', animate('0.1s cubic-bezier(.32, 0,.72, 1)'))
    ])
  ],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => NicoAdvancedSelectComponent), multi: true},
  ],
})
export class NicoAdvancedSelectComponent implements ControlValueAccessor, OnChanges, OnDestroy, OnInit {

  public dropdownExpanded = false;

  public selectedItem: any;

  public busyFetchingItems = false;

  public searchInputId: string;

  public items: Array<any> = [];

  private documentClickListener: (event: any) => void;

  /**
   * The input
   *  {string}
   */

  onTouched: () => void;
  /**
   * The text to be displayed in the input
   *  {string}
   */
  public text = '';
  /**
   * The old value of the search input box
   * @private
   */
  private previousValue = '';

  public searchTextChanged: Subject<string> = new Subject();

  /**
   * Input value
   *  {number}
   *
   */
    // tslint:disable-next-line:variable-name no-input-rename
  @Input('value') _value = null;

  // tslint:disable-next-line:variable-name no-input-rename
  @Input('readonly') _readonly: boolean | string | number = false;
  /**
   * The on select event
   */
  @Output() pick: EventEmitter<any>;
  /**
   * On no result
   *  EventEmitter
   */
  @Output() noResult: EventEmitter<any>;
  /**
   * On Loading
   *  EventEmitter
   */
  @Output() loading: EventEmitter<any>;

  @Output() actionButtonClick: EventEmitter<any>;

  /**
   * Static options
   */
  @Input() options: any;
  /**
   * Enable the remote option
   *  {boolean}
   */
  @Input() remote = false;
  /**
   * Remote url sort/filter options
   */
  @Input() remoteQueryParams: string;
  /**
   * the url for server call
   *  string
   */
  @Input() public url = '';

  @Input() config: AdvancedSelectConfigInterface = {};

  @Input() multiple = false;

  public selectedTemplate: TemplateRef<any>;

  /**
   *  function
   */
  propagateChange: any = () => {
  }
  /**
   *  function
   */
  validateFn: any = () => {
  }

  constructor(protected elemRef: ElementRef, private http: NicoHttpClient) {
    this.searchInputId = `adv-select${parseInt((Math.random() * 10000000).toString(), 0)}`;
    this.pick = new EventEmitter();
    this.noResult = new EventEmitter();
    this.loading = new EventEmitter();
    this.actionButtonClick = new EventEmitter();
    this.remoteQueryParams = NicoUtils.isNullOrUndefined(this.remoteQueryParams) ? '' : this.remoteQueryParams;
    this.searchTextChanged.pipe(debounceTime(400), distinctUntilChanged()).subscribe((text: string) => {
      this.text = text;
      this.getItems();
    });
  }

  protected initConfig(): void {
    this.config.emptyResultText = NicoUtils.isNullOrUndefined(this.config.emptyResultText) ? 'No results' : this.config.emptyResultText;
    this.config.searchPlaceholderText = NicoUtils.isNullOrUndefined(this.config.searchPlaceholderText) ? 'Search' : this.config.searchPlaceholderText;
    this.config.emptySelectionText = NicoUtils.isNullOrUndefined(this.config.emptySelectionText) ? 'Deselect' : this.config.emptySelectionText;
    this.config.selectInfoLabel = NicoUtils.isNullOrUndefined(this.config.selectInfoLabel) ? 'Select one' : this.config.selectInfoLabel;
    this.config.actionButtonLabel = NicoUtils.isNullOrUndefined(this.config.selectInfoLabel) ? 'Add' : this.config.actionButtonLabel;
    this.config.disableEmptyField = NicoUtils.isNullOrUndefined(this.config.disableEmptyField) ? false : this.config.disableEmptyField;
    this.config.dropdownPlacement = NicoUtils.isNullOrUndefined(this.config.dropdownPlacement) ? 'topRight' : this.config.dropdownPlacement;
    this.config.showSearchInput = NicoUtils.isNullOrUndefined(this.config.showSearchInput);
  }

  ngOnInit(): void {
    this.documentClickListener = (event: any) => {
      if (!this.elemRef.nativeElement.contains(event.target)) {
        this.closeDropdown();
      }
    };
    document.addEventListener('click', this.documentClickListener);

    // TODO: Modify setTimeOut logic | Currently config.template is not defined unless we setTimeout
    setTimeout(() => {
      this.selectedTemplate = this.config.itemTemplate;
      if (!this._value) {
        this.selectedTemplate = this.config.emptySelectionTemplate;
      }
      this.selectedItem = this.value = this._value;
    }, 200);
  }

  ngOnDestroy(): void {
    document.removeEventListener('click', this.documentClickListener);
  }

  get value(): any {
    return this._value;
  }

  set value(val) {
    this._value = val;
    this.propagateChange(val);
  }

  ngOnChanges(value): any {
    this.initConfig();
  }

  writeValue(value: any): void {
    this.text = '';
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  getItems(): void {
    if (this.remote === true) {
      let param = new HttpParams();

      param = param.set('skip', 'true');

      this.http.get(this.url + `?keyword=${this.text} ${this.remoteQueryParams}`, {
          params: param,
        }).subscribe((response) => {
          this.items = response.data;
        });
    } else {
      this.items = this.options;
    }
  }

  typeaheadNoResults($event: any, i: number): void {
    this.value = null;
    this.noResult.emit(this.value);
  }

  typeaheadLoading($event: any, i: number): void {
    this.loading.emit(null);
  }

  toggleDropdown(): void {
    if (this._readonly === true || this._readonly === 1 || this._readonly === 'true') {
      return;
    }
    this.dropdownExpanded = !this.dropdownExpanded;
    if (this.dropdownExpanded) {
      if (this.config.showSearchInput) {
        setTimeout(() => {
          document.getElementById(this.searchInputId).focus();
        }, 200);
      }
      this.getItems();
    }
  }

  private selectSingleItem(index): void {
    this.closeDropdown();
    this.selectedItem = this.items[index];
    this.value = this.selectedItem;
    this.selectedTemplate = this.config.itemTemplate;
    this.pick.emit(this.value);
  }

  private addToSelection(item: any): void {
    this.selectedItem.push(item);
  }

  private removeFromSelection(item: any): void {
    const i = this.selectedItem.indexOf(item);
    if (i > -1) {
      this.selectedItem.splice(i, 1);
    }
  }

  private selectMultipleItems(index): void {
    if (!this.selectedItem) {
      this.selectedItem = [];
    }
    if (!this.isItemSelected(this.items[index])) {
      this.addToSelection(this.items[index]);
    } else {
      this.removeFromSelection(this.items[index]);
    }
    this.value = this.selectedItem;
    this.pick.emit(this.value);
  }

  public selectItem(index: number): void {
    if (this._readonly === true || this._readonly === 1 || this._readonly === 'true') {
      return;
    }
    if (this.multiple) {
      this.selectMultipleItems(index);
    } else {
      this.selectSingleItem(index);
    }
  }

  public isItemSelected(item: any): boolean {
    if (!this.multiple) {
      return item === this.selectedItem;
    }
    const iterable = this.selectedItem || [];
    for (const i of iterable) {
      if (i === item) {
        return true;
      }
    }
    return false;
  }

  private closeDropdown(): void {
    this.text = '';
    this.dropdownExpanded = false;
  }

  public clearItem(): void {
    this.selectedItem = null;
    this.value = this.selectedItem;
    this.closeDropdown();

    this.selectedTemplate = this.config.emptySelectionTemplate;
    this.pick.emit(this.value);
  }

  public onModelChange(value: any): void {
    value = value.target.value;
    value += '';
    value = value.trim();
    if (value === '' || value === this.previousValue) {
      return;
    }
    this.previousValue = value;
    this.searchTextChanged.next(value);
  }

  onBlur(): void {
    this.closeDropdown();
    this.onTouched();
  }

  setDisabledState(isDisabled: boolean): void {

  }

  onActionButtonClick($event): void {
    this.actionButtonClick.emit($event);
  }

  validate(c: FormControl): void {
    return this.validateFn(c);
  }
}

export interface AdvancedSelectConfigInterface {
  itemTemplate?: TemplateRef<any>;
  selectionTemplate?: TemplateRef<any>;
  displayProperty?: string;
  emptyResultText?: string;
  selectInfoLabel?: string;
  emptySelectionText?: string;
  emptySelectionTemplate?: TemplateRef<any>;
  searchPlaceholderText?: string;
  equalityCheckProperty?: string;
  actionButtonLabel?: string;
  actionButtonEnabled?: boolean;
  disableEmptyField?: boolean;
  showSearchInput?: boolean;
  dropdownPlacement?: 'topRight' | 'topLeft';
}
