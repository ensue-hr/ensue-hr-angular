import {Component, Input} from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
    selector: 'nico-acronym',
    template: `<span class="acronym">{{acronymizeWords()}}</span>`
})
export class NicoAcronymComponent {
    /**
     * Take input from element
     */
    @Input() words: string;

    @Input() limit = 0;

    /**
     * The acronym of the word
     */
    public acronym: string;

    /**
     * Acronymize the words
     */
    public acronymizeWords(): string {
        // split the words first
        if (!this.words) {
            return '';
        }
        if (!this.limit) {
            this.limit = 1;
        }
        const strings = this.words.split(' ');
        this.acronym = '';
        for (let i = 0; i < this.limit; i++) {
            this.acronym += strings[i].toUpperCase().charAt(0);
        }
        return this.acronym;
    }

}
