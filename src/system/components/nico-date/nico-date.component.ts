import {Component, Input, OnChanges, OnInit} from '@angular/core';
import * as moment from 'moment';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'nico-date',
  template: '<span class=\'date nico-date\'>{{output}}</span>'
})
export class NicoDateComponent implements OnInit, OnChanges {
  /**
   * The output value
   */
  public output = '';
  /**
   * Input value
   */
  @Input() value: moment.Moment;
  /**
   * format
   */
  @Input() format: string;
  /**
   * countUp
   */
  @Input() countUp = false;

  private processValue(): void {
    if (!this.format) {
      this.format = 'MM/DD/YYYY';
    }
    this.output = moment(this.value).utc(true).local().format(this.format);
    if (this.countUp) {
      this.updateOutputForCountUp();
    }
  }

  /**
   *
   */
  public ngOnInit(): void {
    this.processValue();
  }

  public ngOnChanges(): void {
    this.processValue();
  }


  protected updateOutputForCountUp(): void {
    const now = moment().utc(true);
    const valueTime = moment(this.value).utc(true);
    const diff = now.diff(valueTime);

    if (diff === 0) {
      this.output =  '';
    } else if (diff < 60) {
      this.output = `${diff} minutes ago.`;
    } else if (diff < 60 * 24) {
      const hour = Math.floor(diff / 60);
      if (hour < 2) {
        this.output = `${hour} hour ago.`;
      } else {
        this.output = `${hour} hours ago.`;
      }
    }else {
      this.output = moment(this.value).utc(true).local().format(this.format);
    }
  }

}
