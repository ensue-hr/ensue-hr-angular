export class NicoStorageService {

  protected keyPrefix = '';
  protected storage: Storage;
  /**
   * Construct storage
   */
  constructor() {
    this.storage = localStorage;
  }

  public setStoreKeyPrefix(str: string): void {
    this.keyPrefix = str;
  }

  public setItem(key: string, value: string): void {
    this.storage.setItem(key, value);
  }

  public getItem(key: string): string {
    return this.storage.getItem(key);
  }

  public clear(): void {
    this.storage.clear();
  }

  public remove(key: string): void {
    this.storage.removeItem(key);
  }



}
