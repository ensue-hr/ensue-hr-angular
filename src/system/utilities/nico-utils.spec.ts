import {NicoUtils} from './nico-utils';

describe('Nico Utils', () => {
  it('should return true for for undefined variable', () => {
    // tslint:disable-next-line:prefer-const
    let variable;
    expect(NicoUtils.isNullOrUndefined(variable)).toBeTrue();
  });

  it('should return "hello universe" instead of "hello world"', () => {
    const tst = 'Hello world';
    expect(NicoUtils.replaceString(tst, 'world', 'universe')).toEqual('Hello universe');
  });

  it('should generate random string for the given length', () => {
    const str = NicoUtils.getRandomString(20);
    expect(str.length).toEqual(20);
  });

});
