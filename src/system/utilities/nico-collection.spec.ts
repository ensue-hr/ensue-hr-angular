import {NicoCollection} from './nico-collection';

describe('Nico Collection', () => {

  let col: NicoCollection<number>;

  beforeEach(() => {
    col  =  new NicoCollection([1, 2, 3, 4, 5]);
  });

  it('should create a NicoCollection class', () => {
    expect(new NicoCollection()).toBeTruthy();
  });

  it('should create collection with given array of data', () => {
    const data = [1, 2, 3, 4, 6];
    col = new NicoCollection(data);
    expect(col.all()).toEqual(data);
  });

  it('should set collection with the given data', () => {
    const data = [1, 2, 3, 4, 5];
    col = new NicoCollection();
    col.setItems(data);
    expect(col.all()).toEqual(data);
  });

  it('should create a new instance of NicoCollection', () => {
    expect((new NicoCollection()).newInstance()).toBeTruthy();
  });

  it('should have length property and the length should be equal to the number of items in the list', () => {
    expect((new NicoCollection(['a', 'b', 'c'])).length).toEqual(3);
  });

  it('should remove item from the given index', () => {
    col.removeFromIndex(3);
    expect(col.getItem(3)).toBe(5);
  });

  it('should be able to map items into new one', () => {
    const result = col.map((i) => {
      return i * 2;
    });
    expect(result.all()).toEqual([2, 4, 6, 8, 10]);
  });

  it('should return index of a given item', () => {

    expect(col.getIndex(3)).toEqual(2);
  });

  it('should add item to the existing list', () => {
    col.add(6);
    expect(col.getItem(5)).toEqual(6);
  });

  it('should remove item from the list', () => {
    col.remove(4);
    expect(col.length).toEqual(4);
  });

  it('should merge the given list with the items in the list', () => {
    col.merge([10, 11, 12, 13, 14, 15]);
    expect(col.all()).toEqual([1, 2, 3, 4, 5, 10, 11, 12, 13, 14, 15]);
  });

  it('should empty the list', () => {
    col.empty();
    expect(col.length).toEqual(0);
  });

  it('should return the length of the collection', () => {
    expect(col.count()).toEqual(5);
  });

  it('should push item to the list', () => {
    col.push(8);
    expect(col.getItem(5)).toEqual(8);
  });

  it('should pop item from the list', () => {
    const num = col.pop();
    expect(num).toEqual(5);
  });

  it('should return the first item match', () => {
    const num = col.first((i) => {
      return i % 2 === 0;
    });
    expect(num).toEqual(2);
  });

  it('should filter list', () => {
    const collection = col.where((i) => {
      return i % 2 === 0;
    });

    expect(collection.all()).toEqual([2, 4]);

  });

  it('should sort item', () => {
    const result = col.sort((a , b ) => {
      return b - a;
    });
    expect(result.all()).toEqual([5, 4, 3, 2, 1]);
  });

  it('should return new tabularized data', () => {
    const input = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    const output = [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
      [10, 11, 12]
    ];
    col.setItems(input);
    const result = col.tabularize(3);
    expect(result).toEqual(output);

  });

});
